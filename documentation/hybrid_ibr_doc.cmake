# Copyright (C) 2021, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

set(PROJECT_PAGE "hybrid_ibrPage")
set(PROJECT_LINK "https://gitlab.inria.fr/sibr/projects/hybrid_ibr")
set(PROJECT_DESCRIPTION "Hybrid Image-based Rendering for Free-view Synthesis, paper references: http://www-sop.inria.fr/reves/Basilic/2021/PLRD21/ , https://repo-sam.inria.fr/fungraph/hybrid-ibr/ ")
set(PROJECT_TYPE "OURS")