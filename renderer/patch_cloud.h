// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#pragma once

#include "Config.hpp"

#include "projects/hybrid_ibr/renderer/voxel_grid.h"
#include "projects/fribr_framework/renderer/3d.h"
#include "projects/fribr_framework/renderer/vision.h"
#include "projects/fribr_framework/renderer/gl_wrappers.h"
#include "projects/fribr_framework/renderer/ray_tracing.h"

#include "core/assets/InputCamera.hpp"
#include <core/graphics/Texture.hpp>

#include <opencv2/opencv.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <fstream>
#include <unordered_map>

namespace sibr {
	class TF_GL;
	struct TexInfo;

	class SIBR_EXP_HYBRID_EXPORT PatchCloud
	{
		

	public:
		// Vertex shader attribute locations
		static const int VERTEX_LOCATION = 0;
		static const int TEXCOORD_LOCATION = 1;

		struct Component
		{
			typedef std::shared_ptr<Component> Ptr;
			VoxelGridFRIBR* grid;

			//cv::Mat                             image;
			std::vector<fribr::float3>          vertices;
			std::vector<fribr::float3>          texcoords;
			std::unordered_map<int, int>        voxel_to_index_remap;
			std::vector<std::vector<uint32_t> > slice_indices;
			std::vector<Eigen::Vector3i>        slice_voxels;
			std::vector<bool>                   slice_valid;
			std::vector<float>					slice_mean;
			std::vector<float>					slice_var;
			std::vector<float>					slice_max;
			std::vector<std::vector<float>>		slice_error_map;
			std::vector<std::vector<float>>		slice_errors;

			Eigen::Vector2i                     resolution;
			Eigen::Vector3f                     position;

			size_t                              vertices_start;
			std::vector<size_t>                 slices_start;

			Component(VoxelGridFRIBR* _grid);
			~Component();

			void add_camera(fribr::Mesh::Ptr       mesh,
				sibr::InputCamera::Ptr& calibrated_camera,
				const size_t           component_index, const int num_cams);

			bool   is_valid(Eigen::Vector3i voxel) const;
			size_t draw(Eigen::Vector3i voxel) const;
			size_t draw() const;


			void save(std::ofstream& out_file) const;
			void load(std::ifstream& in_file);
		};

		struct ImageBin {
			Eigen::Vector3i voxel;
			Eigen::Array3f  bbox_min;
			Eigen::Array3f  bbox_max;

			ImageBin(int bin_x, int bin_y, int bin_z, const VoxelGridFRIBR& grid);
		};
		std::vector<ImageBin> fetch_image_bins(const Eigen::Affine3f world_to_camera,
			const Eigen::Matrix4f camera_to_clip);

		PatchCloud();
		PatchCloud(Eigen::Vector3i _grid_size);
		~PatchCloud();


		void cluster_k(int k)
		{
			m_cluster_k = std::max(12, k);
			std::cout << "Updating cluster k [max(12, input)] to: " << m_cluster_k << std::endl;
		}

		void cluster_threshold(int k)
		{
			m_cluster_threshold = k;
			//std::cout << "Updating cluster threshold to: " << m_cluster_threshold << std::endl;
			m_cluster_k = std::max(12, 4 + m_cluster_threshold);
			//std::cout << "Updating cluster k to: " << m_cluster_k << std::endl;
		}

		void grid_size(Eigen::Vector3i size)
		{
			bool size_changed = size != m_grid.size();
			m_grid.resize(size);
			if (size_changed)
				clear();
		}

		Eigen::Vector3i grid_size() const
		{
			return m_grid.size();
		}

		static const int VOXEL_MEMORY_SIZE = 12;
		void update_voxel_memory(bool b)
		{
			m_update_voxel_memory = b;
		}
		void clear_voxel_memory()
		{
			for (int i = 0; i < VOXEL_MEMORY_SIZE; ++i)
				m_voxel_memory[i].clear();
		}

		static const int NUM_LAYERS = 4;
		void set_display_layer(int display_layer)
		{
			m_display_layer = display_layer;
		}

		enum DisplayMode { Colors = 0, Depth = 1, Normals = 2, OutputDir = 3, InputDir = 4, Flow = 5 };
		void set_display_mode(DisplayMode mode)
		{
			m_display_mode = mode;
		}

		enum MemoryPolicy { KeepResourcesAlive, DeallocateWhenDone };
		void add_cameras(std::vector<sibr::InputCamera::Ptr>& cameras,
			std::vector<fribr::Mesh::Ptr >& meshes,
			MemoryPolicy policy = KeepResourcesAlive);

		void update_components(std::vector<PatchCloud::Component::Ptr>& components);

		void clear();
		void clear_gpu();

		void set_scene(sibr::Mesh::Ptr mesh);

		void set_scene(sibr::Mesh::Ptr mesh, Eigen::Array3f grid_min, Eigen::Array3f grid_max);

		void set_refinementScale(int scale);
		

		void save(const std::string &filename) const;

		void save(const std::string& filename, std::vector<PatchCloud::Component::Ptr>& m_components, sibr::VoxelGridFRIBR& m_grid,
			int refinementScale) ;

		bool load(const std::string& filename, std::vector<PatchCloud::Component::Ptr>& m_components, sibr::VoxelGridFRIBR& m_grid, 
			sibr::VoxelGridFRIBR& fineGrid, int& refinementScale);

		bool _clearDst = true;
		void						   setTextRes(Eigen::Vector2i res);


		Eigen::Vector3f grid_min() const
		{ 
			return m_grid.min().matrix();
		}

		Eigen::Vector3f grid_max() const
		{
			return m_grid.max().matrix();
		}

	private:
		// Deliberately left unimplemented, PatchCloud is noncopyable
		PatchCloud(const PatchCloud&);
		PatchCloud& operator=(const PatchCloud&);

		int                            m_display_layer;
		DisplayMode                    m_display_mode;
		int                            m_cluster_k;
		int                            m_cluster_threshold;
		std::vector<Component::Ptr>    m_components;
		size_t                         m_all_triangles;
		VoxelGridFRIBR                 m_grid;
		VoxelGridFRIBR                 m_gridFine;
		std::vector<fribr::Mesh::Ptr>  m_meshes;
		fribr::Scene::Ptr              m_scene;
		int							   m_refinement_scale = 1;

		// Give the voxel grid a finite memory, to avoid flickering
		bool                           m_update_voxel_memory;
		std::vector<bool>              m_voxel_memory[VOXEL_MEMORY_SIZE];


		// State for rendering
		/*GLuint                         m_texture_array;
		GLuint                         m_camera_pos_texture;
		GLuint                         m_draw_calls;
		GLuint                         m_vao;
		GLuint                         m_index_buffer;
		GLuint                         m_vertex_buffer;
		GLuint                         m_texcoord_buffer;

		GLuint                         m_gpu_grid[4];
		std::vector<float>             m_all_box_meshes;
		std::vector<float>             m_current_box_meshes;
		void upload_texture_image_if_needed();

		fribr::Framebuffer::Ptr        m_voxel_fbo;
		fribr::Framebuffer::Ptr        m_depth_fbo;
		fribr::Framebuffer::Ptr        m_layers[NUM_LAYERS];
		void update_fbos_if_needed();*/


	};

}
