// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#include <PerViewMeshDebugView.hpp>
#include <core/graphics/GUI.hpp>

sibr::PerViewMeshDebugView::PerViewMeshDebugView(const sibr::HybridScene::Ptr& ibrScene, uint render_w, uint render_h, const std::string viewName) :
	_hybridScene(ibrScene),
	sibr::ViewBase(render_w, render_h),
	_viewName(viewName)
{

	const uint w = render_w;
	const uint h = render_h;
	initializeVoxelGrid();

	_numMeshes = ibrScene->cameras()->inputCameras().size();
	_pvMeshes.resize(_numMeshes);
	_inputTextures.resize(_numMeshes);
	for (int i = 0; i < _numMeshes; ++i) {
		std::string camName = ibrScene->cameras()->inputCameras()[i]->name();
		std::string path = ibrScene->sceneArgs().dataset_path.get() + "/deep_blending/pvmeshes/" + camName;
		//_pvMeshes[i].reset(new sibr::Mesh());
		_pvMeshes[i].load(sibr::removeExtension(path) + ".ply");
	}
	_numFineVoxels = std::pow(ibrScene->get_ref_scale(), 3);
	_hybridRenderer.reset(new HybridRenderer(ibrScene->cameras()->inputCameras(),
		ibrScene->get_components(),
		ibrScene->get_grid(), ibrScene->get_fine_grid(),
		w, h));

	_coloredMeshRenderer.reset(new ColoredMeshRenderer());
	_texturedRenderer.reset(new TexturedMeshRenderer());

}

void sibr::PerViewMeshDebugView::setScene(const sibr::HybridScene::Ptr& newScene) {
	_hybridScene = newScene;
	const uint w = getResolution().x();
	const uint h = getResolution().y();

	std::string shaderName = "hybrid_composite";


	// Tell the scene we are a priori using all active cameras.
	std::vector<uint> imgs_ulr;
	const auto& cams = newScene->cameras()->inputCameras();
	for (size_t cid = 0; cid < cams.size(); ++cid) {
		if (cams[cid]->isActive()) {
			imgs_ulr.push_back(uint(cid));
		}
	}
	_hybridScene->cameras()->debugFlagCameraAsUsed(imgs_ulr);

}


void sibr::PerViewMeshDebugView::onRenderIBR(sibr::IRenderTarget& dst, const sibr::Camera& eye)
{
	if (!_sliceErrMap) {
		if (_numSlices > 0) {
			sibr::Texture2DRGB::Ptr inputTex(new Texture2DRGB(_hybridScene->images()->image((uint)_sliceIndices[_selectedSlice]), SIBR_GPU_LINEAR_SAMPLING | SIBR_CLAMP_UVS));
			_texturedRenderer->process(_pvMeshes[_sliceIndices[_selectedSlice]],
				eye,
				inputTex->handle(),
				dst,
				true);

			//sibr::Texture2DRGB32F::Ptr outputView(new Texture2DRGB32F(_hybridScene->images()->image((uint)_sliceIndices[_selectedView]), SIBR_GPU_LINEAR_SAMPLING | SIBR_CLAMP_UVS | SIBR_FLIP_TEXTURE));
			//if (_numSlices > 0) {
			//	_hybridRenderer->refineSlices(eye, dst.w(), dst.h(),
			//		_sliceIndices[_selectedSlice], outputView, _selectedFineVoxel, dst,
			//		_hybridScene->renderTargets()->getInputRGBTextureArrayPtr());
			//}

		}
	}
	else {
		//glDisable(GL_DEPTH_TEST);
		//glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		//glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		//_hybridRenderer->renderSliceError(dst);
		//blit(*sliceErr, dst);
	}
}

void sibr::PerViewMeshDebugView::onUpdate(Input& input)
{
	// Live shader reloading for debug.
	if (input.key().isReleased(Key::F8)) {

		const std::string shaderSrcPath = "../../src/projects/hybrid_ibr/renderer/shaders/";
		const std::string shaderDstPath = sibr::getShadersDirectory("hybrid_ibr") + "/";
		const auto files = sibr::listFiles(shaderSrcPath, false, false, { "vert", "frag", "geom" });
		for (const auto& file : files) {
			sibr::copyFile(shaderSrcPath + file, shaderDstPath + file, true);
		}


		_hybridRenderer->setupShaders();

	}


	// Visualisation.
	if (_debugView && _debugView->active()) {
		renderVisualisation();
	}
}

void sibr::PerViewMeshDebugView::onGUI()
{
	if (ImGui::Begin(_viewName.c_str())) {
		ImGui::Text("PV Mesh Debug View");

		ImGui::Checkbox("SliceErrorMap ", &_sliceErrMap);
		ImGui::Separator();
		ImGui::Text("Select Voxel");
		if (ImGui::SliderInt(": X", &_voxX, 0, _hybridScene->get_grid().size().x() - 1) ||
			ImGui::SliderInt(": Y", &_voxY, 0, _hybridScene->get_grid().size().x() - 1) ||
			ImGui::SliderInt(": Z", &_voxZ, 0, _hybridScene->get_grid().size().x() - 1)) {
			updateVoxel();
		}

		if (ImGui::Button("Load Camera")) {
			std::string selectedFile;
			if (sibr::showFilePicker(selectedFile, Default)) {
				if (!selectedFile.empty()) {
					// check if topview.txt exists
					std::ifstream topViewFile(selectedFile);
					if (topViewFile.good())
					{
						SIBR_LOG << "Loaded saved topview (" << selectedFile << ")." << std::endl;
						// Intialize a temp camera (used to load the saved top view pose) with
						// the current top view camera to get the resolution/fov right.
						InputCamera cam(_handler->getCamera());
						cam.readFromFile(topViewFile);
						// Apply it to the top view FPS camera.
						//camera_handler.fromCamera(cam, false);
						_handler->fromTransform(cam.transform(), false, true);
					}
				}
			}
		}

		if (ImGui::Button("Confirm Voxel")) {
			_debugView->removeMesh("Used Cams");
			_debugView->removeMesh("Non Used Cams");
			std::cout << _selectedFineVoxel << std::endl;
			_sliceIndices = _hybridScene->get_fine_grid().get_components(_selectedFineVoxel);
			_numSlices = _sliceIndices.size();
			//std::cout << _numSlices << std::endl;

			if (_numSlices > 0) {
				auto used_cams = std::make_shared<Mesh>(), non_used_cams = std::make_shared<Mesh>();
				int counter = 0;
				//int unused = 0;
				for (uint i = 0; i < _sliceIndices.size(); i++) {
					auto cam = _hybridScene->cameras()->inputCameras()[_sliceIndices[i]];
					//std::cout << i << ". SliceIndex: " << _sliceIndices[i] << "\tCam ID: " << cam->id() << std::endl;
					used_cams->merge(*generateCamFrustum(*cam, 0.0f, 0.5f));
				}
				_debugView->addMeshAsLines("Used Cams", used_cams).setColor({ 0,1,0 });
				updateHandler();
			}
		}
		ImGui::SameLine();
		if (ImGui::Button("Refine Voxel")) {
			std::cout << "Start time : "; time_now();
			refineGUI();
			std::cout << "\nEnd time : "; time_now();
		}
		ImGui::SameLine();
		if (ImGui::Button("Save PCloud")) {
			std::cout << "Start time : "; time_now();
			save_pcloud();
			std::cout << "\nEnd time : "; time_now();
		}
		ImGui::SameLine();
		if (ImGui::Button("Debug Voxel")) {
			debugSelectedVoxel();
		}
		ImGui::SameLine();
		ImGui::Checkbox("Log Selected Slice", &_logSelectedSlice);

		if (_numSlices > 0) {
			ImGui::SliderInt("Select Slice", &_selectedSlice, 0, _numSlices - 1);

			if (ImGui::SliderInt("Select Viewpoint", &_selectedView, 0, _numSlices - 1)) {
				updateHandler();
			}

			ImGui::SliderInt("View Mode", &_hybridRenderer->sliceDisp(), 0, 2);

		}

		if (ImGui::SliderInt(": Sub X", &_vX, 0, _hybridScene->get_ref_scale() - 1) ||
			ImGui::SliderInt(": Sub Y", &_vY, 0, _hybridScene->get_ref_scale() - 1) ||
			ImGui::SliderInt(": Sub Z", &_vZ, 0, _hybridScene->get_ref_scale() - 1)) {
			updateVoxel();
		}

		ImGui::End();
	}

}



void sibr::PerViewMeshDebugView::registerDebugObjects(MultiMeshManager::Ptr debugView, InteractiveCameraHandler::Ptr handler)
{
	_debugView = debugView;
	_handler = handler;


	if (_debugView) {
		_debugView->addMesh("Proxy", _hybridScene->proxies()->proxyPtr());
	}

}

void sibr::PerViewMeshDebugView::refineGUI()
{
	int counter;
	const int size = _hybridScene->get_grid().size().x();
	const int scale = _hybridScene->get_ref_scale();
	const int res_scale = 4;

	// Initialize all resized input images, and all destination images.
	// This could be done outside the voxel loop even
	// RAM heavy.
	const size_t numCams = _hybridScene->cameras()->inputCameras().size();
	std::vector<sibr::ImageRGB32F::Ptr> originalImgs(numCams);
	std::vector<RenderTargetRGBA32F::Ptr> sliceProjection(numCams);
	std::vector<sibr::Texture2DRGB::Ptr> outputView(numCams);
	RenderTargetRGBA32F::Ptr mipMap;
	int res = -1;
	for (size_t cid = 0; cid < numCams; ++cid) {
		const sibr::InputCamera& cam = *_hybridScene->cameras()->inputCameras()[cid];
		//const sibr::ImageRGB inputImage = _hybridScene->images()->inputImages()[cid]->resized(cam.w() / res_scale, cam.h() / res_scale);
		// Preconvert reference image to RGB32F.
		outputView[cid].reset(new Texture2DRGB(_hybridScene->images()->image((uint)cid), SIBR_GPU_LINEAR_SAMPLING | SIBR_CLAMP_UVS | SIBR_FLIP_TEXTURE));
		sliceProjection[cid].reset(new RenderTargetRGBA32F(cam.w() / res_scale, cam.h() / res_scale, SIBR_GPU_LINEAR_SAMPLING, 1));
		res = std::max(res, int(std::pow(2, 1 + int(floor(log2(std::max(cam.w() / res_scale, cam.h() / res_scale)))))));
		//mipMaps[cid].reset(new RenderTargetRGBA32F(res, res, SIBR_GPU_LINEAR_SAMPLING, 1));
	}
	mipMap.reset(new RenderTargetRGBA32F(res, res, SIBR_GPU_LINEAR_SAMPLING, 1));

	const Eigen::Vector3f voxel = Eigen::Vector3f(_selectedFineVoxel.x(), _selectedFineVoxel.y(), _selectedFineVoxel.z());
	const std::vector<int>& slices = _hybridScene->get_fine_grid().get_components(_selectedFineVoxel);
	const std::string outPath = _hybridScene->sceneArgs().dataset_path.get() + "/deep_blending/refined_pvmeshes/";
	const int numSlices = slices.size();
	if (numSlices > 0) {
		for (int i = 0; i < numSlices; ++i) {

			const int sliceIdx = slices[i];
			const size_t linear_idx = _hybridScene->get_fine_grid().linear_index(_selectedFineVoxel);
			// fetch the slice index given the voxel and the particular component
			// remember each slice can have multiple triangles

			const Eigen::Vector3f component_pos = _hybridScene->cameras()->inputCameras()[sliceIdx]->position();
			const Eigen::Vector3f voxel_center = _hybridScene->get_fine_grid().voxel_to_world(voxel);
			Eigen::Vector3f slice_to_center = component_pos - voxel_center;
			Eigen::Vector3f camUpVec = _hybridScene->cameras()->inputCameras()[sliceIdx]->up();
			Eigen::Vector2f devs;

			float sum = 0.0f;
			float max = 0.0f;
			const size_t slice_index = _hybridScene->get_components()[sliceIdx]->voxel_to_index_remap.find(linear_idx)->second;
			for (int j = 0; j < numSlices; ++j) {
				const int sid = slices[j];
				const sibr::InputCamera& cam = *_hybridScene->cameras()->inputCameras()[sid];
				
				const Eigen::Vector3f view_pos = cam.position();
				Eigen::Vector3f eye_to_center = view_pos - voxel_center;
				devs = vec2devs(eye_to_center, slice_to_center, camUpVec);
				const uint img_i = std::min(int(9 - 1), int(std::floor(9 * ((devs.x() / 180.0) + 0.5))));
				const uint img_j = std::min(int(9 - 1), int(std::floor(9 * ((devs.y() / 180.0) + 0.5))));
				const uint mapIdx = (img_i * 9) + img_j;

				const int ww = cam.w() / res_scale;
				const int hh = cam.h() / res_scale;
				//std::cout << "Cam_" + std::to_string(slices[j]) + "Slice_" + std::to_string(slices[i]) + ":\t";
				const float error = _hybridRenderer->refineSlices(cam, ww, hh,
					sliceIdx, outputView[sid], _selectedFineVoxel, *sliceProjection[sid], *mipMap,
					_hybridScene->renderTargets()->getInputRGBTextureArrayPtr());

				//std::cout << error << std::endl;
				/*_hybridScene->get_components()[sliceIdx]->populate_err(slice_index, slices[j], error);
				_hybridScene->get_components()[sliceIdx]->populate_errMap(slice_index, mapIdx, error);*/

				sum += error;
				max = std::max(max, error);
				//std::cout<< _hybridScene->get_components()[sliceIdx]->slice_errors[slice_index][slices[j]] << std::endl;
			}
			const float mean = sum / numSlices;
			const float var = simpleVariance(_hybridScene->get_components()[sliceIdx]->slice_errors[slice_index], mean);
			//_hybridScene->get_components()[sliceIdx]->populate_sliceStats(slice_index, mean, var, max);

		
		}

	}

	//save_pcloud();
	//std::cout << std::endl;

}

void sibr::PerViewMeshDebugView::time_now()
{
	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);
	std::cout << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << std::endl;
}

void sibr::PerViewMeshDebugView::refine()
{
	int counter = 0;
	int total_slices = 0;
	const int size = _hybridScene->get_grid().size().x();
	const int scale = _hybridScene->get_ref_scale();
	const int res_scale = 16;

	// Initialize all resized input images, and all destination images.
	// This could be done outside the voxel loop even
	// RAM heavy.
	const size_t numCams = _hybridScene->cameras()->inputCameras().size();
	//std::vector<sibr::ImageRGB32F::Ptr> originalImgs(numCams);
	//std::vector<RenderTargetRGBA32F::Ptr> sliceProjection(numCams);
	//std::vector<sibr::Texture2DRGB::Ptr> outputView(numCams);
	//RenderTargetRGBA32F::Ptr mipMap;
	int res = -1;
	//for (size_t cid = 0; cid < numCams; ++cid) {
	//	const sibr::InputCamera& cam = *_hybridScene->cameras()->inputCameras()[cid]; 
	//	//const sibr::ImageRGB inputImage = _hybridScene->images()->inputImages()[cid]->resized(cam.w() / res_scale, cam.h() / res_scale);
	//	// Preconvert reference image to RGB32F.
	//	outputView[cid].reset(new Texture2DRGB(_hybridScene->images()->image((uint)cid), SIBR_GPU_LINEAR_SAMPLING | SIBR_CLAMP_UVS | SIBR_FLIP_TEXTURE));
	//	sliceProjection[cid].reset(new RenderTargetRGBA32F(cam.w() / res_scale, cam.h() / res_scale, SIBR_GPU_LINEAR_SAMPLING, 1));
	//	res = std::max(res, int(std::pow(2, 1 + int(floor(log2(std::max(cam.w() / res_scale, cam.h() / res_scale)))))));
	//	//mipMaps[cid].reset(new RenderTargetRGBA32F(res, res, SIBR_GPU_LINEAR_SAMPLING, 1));
	//}
	//mipMap.reset(new RenderTargetRGBA32F(res, res, SIBR_GPU_LINEAR_SAMPLING, 1));

	
	for (int X = 0; X < size; ++X) {
		for (int Y = 0; Y < size; ++Y) {
			for (int Z = 0; Z < size; ++Z) {
				const Eigen::Vector3i selectedVoxel = Vector3i(X, Y, Z);
				std::cout << "\rVoxel : " << selectedVoxel << std::flush;
				for (int xx = 0; xx < scale; ++xx) {
					for (int yy = 0; yy < scale; ++yy) {
						for (int zz = 0; zz < scale; ++zz) {
							const int progress = xx + scale * (yy + scale * zz);
							//std::cout << "\rVoxel : " << selectedVoxel << "\tRefinement progress: " << counter * 100.0 / std::pow(scale, 3) << "%..." << std::flush;
							const Eigen::Vector3i offset = Vector3i(xx, yy, zz);
							const Eigen::Vector3i selectedFineVoxel = (selectedVoxel * scale) + offset;
	//						const Eigen::Vector3i selectedFineVoxel = Vector3i(97, 72, 20);
							const Eigen::Vector3f voxel = Eigen::Vector3f(selectedFineVoxel.x(), selectedFineVoxel.y(), selectedFineVoxel.z());

							const std::vector<int>& slices = _hybridScene->get_fine_grid().get_components(selectedFineVoxel);
							const std::string outPath = _hybridScene->sceneArgs().dataset_path.get() + "/deep_blending/refined_pvmeshes/";
							const int numSlices = slices.size();
							if (numSlices > 0) {
								counter++;
								total_slices += numSlices;
								for (int i = 0; i < numSlices; ++i) {

									const int sliceIdx = slices[i];
									const size_t linear_idx = _hybridScene->get_fine_grid().linear_index(selectedFineVoxel);
									// fetch the slice index given the voxel and the particular component
									// remember each slice can have multiple triangles
									const Eigen::Vector3f component_pos = _hybridScene->cameras()->inputCameras()[sliceIdx]->position();
									const Eigen::Vector3f voxel_center = _hybridScene->get_fine_grid().voxel_to_world(voxel);
									Eigen::Vector3f slice_to_center = component_pos - voxel_center;
									Eigen::Vector3f camUpVec = _hybridScene->cameras()->inputCameras()[sliceIdx]->up();
									Eigen::Vector2f devs;

									float sum = 0.0f;
									float max = 0.0f;
									const size_t slice_index = _hybridScene->get_components()[sliceIdx]->voxel_to_index_remap.find(linear_idx)->second;
									for (int j = 0; j < numSlices; ++j) {
										const int sid = slices[j];
										const sibr::InputCamera& cam = *_hybridScene->cameras()->inputCameras()[sid];
										const Eigen::Vector3f view_pos = cam.position();
										Eigen::Vector3f eye_to_center = view_pos - voxel_center;
										devs = vec2devs(eye_to_center, slice_to_center, camUpVec);
										const uint img_i = std::min(int(9 - 1), int(std::floor(9 * ((devs.x() / 180.0) + 0.5))));
										const uint img_j = std::min(int(9 - 1), int(std::floor(9 * ((devs.y() / 180.0) + 0.5))));
										const uint mapIdx = (img_i * 9) + img_j;
										const int ww = cam.w() / res_scale;
										const int hh = cam.h() / res_scale;
										//std::cout << "Cam_" + std::to_string(slices[j]) + "Slice_" + std::to_string(slices[i]) + ":\t";
										//const float error = _hybridRenderer->refineSlices(cam, ww, hh,
										//	sliceIdx, outputView[sid], selectedFineVoxel, *sliceProjection[sid], *mipMap,
										//	_hybridScene->renderTargets()->getInputRGBTextureArrayPtr());

										const float error = 1.0f;
										//std::cout << error << std::endl;
										//_hybridScene->get_components()[sliceIdx]->populate_err(slice_index, slices[j], error);
										//_hybridScene->get_components()[sliceIdx]->populate_errMap(slice_index, mapIdx, error);
										sum += error;
										max = std::max(max, error);
										//std::cout<< _hybridScene->get_components()[sliceIdx]->slice_errors[slice_index][slices[j]] << std::endl;
									}
									//const float mean = sum / numSlices;
									//const float var = simpleVariance(_hybridScene->get_components()[sliceIdx]->slice_errors[slice_index], mean);
									//_hybridScene->get_components()[sliceIdx]->populate_sliceStats(slice_index, mean, var, max);

								}

							}
						}
					}
				}
				//std::cout << std::endl;
			}
		}
	}
	save_pcloud();
	std::cout << "\nPatch Cloud Refinement Complete!" << std::endl;
	std::cout << "Total occupied subvoxels: " << counter << std::endl;
	std::cout << "Total slices refined: " << total_slices << std::endl;
}

void sibr::PerViewMeshDebugView::save_pcloud()
{
	_hybridScene->get_patch_cloud().update_components(_hybridScene->get_components());
	const std::string cloud_path = std::string(_hybridScene->sceneArgs().dataset_path.get() + "/deep_blending/output_refined.pcloud");
	_hybridScene->get_patch_cloud().save(cloud_path, _hybridScene->get_components(), _hybridScene->get_grid(), _hybridScene->get_ref_scale());

}


void sibr::PerViewMeshDebugView::debugSelectedVoxel()
{
	const std::vector<int> slices = _hybridScene->get_fine_grid().get_components(_selectedFineVoxel);
	const std::string outPath = _hybridScene->sceneArgs().dataset_path.get() + "/deep_blending/refined_pvmeshes/";
	const int numSlices = slices.size();
	_result.resize(numSlices);
	size_t linear_idx = _hybridScene->get_fine_grid().linear_index(_selectedFineVoxel);
	const Eigen::Vector3f voxel = Eigen::Vector3f(_selectedFineVoxel.x(), _selectedFineVoxel.y(), _selectedFineVoxel.z());
	std::ofstream table;
	table.open(outPath + "results_" + std::to_string(linear_idx) + ".txt");

	const std::vector<sibr::InputCamera::Ptr> cams = _hybridScene->cameras()->inputCameras();
	if (numSlices > 0) {
		for (int i = 0; i < numSlices; ++i) {
			const int sliceIdx = slices[i];
			size_t slice_index = _hybridScene->get_components()[sliceIdx]->voxel_to_index_remap.find(linear_idx)->second;
			const Eigen::Vector3f component_pos = cams[sliceIdx]->position();
			const Eigen::Vector3f voxel_center = _hybridScene->get_fine_grid().voxel_to_world(voxel);
			Eigen::Vector3f slice_to_center = component_pos - voxel_center;
			Eigen::Vector3f camUpVec = cams[sliceIdx]->up();
			Eigen::Vector2f devs;

			for (int j = 0; j < numSlices; ++j) {
				const Eigen::Vector3f view_pos = cams[slices[j]]->position();
				Eigen::Vector3f eye_to_center = view_pos - voxel_center;
				devs = vec2devs(eye_to_center, slice_to_center, camUpVec);
				const float err = _hybridScene->get_components()[sliceIdx]->slice_errors[slice_index][slices[j]];
				//_hybridScene->get_components()[sliceIdx]->populate_meanErr(slice_index, sum / numSlices);
				std::cout << "View " << j << "\tCamIdx " << slices[j] <<
					"\tError: " << err << 
					"\tDeviation: " << devs << std::endl;
			}
			const float var = _hybridScene->get_components()[sliceIdx]->slice_var[slice_index];
			const float max = _hybridScene->get_components()[sliceIdx]->slice_max[slice_index];
			const float mean = _hybridScene->get_components()[sliceIdx]->slice_mean[slice_index];

			std::cout << "Slice " << i << "\tCamIdx " << slices[i] << 
				"\tMean: " << mean << "\tVar: " << var << "\tMax: " << max << std::endl;
			std::cout << "==========================================================" << std::endl;
		}

		if (_logSelectedSlice) {
			//table << "Voxel: " << _selectedFineVoxel << std::endl;
			//for (int i = 0; i < numSlices; ++i) {
			//	table << "Slice:" << slices[i] << std::endl;
			//	for (auto& it : _result[i]) {
			//		table << "\t" << it.first << "\t" << it.second.first << "\tCam: " << it.second.second << std::endl;
			//		//std::cout << "\tDev: " << it.first << "\tErr: " << it.second.first << "\tCam: " << it.second.second << std::endl;
			//	}
			//}
		}
	}
	table.close();
}

void sibr::PerViewMeshDebugView::renderVisualisation()
{

	sibr::InputCamera generalCam = _handler->getCamera();
	sibr::Mesh::Ptr camMesh = generateCamFrustum(generalCam, 0.0f, 0.5f);
	//sibr::Mesh::Ptr visibleMesh;
	_debugView->addMeshAsLines("Scene Cam", camMesh).setColor({ 1, 0, 0 });

}

void sibr::PerViewMeshDebugView::initializeVoxelGrid()
{
	const Eigen::Vector3i size = _hybridScene->get_grid().size();
	const sibr::VoxelGridFRIBR grid = _hybridScene->get_grid();
	const Eigen::AlignedBox3f bBox(grid.min(), grid.max());
	const Eigen::Vector3i fine_size = _hybridScene->get_fine_grid().size();

	_vGrid.reset(new sibr::VoxelGrid<BasicVoxelType>(bBox, size, false));
	_vGridFine.reset(new sibr::VoxelGrid<BasicVoxelType>(bBox, fine_size, false));
}

void sibr::PerViewMeshDebugView::updateHandler()
{
	_handler->fromCamera(*_hybridScene->cameras()->inputCameras()[_sliceIndices[_selectedView]]);
}

void sibr::PerViewMeshDebugView::updateVoxel()
{
	_debugView->removeMesh("Selected Voxel");
	_debugView->removeMesh("Selected Sub-Voxel");
	std::vector<std::size_t> selectedVoxel, selectedFineVoxel;
	_selectedVoxel = sibr::Vector3i(_voxX, _voxY, _voxZ);
	_voxel = _hybridScene->get_grid().linear_index(_selectedVoxel);
	selectedVoxel.push_back(_voxel);

	int refScale = _hybridScene->get_ref_scale();

	sibr::Vector3i offset, fineVoxelStart;

	offset = Vector3i(_vX, _vY, _vZ);
	_selectedFineVoxel = (_selectedVoxel * refScale) + offset;
	size_t refinedVoxel = _hybridScene->get_fine_grid().linear_index(_selectedFineVoxel);
	selectedFineVoxel.push_back(refinedVoxel);

	sibr::Mesh::Ptr selectedVoxelMesh, selectedFineVoxelMesh;
	selectedVoxelMesh.reset(new sibr::Mesh());
	selectedFineVoxelMesh.reset(new sibr::Mesh());
	selectedVoxelMesh = _vGrid->getAllCellMeshWithIds(false, selectedVoxel);
	selectedFineVoxelMesh = _vGridFine->getAllCellMeshWithIds(false, selectedFineVoxel);

	std::cout << "Coarse Voxel: " << _selectedVoxel << std::endl;
	std::cout << "Fine Voxel: " << _selectedFineVoxel << std::endl;
	_debugView->addMeshAsLines("Selected Voxel", selectedVoxelMesh).setColor({ 1, 0, 1 });
	_debugView->addMeshAsLines("Selected Sub-Voxel", selectedFineVoxelMesh).setColor({ 1, 1, 0 });

	_sliceIndices = _hybridScene->get_fine_grid().get_components(_selectedFineVoxel);
	_numSlices = _sliceIndices.size();
}

float sibr::PerViewMeshDebugView::simpleVariance(std::vector<float> errors, const float mean)
{
	if (errors.size() <= 1) {
		return 0.0f;
	}

	const size_t numElements = errors.size();
	std::vector<double> diff(numElements);
	std::transform(errors.begin(), errors.end(), diff.begin(),
		std::bind2nd(std::minus<double>(), mean));
	const double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
	const float variance = std::sqrt(sq_sum / numElements);
	return variance;
}

Eigen::Vector2f sibr::PerViewMeshDebugView::vec2devs(Eigen::Vector3f& inputVec, Eigen::Vector3f& normalVec, Eigen::Vector3f& upVec)
{
	const Vector3f u = normalVec;
	const Vector3f v = upVec - ((upVec.dot(normalVec) / normalVec.squaredNorm()) * normalVec);
	const Vector3f w = u.cross(v);
	const float x = inputVec.dot((u / u.norm()));
	const float y = inputVec.dot((v / v.norm()));
	const float z = inputVec.dot((w / w.norm()));
	const float theta = SIBR_RADTODEG(acosf(z / std::sqrt(x * x + y * y + z * z))) - 90;
	const float phi = ((y - 0) < 0.0001) ? 0.0 : SIBR_RADTODEG(atanf(y / x));
	
	return Eigen::Vector2f(theta, phi);
}


