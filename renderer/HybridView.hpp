// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#pragma once

# include "Config.hpp"
# include <projects/hybrid_ibr/renderer/HybridRenderer.hpp>
# include <core/graphics/Mesh.hpp>
# include <core/view/ViewBase.hpp>
# include <core/raycaster/VoxelGrid.hpp>
# include <core/renderer/CopyRenderer.hpp>
# include <core/renderer/ColoredMeshRenderer.hpp>
# include <core/renderer/TexturedMeshRenderer.hpp>
# include <projects/ulr/renderer/ULRV3Renderer.hpp>
# include <core/renderer/PoissonRenderer.hpp>
# include <core/view/SceneDebugView.hpp>

#include "projects/hybrid_ibr/renderer/HybridScene.hpp"

namespace sibr { 

	/**
	 * \class ULRV3View
	 * \brief Wrap a ULR renderer with additional parameters and information.
	 */
	class SIBR_EXP_HYBRID_EXPORT HybridView : public sibr::ViewBase
	{
		SIBR_CLASS_PTR(HybridView);

		/// Rendering mode: default, use only one camera, use all cameras but one.
		enum RenderMode { ALL_CAMS, ONE_CAM, LEAVE_ONE_OUT, EVERY_N_CAM };

		/// Blending mode: keep the four best values per pixel, or aggregate them all.
		enum WeightsMode { ULR_W , VARIANCE_BASED_W, ULR_FAST};

		enum RenderLayer : int {
			MESH = 1,
			ULR = 2,
			PVM = 4,
			HYBRID = 8,
			DIFF = MESH | ULR | PVM
		};

	public:

		/**
		 * Constructor
		 * \param ibrScene The scene to use for rendering.
		 * \param render_w rendering width
		 * \param render_h rendering height
		 * \param viewName name of view
		 * \param ibrBasicScene Basic IBR scene to use for rendering, for example to show non-harmonized scene.
		 */
		HybridView(const sibr::HybridScene::Ptr& ibrScene, uint render_w, uint render_h, 
			const std::string viewName = "Hybrid View Settings", const sibr::BasicIBRScene::Ptr& ibrBasicScene = NULL);

		/** Replace the current scene.
		 *\param newScene the new scene to render */
		void setScene(const sibr::HybridScene::Ptr & newScene);

		/**
		 * Perform rendering. Called by the view manager or rendering mode.
		 * \param dst The destination rendertarget.
		 * \param eye The novel viewpoint.
		 */
		void onRenderIBR(sibr::IRenderTarget& dst, const sibr::Camera& eye) override;

		/**
		 * Update inputs (do nothing).
		 * \param input The inputs state.
		 */
		void onUpdate(Input& input) override;

		/**
		 * Update the GUI.
		 */
		void onGUI() override;

		/** \return a reference to the renderer. */
		const ULRV3Renderer::Ptr & getULRrenderer() const { return _ulrRenderer; }

		/** Set the renderer blending weights mode.
		 *\param mode the new mode to use
		 *\sa WeightsMode
		 **/
		void setMode(const WeightsMode mode);

		/** Set the renderer layer mode.
		 *\param mode the new mode to use
		 *\sa WeightsMode
		 **/
		void setLayerMode(const RenderLayer mode);

		std::string		getProfileString() { return profileStr; }

		void registerDebugObjects(MultiMeshManager::Ptr debugView, InteractiveCameraHandler::Ptr handler);
		
		void setHybridMask() {
			if (!_hybridRenderer->toggleMask()) 
			{ 
				_hybridRenderer->toggleMask() = true; 
			} 
		}

		void onRenderIBRDB(sibr::IRenderTarget& dst, const sibr::Camera& eye, const sibr::Texture2DRGBA::Ptr dbTex);

		void jitterCameraAdjustment(const sibr::ImageRGB::Ptr& gtImage, const sibr::Camera& cam,
			const int camID, std::string& results);

		void runParamSweepOnCamera(const sibr::ImageRGB::Ptr& gtImage, const sibr::Camera& cam, 
			const int camID, std::string& results, const std::string& fileName);

		double evalRMSE(const sibr::ImageRGB::Ptr& gt, const sibr::ImageRGB::Ptr& img);
		sibr::Vector3d getSSIM(const sibr::Vector3d& mean0, const sibr::Vector3d& mean1,
			const sibr::Vector3d& variance0, const sibr::Vector3d& variance1, const sibr::Vector3d& covariance);
		double gaussian(const double x, const double mu, const double sigma);
		double gaussian(const double x, const double sigma);
		double evalSSIM(ImageRGB::Ptr gt, ImageRGB::Ptr img);

		void createParamsForSweep();

		/** Resize the internal rendertargets.
		 *\param w the new width
		 *\param h the new height
		 **/
		void resize(const unsigned int w, const unsigned int h);
		

	protected:

		/**
		 * Update the camera informations in the ULR renderer based on the current rendering mode and selected index.
		 * \param mesh the mesh to render
		 * \param eye the current camera
		 * \param textureID the texture id
		 */
		void updateCameras(const sibr::Mesh& mesh,
			const sibr::Camera& eye, const uint textureID);
		void renderVisualisation();
		void initializeVoxelGrid();
		std::vector<std::size_t> detectNonEmptyVoxels();
		std::vector<std::size_t> detectVisibleVoxels();

	

		sibr::PatchCloud::DisplayMode _toDisplayMode[5] = 
		{
			sibr::PatchCloud::Colors,
			sibr::PatchCloud::Depth,
			sibr::PatchCloud::Normals,
			sibr::PatchCloud::OutputDir,
			sibr::PatchCloud::InputDir
		};
		
		sibr::MultiMeshManager::Ptr			_debugView;
		sibr::InteractiveCameraHandler::Ptr _handler;
		
		sibr::VoxelGrid<BasicVoxelType>::Ptr _vGrid;
		sibr::VoxelGrid<BasicVoxelType>::Ptr _vGridFine;

		int vox_x = 16, vox_y = 21, vox_z = 11;
		int vox_sx = 64, vox_sy = 84, vox_sz = 44;
		size_t _voxel;
		size_t _subvoxel;

		std::vector<PatchCloud::ImageBin>	_bins;


		std::string							_viewName;
		std::shared_ptr<sibr::BasicIBRScene>_scene; ///< The current basic scene.
		std::shared_ptr<sibr::HybridScene>	_hybridScene; ///< The current hybrid scene.

		
		HybridRenderer::Ptr					_hybridRenderer;  /// <Hybrid Renderer 
		ColoredMeshRenderer::Ptr			_coloredMeshRenderer; /// <Colored Mesh renderer
		TexturedMeshRenderer::Ptr			_texturedRenderer; /// <Textured Mesh renderer
		ULRV3Renderer::Ptr					_ulrRenderer; ///< The ULR renderer.
		PoissonRenderer::Ptr				_poissonRenderer; ///< The poisson filling renderer.

		std::vector< RenderTargetRGBA::Ptr> _diffTex; /// Destination to store textures used for difference
		RenderTargetRGBA::Ptr				_coloredMeshRT; ///< Texture to render per-vertex variance
		RenderTargetRGBA::Ptr				_texMeshRT;	///<TexturedMesh destination RT
		RenderTargetRGBA::Ptr				_ulrRenderRT;	///<TexturedMesh destination RT
		RenderTargetRGBA::Ptr				_ibrNextRT;	///<IBRNext destination RT
		RenderTargetRGBA::Ptr				_hybridRT;	///<Hybrid Rendering destination RT
		RenderTargetRGBA::Ptr				_blendRT; ///< ULR destination RT.
		RenderTargetRGBA::Ptr				_poissonRT; ///< Poisson filling destination RT.

		bool								_poissonBlend = true; ///< Should Poisson filling be applied.
		bool								_showVoxels = false;
		RenderLayer							_layerMode = RenderLayer::HYBRID;
		RenderMode							_renderMode = ALL_CAMS; ///< Current rendering mode.
		WeightsMode							_weightsMode = ULR_FAST; ///< Current blend weights mode.
		int									_singleCamId = 0; ///< Selected camera for the single view mode.
		int									_everyNCamStep = 1; ///< Camera step size for the every other N mode.
		int									_tex1 = 0, _tex2 = 1;
		int									_layerSelect = 0;
		int									_display_mode = 0;
		bool								_cameraReject = false;

		int									_slcSz = 9;
		int									_selectedView = 0;
		int									_selectedSlice = 0;
		int									_numSlices = -1;
		std::vector<int>					_sliceIndices;
		sibr::Mesh::Ptr						_selectedVoxelMesh;
		sibr::Mesh::Ptr						_selectedSubVoxelMesh;
		// **TO REMOVE
		// Patch cloud properties 
		int									_displayMode = static_cast<int>(sibr::PatchCloud::Colors);
		int									_displayLayer = 0;
		int									_clusterThreshold = 4;
		int									_clusterK = 12;
		int									_subClusterK = 12;

		bool								_useIOMultiPass = true;
		int									_blendIndex = 0;
		int									_depthIndex = 0;
		// TO REMOVE**

		bool								_showWireframe = false;
		float								_cameraScale = 0.5f;
		std::vector<std::vector<float>>		_params;

		int			_numFramesProfiling = 100;
		std::string profileStr = "";
		std::vector<float> _frameTimes;
		bool _monitoring = false;

	};

} /*namespace sibr*/ 
