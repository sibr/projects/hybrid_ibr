// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#include <HybridView.hpp>
#include <core/graphics/GUI.hpp>

sibr::HybridView::HybridView(const sibr::HybridScene::Ptr & ibrScene, uint render_w, uint render_h, 
	const std::string viewName, const sibr::BasicIBRScene::Ptr& ibrBasicScene) :
	_hybridScene(ibrScene),
	_scene(ibrBasicScene),
	sibr::ViewBase(render_w, render_h),
	_viewName(viewName)
{

	const uint w = render_w;
	const uint h = render_h;
	
	initializeVoxelGrid();
	//  Renderers.
	
	if (!ibrScene->sceneArgs().noPC && !ibrScene->get_components().empty()) {
		_hybridRenderer.reset(new HybridRenderer(ibrScene->cameras()->inputCameras(),
			ibrScene->get_components(),
			ibrScene->get_grid(), ibrScene->get_fine_grid(),
			w, h));
	}
	else {
		_hybridRenderer.reset(new HybridRenderer(ibrScene->cameras()->inputCameras(), w, h));
	}
	
	_coloredMeshRenderer.reset(new ColoredMeshRenderer());
	_texturedRenderer.reset(new TexturedMeshRenderer());
	_ulrRenderer.reset(new ULRV3Renderer(_hybridScene->cameras()->inputCameras(), w, h, "ulr/ulr_v3_fast"));
	_poissonRenderer.reset(new PoissonRenderer(w, h));
	_poissonRenderer->enableFix() = true;

	// Rendertargets.
	_diffTex.resize(4);
	for (uint i = 0; i < 4; i++) {
		_diffTex[i].reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
	}
	_hybridRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
	_ibrNextRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
	_coloredMeshRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
	_texMeshRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
	_ulrRenderRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
	_poissonRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
	_blendRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));

	// Tell the scene we are a priori using all active cameras for ULR Rendering.
	std::vector<uint> imgs_ulr;
	const auto & cams = _hybridScene->cameras()->inputCameras();
	for(size_t cid = 0; cid < cams.size(); ++cid) {
		if(cams[cid]->isActive()) {
			imgs_ulr.push_back(uint(cid));
		}
	}
	_hybridScene->cameras()->debugFlagCameraAsUsed(imgs_ulr);


}

void sibr::HybridView::setScene(const sibr::HybridScene::Ptr & newScene) {
	_hybridScene = newScene;
	const uint w = getResolution().x();
	const uint h = getResolution().y();

	std::string shaderName = "hybrid_composite";

	_hybridRenderer.reset(new HybridRenderer(newScene->cameras()->inputCameras(), w, h, shaderName));

	// Tell the scene we are a priori using all active cameras.
	std::vector<uint> imgs_ulr;
	const auto & cams = newScene->cameras()->inputCameras();
	for (size_t cid = 0; cid < cams.size(); ++cid) {
		if (cams[cid]->isActive()) {
			imgs_ulr.push_back(uint(cid));
		}
	}
	_hybridScene->cameras()->debugFlagCameraAsUsed(imgs_ulr);

}


void sibr::HybridView::onRenderIBR(sibr::IRenderTarget & dst, const sibr::Camera & eye)
{
	Eigen::Affine3f viewMat;
	viewMat.matrix() = eye.view();
	dst.clear();
	// Perform Textured Mesh rendering, either directly to the Textured Mesh RT, or to the intermediate RT when poisson blending is enabled.
	if (_layerMode & RenderLayer::MESH) {
	
		_hybridRenderer->clearVisibleVoxels();
		_texMeshRT->clear();
		_texturedRenderer->process(_hybridScene->proxies()->proxy(),
			eye,
			_hybridScene->inputMeshTextures()->handle(),
			*_texMeshRT,
			true);

		if (!_poissonBlend) {
			blit(*_texMeshRT, dst);
		}
		else {
			_poissonRenderer->process(_texMeshRT, _poissonRT);
			blit(*_poissonRT, dst);
		}
		_diffTex[0] = _texMeshRT;
	}

	// Perform ULR rendering, either directly to the ULR Render RT, or to the intermediate RT when poisson blending is enabled.
	if (_layerMode & RenderLayer::ULR) {
		
		//updateCameras(_hybridScene->proxies()->proxy(), eye, _hybridScene->inputMeshTextures()->handle());
	
		_ulrRenderer->process(
		_scene->proxies()->proxy(),
		eye,
		*_ulrRenderRT,
		_scene->renderTargets()->getInputRGBTextureArrayPtr(),
		_scene->renderTargets()->getInputDepthMapArrayPtr());
		
		if (!_poissonBlend) {
			blit(*_ulrRenderRT, dst);
		}
		else {
			_poissonRenderer->process(_ulrRenderRT, _poissonRT);
			blit(*_poissonRT, dst);
		}
		
		_diffTex[1] = _ulrRenderRT;
	}

	if (_layerMode & RenderLayer::PVM ) {

		_ibrNextRT->clear();
		_hybridRenderer->renderMode() = true;
		_hybridRenderer->geoMaskThresh() = 0.0f;
		_hybridRenderer->process(_hybridScene->proxies()->proxy(),
			eye,
			*_ibrNextRT,
			_hybridScene->inputMeshTextures()->handle(),
			_hybridScene->inputSpecularTextures()->handle(),
			_hybridScene->renderTargets()->getInputRGBTextureArrayPtr(),
			_hybridScene->renderTargets()->getInputDepthMapArrayPtr()
		);
		
		if (!_poissonBlend) {
			blit(*_ibrNextRT, dst);
		}
		else {
			_poissonRenderer->process(_ibrNextRT, _poissonRT);
			blit(*_poissonRT, dst);
		}

		_diffTex[2] = _ibrNextRT;
	}


	if (_layerMode == RenderLayer::HYBRID) {
		// Blend IBRNext, ULR and Textured Mesh based on the view-dependent map
		
		_hybridRenderer->renderMode() = false;

		//_hybridRenderer->geoMaskThresh() = 0.5f;
		_hybridRenderer->process(*_hybridScene->varianceMesh(),
			eye,
			*_hybridRT,
			_hybridScene->inputMeshTextures()->handle(),
			_hybridScene->inputSpecularTextures()->handle(),
			_hybridScene->renderTargets()->getInputRGBTextureArrayPtr(),
			_hybridScene->renderTargets()->getInputDepthMapArrayPtr()
		);

		if (!_poissonBlend) {
			blit(*_hybridRT, dst);
		}
		else {
			_poissonRenderer->process(_hybridRT, _poissonRT);
			blit(*_poissonRT, dst);
		}
		_diffTex[3] = _hybridRT;
	}

	//if (_layerMode == RenderLayer::DIFF) {
	//	if ((_tex1 != 2 && _tex2 != 2) || (_tex1 != 3 && _tex2 != 3)) {
	//		_hybridRenderer->clearVisibleVoxels();
	//	}
	//	// Show the difference between selected renderers using difference shader
	//	_hybridRenderer->process(dst, *_diffTex[_tex1], *_diffTex[_tex2]);
	//}

	// Perform Poisson blending if enabled and copy to the destination RT.

	if (_monitoring) {
		_frameTimes.push_back(ImGui::GetIO().DeltaTime * 1000.0f);
	}
}

void sibr::HybridView::onUpdate(Input & input)
{
	// Live shader reloading for debug.
	if (input.key().isReleased(Key::F8)) {

		const std::string shaderSrcPath = "../../src/projects/hybrid_ibr/renderer/shaders/";
		const std::string shaderDstPath = sibr::getShadersDirectory("hybrid_ibr") + "/";
		const auto files = sibr::listFiles(shaderSrcPath, false, false, { "vert", "frag", "geom" });
		for (const auto& file : files) {
			sibr::copyFile(shaderSrcPath + file, shaderDstPath + file, true);
		}

		
		_hybridRenderer->setupShaders();
		//_hybridRenderer->biFilter()->loadShaders();
	
	}
	

	// Visualisation.
	if (_debugView && _debugView->active()) {
		renderVisualisation();
	}
}

void sibr::HybridView::onGUI()
{
	if (ImGui::Begin(_viewName.c_str())) {

		if (_hybridScene->sceneArgs().compareMode) {
			// Rendering Layer Selection
			ImGui::RadioButton("Mesh", (int*)&_layerMode, RenderLayer::MESH); ImGui::SameLine();
			ImGui::RadioButton("ULR", (int*)&_layerMode, RenderLayer::ULR); ImGui::SameLine();
			if (!_hybridScene->sceneArgs().noPC) {
				ImGui::RadioButton("Hybrid", (int*)&_layerMode, RenderLayer::HYBRID);
			}
		}
		if (!_hybridScene->sceneArgs().noPC) {
			
			std::string algorithm;
			switch (_layerMode) 
			{
			case RenderLayer::MESH: algorithm = "Selected Algorithm: Textured Mesh";
				break;
			case RenderLayer::ULR: algorithm = "Selected Algorithm: Unstructured Lumigraph Rendering";
				break;
			case RenderLayer::PVM: algorithm = "Selected Algorithm: Per View Mesh Blending";
				break;
			case RenderLayer::HYBRID: algorithm = "Selected Algorithm: Hybrid Rendering";
				break;
			default: algorithm = "Unidentified Algorithm Selected!";
				break;
			}
			ImGui::Text(algorithm.c_str());
			ImGui::Separator();
			//ImGui::RadioButton("Difference", (int*)&_layerMode, RenderLayer::DIFF); ImGui::SameLine();
			//
			//ImGui::Separator();
			//ImGui::Text("Difference Algorithm"); ImGui::SameLine();
			//ImGui::PushScaledItemWidth(100);
			//ImGui::Combo("Algo 1", (int*)&_tex1, "TexturedMesh\0ULR\0IBRNext\0All\0\0"); ImGui::SameLine();
			//ImGui::Combo("Algo 2", (int*)&_tex2, "TexturedMesh\0ULR\0IBRNext\0All\0\0"); ImGui::SameLine();
			//ImGui::PopItemWidth();
			
			
			
			//ImGui::SliderFloat("Slice Angular Threshold", &_hybridRenderer->angThreshSlice(), 0.0f, 90.0f);
			//ImGui::PopItemWidth();
			ImGui::Separator();
			if (_debugView->active()) {
				ImGui::Checkbox("Show Grid and Selected Voxels", &_showVoxels);
				ImGui::Text("Select Voxel");
				ImGui::PushScaledItemWidth(100);
				ImGui::InputInt(": X", &vox_x, 1, 5); ImGui::SameLine();
				ImGui::InputInt(": Y", &vox_y, 1, 5); ImGui::SameLine();
				ImGui::InputInt(": Z", &vox_z, 1, 5);

				ImGui::InputInt(": x", &vox_sx, 1, 5); ImGui::SameLine();
				ImGui::InputInt(": y", &vox_sy, 1, 5); ImGui::SameLine();
				ImGui::InputInt(": z", &vox_sz, 1, 5);
				ImGui::PopItemWidth();

				if (ImGui::Button("Confirm Voxel")) {
					_debugView->removeMesh("Selected Voxel");
					std::vector<std::size_t> selectedVoxel;
					_voxel = _hybridScene->get_grid().linear_index(Vector3i(vox_x, vox_y, vox_z));
					selectedVoxel.push_back(_voxel);
					_selectedVoxelMesh.reset(new sibr::Mesh());
					_selectedVoxelMesh = _vGrid->getAllCellMeshWithIds(false, selectedVoxel);

					_debugView->addMeshAsLines("Selected Voxel", _selectedVoxelMesh).setColor({ 1, 0, 1 });
					_hybridRenderer->setVoxel(_voxel);
				}
				ImGui::SameLine();
				if (ImGui::Button("Confirm Sub-Voxel")) {
					_debugView->removeMesh("Selected Sub Voxel");
					std::vector<std::size_t> selectedSubVoxel;
					_subvoxel = _hybridScene->get_fine_grid().linear_index(Vector3i(vox_sx, vox_sy, vox_sz));
					selectedSubVoxel.push_back(_subvoxel);
					_selectedSubVoxelMesh.reset(new sibr::Mesh());
					_selectedSubVoxelMesh = _vGridFine->getAllCellMeshWithIds(false, selectedSubVoxel);

					_debugView->addMeshAsLines("Selected Sub Voxel", _selectedSubVoxelMesh).setColor({ 0, 0, 0 });
					_hybridRenderer->setSubVoxel(_subvoxel);
					_sliceIndices = _hybridScene->get_fine_grid().get_components(Vector3i(vox_sx, vox_sy, vox_sz));
					_numSlices = _sliceIndices.size();
				}
				ImGui::SameLine();

				if (ImGui::Button("Reset Voxel")) {
					_debugView->removeMesh("Selected Voxel");
					_debugView->removeMesh("Selected Sub Voxel");
					_hybridRenderer->resetVoxel(-1);
					_hybridRenderer->setSubVoxel(-1);
				}
				/*ImGui::SameLine();
				if (ImGui::Button("Debug log & images")) {
					_hybridRenderer->setDebugPath(_hybridScene->sceneArgs().dataset_path);
					_hybridRenderer->debugPrint();
				}*/
				ImGui::Separator();
			}
			//ImGui::SameLine();
			//ImGui::Checkbox("Reject Slice", &_hybridRenderer->rejectSlice());
			//if (ImGui::InputInt("Slice Error Map Size", &_slcSz, 1, 10)) {
			//	_hybridRenderer->setSliceErrMapSize(_slcSz);
			//}
			//if (_numSlices > 0) {
			//	if (ImGui::SliderInt("Select Slice", &_selectedSlice, 0, _numSlices - 1)) {
			//		_hybridRenderer->setSlice(_selectedSlice);
			//	}
			//
			//	if (ImGui::InputInt("Select Viewpoint", &_selectedView, 1, 5)) {
			//		if (_sliceIndices.size() > _selectedView) {
			//			_handler->fromCamera(*_hybridScene->cameras()->inputCameras()[_sliceIndices[_selectedView]]);
			//		}
			//	}
			//
			//}
			//
			//ImGui::PushScaledItemWidth(150);
			//ImGui::SliderFloat("Deviation Threshold", &_hybridRenderer->devThresh(), 0.0f, 180.0f);
			//ImGui::SameLine();
			//ImGui::SliderFloat("Slice Variance Threshold", &_hybridRenderer->sliceVarThresh(), 0.0f, 0.5f);
			//ImGui::PopItemWidth();
			if ((_layerMode & RenderLayer::PVM) || (_layerMode & RenderLayer::HYBRID)) {
				ImGui::Checkbox("Show Per-View Meshes", &_hybridRenderer->showSlices()); ImGui::SameLine();
				ImGui::PushScaledItemWidth(200);
				if (_hybridRenderer->showSlices()) {
					_poissonBlend = false;
					ImGui::InputInt(": Per-view Mesh (Camera) Index", &_hybridRenderer->displaySlice(), 1, 10);
				}
				else {
					_poissonBlend = true;
				}

				ImGui::PushScaledItemWidth(150);
				ImGui::Separator();
				ImGui::Text("Spatial Filter");
				ImGui::SliderInt("Kernel Radius", &_hybridRenderer->kernelSize(), 0, 50); ImGui::SameLine();
				ImGui::SliderFloat("Alpha", &_hybridRenderer->alphaThresh(), 0.0f, 10.0f);
				ImGui::SliderFloat("Blend weights", &_hybridRenderer->weightsExp(), 0.0f, 10.0f);
				//ImGui::Checkbox("Bilateral Filter Options", &_hybridRenderer->bilateralFilter());
				//if (_hybridRenderer->bilateralFilter()) {
				//
				//	ImGui::PushScaledItemWidth(150);
				//	ImGui::SameLine();
				//
				//	ImGui::SliderFloat("Filter Mask Threshold", &_hybridRenderer->filterMaskThresh(), 0.0f, 0.5f);
				//	ImGui::Checkbox("Variance guided sigma", &_hybridRenderer->varSigma());
				//	if (_hybridRenderer->varSigma()) {
				//		ImGui::SameLine();
				//		ImGui::Checkbox("Use Geometric Depth Diff", &_hybridRenderer->varMeasure());
				//	}
				//	ImGui::SliderInt("Show Filter layers (0=Filtered IBR)", &_hybridRenderer->biFilter()->displayFilterLayer(), 0, 4);
				//	ImGui::PopItemWidth();
				//}

				//ImGui::InputFloat("Hybrid Epsilon occlusion", &_hybridRenderer->epsilonOcclusion(), 0.001f, 0.01f);
				//ImGui::Checkbox("Hybrid Occlusion Testing", &_hybridRenderer->occTest());
				//ImGui::Checkbox("Hybrid Disble Rendering", &_hybridRenderer->renderMode());
				//ImGui::Checkbox("Hybrid Debug weights", &_hybridRenderer->showWeights());


				// Camera selection settings
				ImGui::Separator();
				ImGui::InputFloat("IBR Depth Threshold", &_hybridRenderer->depthThreshold(), 0.05f, 0.25f);

				ImGui::PushScaledItemWidth(400);
				//if (ImGui::SliderInt("Cluster Threshold", &_clusterThreshold, 0, 24)) {
				//	_hybridRenderer->cluster_threshold((int)_clusterThreshold);
				//}

				if (ImGui::SliderInt("Cluster K", &_clusterK, 0, 24)) {
					_hybridRenderer->cluster_k((int)_clusterK);
				}


				//if (ImGui::SliderInt("Sub Cluster K", &_subClusterK, 0, 24)) {
				//	_hybridRenderer->cluster_subk((int)_subClusterK);
				//}
				ImGui::PopItemWidth();
				//// PV Meshes settings

				//ImGui::Separator();
				//ImGui::Text("Display mode:");                       ImGui::SameLine();
				//ImGui::RadioButton("Colors", &_display_mode, 0); ImGui::SameLine();
				//ImGui::RadioButton("Depth", &_display_mode, 1); ImGui::SameLine();
				//ImGui::RadioButton("Normals", &_display_mode, 2); ImGui::SameLine();
				//ImGui::RadioButton("Output dir", &_display_mode, 3); ImGui::SameLine();
				//ImGui::RadioButton("Input dir", &_display_mode, 4);


				//ImGui::PushScaledItemWidth(400);
				//ImGui::SliderInt("Num Iter", &_hybridRenderer->numIter(), 0, _hybridRenderer->getLayers()); 
				//ImGui::SliderInt("Show Nth layer", &_hybridRenderer->displayLayer(), 0, _hybridRenderer->getLayers());
				//ImGui::PopItemWidth();
				//ImGui::Separator();
				//ImGui::Checkbox("Use IO MultiPass", &_hybridRenderer->useIOMulti());
				//ImGui::PushScaledItemWidth(150);
				//ImGui::Combo("Blending heuristic", (int*)&_hybridRenderer->blendIdx(), "ULR\0None\0\0");
				//ImGui::PopItemWidth();
				//ImGui::Text("Depth heuristic"); ImGui::SameLine();
				//ImGui::RadioButton("Depth Test", (int*)&_hybridRenderer->depthIdx(), 0); ImGui::SameLine();
				//ImGui::RadioButton("Fuzzy Test", (int*)&_hybridRenderer->depthIdx(), 1); ImGui::SameLine();
				//ImGui::RadioButton("No Test", (int*)&_hybridRenderer->depthIdx(), 2);

				ImGui::Separator();
				ImGui::Checkbox("Enable GPU Sorting ", &_hybridRenderer->gpuSort());
				
				
				ImGui::Separator();
				ImGui::Text("Voxel Selection");
				ImGui::SliderFloat("Tolerance", &_hybridRenderer->tolerance(), 0.0f, 0.5f);
				ImGui::Checkbox("Neighborhood Select", &_hybridRenderer->neighborhood());
				ImGui::Checkbox("Clear Voxel Memory", &_hybridRenderer->updateVoxelMemory());
				
				// Hybrid Settings.
				ImGui::Separator();

				if (_layerMode == RenderLayer::HYBRID) {
					ImGui::Checkbox("Show Hybrid Masks ", &_hybridRenderer->toggleMask());
					ImGui::InputFloat("Photometric Threshold", &_hybridRenderer->varMaskThresh(), 0.001f, 0.01f);
					ImGui::InputFloat("Geometric Threshold", &_hybridRenderer->geoMaskThresh(), 0.001f, 0.01f);
					ImGui::SliderInt("Mask Kernel Radius", &_hybridRenderer->maskKernelSize(), 0, 50);
					ImGui::InputFloat("Composite Margin", &_hybridRenderer->compositeMargin(), 0.01f, 0.1f);
				}
				ImGui::PopItemWidth();

			}
			
		}

		// ULR settings.
		if ((_layerMode & RenderLayer::ULR)) {
		
			// Switch the shaders for ULR rendering.
			if (ImGui::Combo("Weights mode", (int*)(&_weightsMode), "Standard ULR\0Variance based\0Fast ULR\0\0")) {
				setMode(_weightsMode);
			}
		}


		ImGui::Separator();
		// Poisson settings.
		ImGui::Checkbox("Poisson ", &_poissonBlend); ImGui::SameLine();
		ImGui::Checkbox("Poisson fix", &_poissonRenderer->enableFix());


		ImGui::End();
	}

	if (ImGui::Begin("Profiling")) {


		static std::string perfStr;
		ImGui::Text("Select #frames to profile");
		ImGui::RadioButton("100", (int*)&_numFramesProfiling, 100); ImGui::SameLine();
		ImGui::RadioButton("250", (int*)&_numFramesProfiling, 300); ImGui::SameLine();
		ImGui::RadioButton("500", (int*)&_numFramesProfiling, 500); 

		ImGui::Separator();
		if (!_monitoring) {

			if (ImGui::Button("Start monitoring") ) {
				if (_layerMode & RenderLayer::ULR) {
					_ulrRenderer->startProfile();
				}
				else if (_layerMode & RenderLayer::HYBRID) {
					_hybridRenderer->profile() = true;
				}
				//_hybridRenderer->startProfiling();
				_monitoring = true;
				_frameTimes.clear();
				perfStr = "";
			}
		}
		else {
			if (_frameTimes.size() == _numFramesProfiling) {
				if (_layerMode & RenderLayer::ULR) {
					_ulrRenderer->stopProfile();
				}
				else if (_layerMode & RenderLayer::HYBRID) {
					_hybridRenderer->profile() = false;
					_hybridRenderer->stopProfiling();
				}
				_monitoring = false;
				// Compute statistic on the timings for each step and display them.
				const std::vector<std::string> names = { "Frame" };
				const  std::vector<std::vector<float>> counts = {
					_frameTimes
				};
				perfStr = "";
				
				for (int i = 0; i < names.size(); ++i) {
					// Compute metrics: min, max, avg, variance.
					double miniF = std::numeric_limits<double>::max();
					double maxiF = 0.0;
					double avgF = 0.0;
					for (size_t tid = 0; tid < counts[i].size(); ++tid) {
						const double ft = double(counts[i][tid]);
						avgF += ft;
						miniF = std::min(miniF, ft);
						maxiF = std::max(maxiF, ft);
					}
					avgF /= double(counts[i].size());
					double varF = 0.0;
					for (size_t tid = 0; tid < counts[i].size(); ++tid) {
						const double residualF = double(counts[i][tid]) - avgF;
						varF += residualF * residualF;
					}
					varF /= double(int(counts[i].size()) - 1);
					perfStr += "Includes GUI overhead!\nPlease check logs for detailed breakdown of each step!!\n";
					perfStr += names[i] + " num frames: " + std::to_string(counts[i].size()) + "\n";
					perfStr += names[i] + " min/max: " + std::to_string(miniF) + "/" + std::to_string(maxiF) + "\n";
					perfStr += names[i] + " avg/stddev: " + std::to_string(avgF) + "/" + std::to_string(std::sqrt(varF)) + "\n";
				}
				//_hybridRenderer->stopProfiling();
			}
			
			profileStr = perfStr;
		}

		ImGui::Text(profileStr.c_str());
		if (ImGui::Button("Copy perf string")) {
			ImGui::SetClipboardText(profileStr.c_str());
		}
	}
	ImGui::End();


}

void sibr::HybridView::setLayerMode(const RenderLayer mode)
{
	_layerMode = mode;
}

void sibr::HybridView::registerDebugObjects(MultiMeshManager::Ptr debugView, InteractiveCameraHandler::Ptr handler)
{
	_debugView = debugView;
	_handler = handler;

	if (_debugView) {
		_debugView->addMesh("Proxy", _hybridScene->proxies()->proxyPtr());
	}
}

void sibr::HybridView::onRenderIBRDB(sibr::IRenderTarget& dst, const sibr::Camera& eye, const sibr::Texture2DRGBA::Ptr dbTex)
{
	// Render ULR Background
	_ulrRenderer->process(
		_hybridScene->proxies()->proxy(),
		eye,
		*_ulrRenderRT,
		_hybridScene->renderTargets()->getInputRGBTextureArrayPtr(),
		_hybridScene->renderTargets()->getInputDepthMapArrayPtr());

	// Composite ULR with DB based on margin
	_hybridRenderer->process(_hybridScene->proxies()->proxy(),
		eye, *_hybridRT, *_ulrRenderRT, dbTex);

	blit(*_hybridRT, dst);
	if (_poissonBlend){
		_poissonRenderer->process(_hybridRT, _poissonRT);
		blit(*_poissonRT, dst);
	}

}

void sibr::HybridView::jitterCameraAdjustment(const sibr::ImageRGB::Ptr& gtImage, const sibr::Camera& cam, const int camID, std::string& results)
{

	sibr::RenderTargetRGBA::Ptr outFrame;
	sibr::ImageRGB::Ptr outImage;

	outFrame.reset(new RenderTargetRGBA(gtImage->w(), gtImage->h()));
	outImage.reset(new ImageRGB(outFrame->w(), outFrame->h()));
	sibr::Camera currentCam = cam;
	
	std::map<double, sibr::Vector3f> errorsToIdx;
	
	float dx = 0.0;
	float dy = 0.0;
	float dz = 0.0;

	Vector3f curPos = cam.position();
	Vector3f newPos = curPos + Vector3f(dx, dy, dz);
	currentCam.set(newPos, currentCam.rotation());
	
	outFrame->clear();
	_ulrRenderer->process(
		_hybridScene->proxies()->proxy(),
		currentCam,
		*outFrame,
		_hybridScene->renderTargets()->getInputRGBTextureArrayPtr(),
		_hybridScene->renderTargets()->getInputDepthMapArrayPtr());


	// Readback rendered image
	outFrame->readBack(*outImage);
	double error = evalRMSE(gtImage, outImage);
	errorsToIdx.insert(std::pair<double, sibr::Vector3f>(error, Vector3f(dx, dy, dz)));
	//results += "Original Error: " + std::to_string(error) + "\n";
	for (int i = 0; i < 1000; i++) {
		dx = 0.0 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 0.02));
		dy = 0.0 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 0.015));
		dz = 0.0 + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 0.1));

		newPos = curPos + Vector3f(dx, dy, dz);
		currentCam.set(newPos, currentCam.rotation());
		outFrame->clear();
		_ulrRenderer->process(
			_hybridScene->proxies()->proxy(),
			currentCam,
			*outFrame,
			_hybridScene->renderTargets()->getInputRGBTextureArrayPtr(),
			_hybridScene->renderTargets()->getInputDepthMapArrayPtr());


		// Readback rendered image
		outFrame->readBack(*outImage);
		double error = evalRMSE(gtImage, outImage);
		errorsToIdx.insert(std::pair<double, sibr::Vector3f>(error, Vector3f(dx, dy, dz)));
	}

	for (auto& it : errorsToIdx) {
		results += std::to_string(it.second.x()) + " " + std::to_string(it.second.y()) + " " + std::to_string(it.second.z()) + "\n";
		break;
	}
}

void sibr::HybridView::runParamSweepOnCamera(const sibr::ImageRGB::Ptr& gtImage,
	const sibr::Camera& cam, const int camID, std::string& results, const std::string& fileName)
{
	sibr::RenderTargetRGBA::Ptr midFrame;
	sibr::RenderTargetRGBA::Ptr outFrame;
	sibr::ImageRGB::Ptr outImage;
	midFrame.reset(new RenderTargetRGBA(gtImage->w(), gtImage->h()));
	outFrame.reset(new RenderTargetRGBA(gtImage->w(), gtImage->h()));
	outImage.reset(new ImageRGB(outFrame->w(), outFrame->h()));
	
	//std::cout << "Number of configs: " << _params.size() << std::endl;
	results = "Cam " + std::to_string(camID) + "\n";
	//if (camID == 0) {
	//	gtImage->save(parentDirectory(fileName) + "/Cam0.png", false);
	//}
	std::map<double, int> errorsToIdx;
	// Set the parameters
	_hybridRenderer->renderMode() = false;
	//_hybridRenderer->toggleMask() = true;
	for (int i = 0; i < _params.size(); ++i) {
		_hybridRenderer->depthThreshold() = _params[i][0];
		_hybridRenderer->varMaskThresh() = _params[i][1];
		_hybridRenderer->geoMaskThresh() = _params[i][2];
		_hybridRenderer->kernelSize() = _params[i][3];
		_hybridRenderer->alphaThresh() = _params[i][4];
		_hybridRenderer->weightsExp() = _params[i][5];
		_hybridRenderer->maskKernelSize() = _params[i][6];

		// Render with current parameters
		_hybridRenderer->process(*_hybridScene->varianceMesh(),
			cam,
			*midFrame,
			_hybridScene->inputMeshTextures()->handle(),
			_hybridScene->inputSpecularTextures()->handle(),
			_hybridScene->renderTargets()->getInputRGBTextureArrayPtr(),
			_hybridScene->renderTargets()->getInputDepthMapArrayPtr()
		);

		_poissonRenderer->process(midFrame, _poissonRT);
		blit(*_poissonRT, *outFrame);

		// Readback rendered image
		outFrame->readBack(*outImage);
		//if (camID == 0) {
		//	std::ostringstream ssZeroPad;
		//	ssZeroPad << std::setw(3) << std::setfill('0') << i;
		//	std::string fName = parentDirectory(fileName) + "/" + std::to_string(camID) + "_" + ssZeroPad.str() + ".png";
		//	//std::string fName = parentDirectory(fileName) + "/mask_" + std::to_string(camID) + "_" + ssZeroPad.str() + ".png";
		//	outImage->save(fName);
		//	//std::cout << fName << std::endl;;
		//}

		// Compute DSSIM between GT and rendered image
		double ssim = evalSSIM(gtImage, outImage);
		double error = (1 - ssim) / 2;
		//std::cout << "Config " << i << " DSSIM: " << error << std::endl;
		// Compute RMSE between GT and rendered image
		//double error = evalRMSE(gtImage, outImage);
		errorsToIdx.insert(std::pair<double, int>(error, i));
	}
	//int ctr = 0;

	for (auto& idx : errorsToIdx) {
		//std::cout << "Cam " << camID << "\tConfig " << idx.second << "\tRMSE: " << idx.first << std::endl;
		for (int j = 0; j < _params[0].size(); j++) {
			results += std::to_string(_params[idx.second][j]) + "\t\t";
		}
		results += "DSSIM: " + std::to_string(idx.first) + "\n";
	}
	
}

double sibr::HybridView::evalRMSE(const sibr::ImageRGB::Ptr& gt, const sibr::ImageRGB::Ptr& img)
{
	double accum = 0.0;
	for (uint y = 0; y < gt->h(); ++y) {
		for (uint x = 0; x < gt->w(); ++x) {
			sibr::Vector3d dif = gt(x, y).cast<double>() - img(x, y).cast<double>();
			dif /= 255.0;
			accum += dif.squaredNorm();
		}
	}
	const double mse = accum / (gt->h() * gt->w() * 3);

	return std::sqrt(mse);
}

sibr::Vector3d sibr::HybridView::getSSIM(const sibr::Vector3d& mean0, const sibr::Vector3d& mean1,
	const sibr::Vector3d& variance0, const sibr::Vector3d& variance1, const sibr::Vector3d& covariance)
{
	// compute SSIM
	const double k1 = 1e-2;//0.01;
	const double k2 = 3e-2;//0.03;
	const double L = 1; // value range (2^8 - 1 => 255, we have 0-1 => 1
	const double c1c = (k1 * L) * (k1 * L);
	const double c2c = (k2 * L) * (k2 * L);
	const sibr::Vector3d c1 = { c1c, c1c, c1c };
	const sibr::Vector3d c2 = { c2c, c2c, c2c };

	const sibr::Vector3d ssim = ((2.0 * mean0.cwiseProduct(mean1) + c1).cwiseProduct(2 * covariance + c2)).cwiseQuotient(((mean0.cwiseProduct(mean0) + mean1.cwiseProduct(mean1) + c1).cwiseProduct(variance0 + variance1 + c2)));

	return ssim;
}

double sibr::HybridView::gaussian(const double x, const double mu, const double sigma)
{
	return 1.0 / (sigma * std::sqrt(2.0 * M_PI)) * std::exp((-0.5 * ((x - mu) * (x - mu))) / (sigma * sigma));
}

double sibr::HybridView::gaussian(const double x, const double sigma)
{
	return gaussian(x, 0, sigma);
}

double sibr::HybridView::evalSSIM(ImageRGB::Ptr gt, ImageRGB::Ptr img)
{


	double accum = 0.0;
	double dssim = 0.0;
	for (int py = 0; py < gt->h(); ++py) {
		for (int px = 0; px < gt->w(); ++px) {
			const sibr::Vector3d centerColor0 = gt(px, py).cast<double>() / 255.0;
			const sibr::Vector3d centerColor1 = img(px, py).cast<double>() / 255.0;

			sibr::Vector3d ssimOutput(0.0, 0.0, 0.0);

			//ivec2 position = ivec2(gl_FragCoord.xy);
			//ivec2 resolution = min(textureSize(image0, 0), textureSize(image1, 0));
			const int radius = 2;
			const double domainStdDev = std::max(radius, 1) / 3.2f;

			sibr::Vector3d mean0(0.0, .0, .0);
			sibr::Vector3d mean0Squared(0.0, .0, .0);
			sibr::Vector3d mean1(0.0, .0, .0);
			sibr::Vector3d mean1Squared(0.0, .0, .0);
			sibr::Vector3d mean0x1(0.0, .0, .0);
			double weightSum = 0;

			for (int y = -radius; y <= radius; y++)
			{
				for (int x = -radius; x <= radius; x++)
				{
					sibr::Vector2i newCoords = { px + x, py + y };
					if (newCoords[0] < 0 || newCoords[1] < 0 ||
						newCoords[0] >= gt->w() || newCoords[1] >= gt->h()) {
						continue;
					}

					sibr::Vector3d color0 = gt(newCoords).cast<double>() / 255.0;
					sibr::Vector3d color1 = img(newCoords).cast<double>() / 255.0;
					const sibr::Vector2d offset(x, y);
					double dWeight = gaussian(offset.norm(), domainStdDev);
					double weight = dWeight;

					mean0 += weight * color0;
					mean0Squared += weight * color0.cwiseProduct(color0);

					mean1 += weight * color1;
					mean1Squared += weight * color1.cwiseProduct(color1);

					mean0x1 += weight * color0.cwiseProduct(color1);

					weightSum += weight;
				}
			}

			// normalize
			if (weightSum < 1e-10) continue;
			mean0 /= weightSum;
			mean0Squared /= weightSum;
			mean1 /= weightSum;
			mean1Squared /= weightSum;
			mean0x1 /= weightSum;

			// compute variances and covariance
			const sibr::Vector3d variance0 = mean0Squared - mean0.cwiseProduct(mean0);
			const sibr::Vector3d variance1 = mean1Squared - mean1.cwiseProduct(mean1);
			const sibr::Vector3d covariance = mean0x1 - mean0.cwiseProduct(mean1);

			// compute SSIM
			ssimOutput = getSSIM(mean0, mean1, variance0, variance1, covariance);
			accum += ssimOutput[0] + ssimOutput[1] + ssimOutput[2];

		}
	}


	return accum / (gt->h() * gt->w() * 3);
}

void sibr::HybridView::createParamsForSweep()
{
	SIBR_LOG << "[HybridView] Creating Parameters Config for Sweep!!" << std::endl;

	// Block 1
	std::vector<float> depth_alpha = { 0.5, 0.75, 0.85, 1.0 };
	std::vector<float> photo_thresh = { 0.5, 0.75, 0.85, 0.95, 1.0 };
	std::vector<float> geo_thresh = { 0.0, 0.01, 0.03, 0.05, 0.1 };
	std::vector<int>   kernel_radius = { 10 };
	std::vector<int>   spatialWeights_alpha = { 5 };
	std::vector<int>   weights_exps = { 5};
	std::vector<int>   mask_kernel_radius = { 9 };

	std::vector<float> config;
	
	for (int idx1 = 0; idx1 < depth_alpha.size(); ++idx1) {
		config.clear();
		config.push_back(depth_alpha[idx1]);
		for (int idx2 = 0; idx2 < photo_thresh.size(); ++idx2) {
			config.push_back(photo_thresh[idx2]);
			for (int idx3 = 0; idx3 < geo_thresh.size(); ++idx3) {
				config.push_back(geo_thresh[idx3]);
				for (int idx4 = 0; idx4 < kernel_radius.size(); ++idx4) {
					config.push_back(kernel_radius[idx4]);
					for (int idx5 = 0; idx5 < spatialWeights_alpha.size(); ++idx5) {
						config.push_back(spatialWeights_alpha[idx5]);
						for (int idx6 = 0; idx6 < weights_exps.size(); ++idx6) {
							config.push_back(weights_exps[idx6]);
							for (int idx7 = 0; idx7 < mask_kernel_radius.size(); ++idx7) {
								config.push_back(mask_kernel_radius[idx7]);
								_params.push_back(config);
								config.pop_back();
							}
							config.pop_back();
						}
						config.pop_back();
					}
					config.pop_back();
				}
				config.pop_back();
			}
			config.pop_back();
		}
		config.pop_back();
	}

	//// Block 2
	kernel_radius = { 7, 10, 12 };
	spatialWeights_alpha = { 1, 3, 5, 7 };
	weights_exps = { 1, 3, 5, 7 };
	depth_alpha = { 0.75 };
	photo_thresh = { 0.85 };
	geo_thresh = { 0.0 };
	mask_kernel_radius = { 9 };
	
	for (int idx1 = 0; idx1 < depth_alpha.size(); ++idx1) {
		config.clear();
		config.push_back(depth_alpha[idx1]);
		for (int idx2 = 0; idx2 < photo_thresh.size(); ++idx2) {
			config.push_back(photo_thresh[idx2]);
			for (int idx3 = 0; idx3 < geo_thresh.size(); ++idx3) {
				config.push_back(geo_thresh[idx3]);
				for (int idx4 = 0; idx4 < kernel_radius.size(); ++idx4) {
					config.push_back(kernel_radius[idx4]);
					for (int idx5 = 0; idx5 < spatialWeights_alpha.size(); ++idx5) {
						config.push_back(spatialWeights_alpha[idx5]);
						for (int idx6 = 0; idx6 < weights_exps.size(); ++idx6) {
							config.push_back(weights_exps[idx6]);
							for (int idx7 = 0; idx7 < mask_kernel_radius.size(); ++idx7) {
								config.push_back(mask_kernel_radius[idx7]);
								_params.push_back(config);
								config.pop_back();
							}
							config.pop_back();
						}
						config.pop_back();
					}
					config.pop_back();
				}
				config.pop_back();
			}
			config.pop_back();
		}
		config.pop_back();
	}

	// Block 3
	mask_kernel_radius = { 0, 4, 9, 12 };
	depth_alpha = { 0.75};
	photo_thresh = { 0.85};
	geo_thresh = { 0.03 };
	kernel_radius = { 10};
	spatialWeights_alpha = { 5 };
	weights_exps = { 5};
	
	for (int idx1 = 0; idx1 < depth_alpha.size(); ++idx1) {
		config.clear();
		config.push_back(depth_alpha[idx1]);
		for (int idx2 = 0; idx2 < photo_thresh.size(); ++idx2) {
			config.push_back(photo_thresh[idx2]);
			for (int idx3 = 0; idx3 < geo_thresh.size(); ++idx3) {
				config.push_back(geo_thresh[idx3]);
				for (int idx4 = 0; idx4 < kernel_radius.size(); ++idx4) {
					config.push_back(kernel_radius[idx4]);
					for (int idx5 = 0; idx5 < spatialWeights_alpha.size(); ++idx5) {
						config.push_back(spatialWeights_alpha[idx5]);
						for (int idx6 = 0; idx6 < weights_exps.size(); ++idx6) {
							config.push_back(weights_exps[idx6]);
							for (int idx7 = 0; idx7 < mask_kernel_radius.size(); ++idx7) {
								config.push_back(mask_kernel_radius[idx7]);
								_params.push_back(config);
								config.pop_back();
							}
							config.pop_back();
						}
						config.pop_back();
					}
					config.pop_back();
				}
				config.pop_back();
			}
			config.pop_back();
		}
		config.pop_back();
	}

	std::ofstream output_params;
	const std::string fName = parentDirectory(_hybridScene->data()->meshPath()) + "/../params.txt";
	output_params.open(fName);
	
	for (int i = 0; i < _params.size(); ++i) {
		for (int j = 0; j < _params[i].size(); j++) {
			output_params << _params[i][j] << "\t";
		}
		output_params << std::endl;
	}
	std::cout << _params.size() << " configurations created!! Configs saved in file "<< fName << std::endl;
	output_params.close();
}

void sibr::HybridView::resize(const unsigned int w, const unsigned int h)
{
	// Rendertargets.
	_diffTex.resize(4);
	for (uint i = 0; i < 4; i++) {
		_diffTex[i].reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
	}
	_hybridRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
	_ibrNextRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
	_coloredMeshRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
	_texMeshRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
	_ulrRenderRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
	_poissonRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
	_blendRT.reset(new RenderTargetRGBA(w, h, SIBR_CLAMP_UVS));
}

void sibr::HybridView::updateCameras(const sibr::Mesh& mesh,
	const sibr::Camera& eye, const uint textureID)
{
	std::vector<uint> imgs_ulr;
	if (_cameraReject) {
		//_hybridRenderer->visibleCameraSelection(imgs_ulr, mesh, eye, textureID);
	}
	else {
		_hybridRenderer->clearVisibleVoxels();
		const auto& cams = _hybridScene->cameras()->inputCameras();
		for (size_t cid = 0; cid < cams.size(); ++cid) {
			if (cams[cid]->isActive()) {
				imgs_ulr.push_back(uint(cid));
			}
		}
	}
	// Only update if there is at least one camera enabled.
	if (!imgs_ulr.empty()) {
		//std::cout << "\rUsing " << imgs_ulr.size() << " cameras for ULR!!!" << std::flush;
		// Update the shader informations in the renderer.
		_ulrRenderer->updateCameras(imgs_ulr);
		// Tell the scene which cameras we are using for debug visualization.
		_hybridScene->cameras()->debugFlagCameraAsUsed(imgs_ulr);
	}
}

void sibr::HybridView::setMode(const WeightsMode mode)
{
	_weightsMode = mode;
	if (_weightsMode == VARIANCE_BASED_W) {
		_ulrRenderer->setupShaders("ulr/ulr_v3_alt");
	}
	else if (_weightsMode == ULR_FAST) {
		_ulrRenderer->setupShaders("ulr/ulr_v3_fast");
	}
	else {
		_ulrRenderer->setupShaders();
	}
}

void sibr::HybridView::renderVisualisation()
{

	sibr::InputCamera generalCam = _handler->getCamera();
	sibr::Mesh::Ptr camMesh = generateCamFrustum(generalCam, 0.0f, _cameraScale);
	sibr::Mesh::Ptr visibleMesh;
	_debugView->addMeshAsLines("Scene Cam", camMesh).setColor({ 1, 0, 0});
	
	if (_showVoxels) {
		std::vector<std::size_t> visibleVoxels = detectVisibleVoxels();
		visibleMesh.reset(new sibr::Mesh());
		visibleMesh = _vGrid->getAllCellMeshWithIds(false, visibleVoxels);
		
		if (_debugView->getMeshData("Visible Voxels").meshType == MeshData::MeshType::DUMMY) {
			_debugView->addMeshAsLines("Visible Voxels", visibleMesh).setColor({ 0, 1, 0 });
		}
		_debugView->addMeshAsLines("Occupied Grid", 
			_vGridFine->getAllCellMeshWithIds(false, detectNonEmptyVoxels())).setColor({ 0, 0, 1 }).setAlpha(0.5);
		_debugView->getMeshData("Visible Voxels").meshPtr = visibleMesh;
	}
	else {
		if (_debugView->getMeshData("Occupied Grid").meshType != MeshData::MeshType::DUMMY ||
			_debugView->getMeshData("Visible Voxels").meshType == MeshData::MeshType::DUMMY) {
			_debugView->removeMesh("Occupied Grid");
			_debugView->removeMesh("Visible Voxels");
		}
	}
	
}

void sibr::HybridView::initializeVoxelGrid()
{
	const Eigen::Vector3i size = _hybridScene->get_grid().size();
	const sibr::VoxelGridFRIBR grid = _hybridScene->get_grid();
	const Eigen::AlignedBox3f bBox(grid.min(), grid.max());
	
	_vGrid.reset(new sibr::VoxelGrid<BasicVoxelType>(bBox, size, false));
	_vGridFine.reset(new sibr::VoxelGrid<BasicVoxelType>(bBox, _hybridScene->get_fine_grid().size(), false));
}

std::vector<std::size_t> sibr::HybridView::detectNonEmptyVoxels()
{
	const Eigen::Vector3i size = _hybridScene->get_fine_grid().size();
	sibr::VoxelGridFRIBR grid = _hybridScene->get_fine_grid();
	
	std::vector<std::size_t> out_ids;
	for (int z = 0; z < size.z(); ++z) {
		for (int y = 0; y < size.y(); ++y) {
			for (int x = 0; x < size.x(); ++x)
			{
				if (!grid.get_components(Eigen::Vector3i(x, y, z)).empty()) {
					const size_t cellID = _vGridFine->getCellId(sibr::Vector3i(x, y, z));
					out_ids.push_back(cellID);
				}
			}
		}
	}
	//SIBR_LOG << "Number of occupied grid: " << out_ids.size() << std::endl;
	return out_ids;
}

std::vector<std::size_t> sibr::HybridView::detectVisibleVoxels()
{
	
	const Eigen::Vector3i size = _hybridScene->get_grid().size();
	sibr::VoxelGridFRIBR grid = _hybridScene->get_grid();

	std::vector<std::size_t> out_ids;
	if (!_hybridRenderer->getVoxels().empty()) {
		for (int z = 0; z < size.z(); ++z) {
			for (int y = 0; y < size.y(); ++y) {
				for (int x = 0; x < size.x(); ++x)
				{
					const size_t cellID = _vGrid->getCellId(sibr::Vector3i(x, y, z));
					if (_hybridRenderer->getVoxels()[cellID]) {
						out_ids.push_back(cellID);
					}
				}
			}
		}
	}
	else {
		out_ids.clear();
	}

	return out_ids;
}
