// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#include "patch_cloud.h"
#include "GL/glu.h"

#include "projects/fribr_framework/renderer/tools/file_tools.h"
#include "projects/fribr_framework/renderer/tools/gl_tools.h"
#include "projects/fribr_framework/renderer/tools/image_tools.h"
#include "projects/fribr_framework/renderer/tools/sized_priority_queue.h"
#include "projects/fribr_framework/renderer/tools/profiling.h"
#include "projects/fribr_framework/renderer/tools/geometry_tools.h"
#include "projects/inside_out_deep_blending/renderer/simple_ransac.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic warning "-fpermissive"


#include <MixKit/stdmix.h>
#include "MixKit/MxStdModel.h"
#include "MixKit/MxQSlim.h"

#pragma GCC diagnostic pop


#include <cmath>
#include <fstream>
#include <memory>
#include <sstream>
#include <unordered_map>
#include <unordered_set>

#define DO_PROFILING 0
#ifdef INRIA_WIN
size_t doAtomicOp(size_t* progress, int val);
#endif

namespace
{

	void write_float(std::ofstream &file, float f)
	{
		file.write((char*)&f, 1 * sizeof(float) / sizeof(char));
	}

	void write_float2(std::ofstream &file, fribr::float2 f)
	{
		file.write((char*)&f, 2 * sizeof(float) / sizeof(char));
	}

	void write_float3(std::ofstream &file, fribr::float3 f)
	{
		file.write((char*)&f, 3 * sizeof(float) / sizeof(char));
	}

	void write_float4(std::ofstream &file, fribr::float4 f)
	{
		file.write((char*)&f, 4 * sizeof(float) / sizeof(char));
	}

	float read_float(std::ifstream &file)
	{
		float buff[1];
		file.read((char*)buff, 1 * sizeof(float) / sizeof(char));
		return buff[0];
	}

	fribr::float2 read_float2(std::ifstream &file)
	{
		float buff[2];
		file.read((char*)buff, 2 * sizeof(float) / sizeof(char));
		return fribr::make_float2(buff[0], buff[1]);
	}

	fribr::float3 read_float3(std::ifstream &file)
	{
		float buff[3];
		file.read((char*)buff, 3 * sizeof(float) / sizeof(char));
		return fribr::make_float3(buff[0], buff[1], buff[2]);
	}

	fribr::float4 read_float4(std::ifstream &file)
	{
		float buff[4];
		file.read((char*)buff, 4 * sizeof(float) / sizeof(char));
		return fribr::make_float4(buff[0], buff[1], buff[2], buff[3]);
	}

	void write_int(std::ofstream &file, int i)
	{
		file.write((char*)&i, 1 * sizeof(int) / sizeof(char));
	}

	void write_int2(std::ofstream &file, fribr::int2 i)
	{
		file.write((char*)&i, 2 * sizeof(int) / sizeof(char));
	}

	void write_int3(std::ofstream &file, fribr::int3 i)
	{
		file.write((char*)&i, 3 * sizeof(int) / sizeof(char));
	}

	int read_int(std::ifstream &file)
	{
		int buff[1];
		file.read((char*)buff, 1 * sizeof(int) / sizeof(char));
		return buff[0];
	}

	fribr::int2 read_int2(std::ifstream &file)
	{
		int buff[2];
		file.read((char*)buff, 2 * sizeof(int) / sizeof(char));
		return fribr::make_int2(buff[0], buff[1]);
	}

	fribr::int3 read_int3(std::ifstream &file)
	{
		int buff[3];
		file.read((char*)buff, 3 * sizeof(int) / sizeof(char));
		return fribr::make_int3(buff[0], buff[1], buff[2]);
	}

	void copy_mesh(MxStdModel *from, MxStdModel *to)
	{
		for (size_t i = 0; i < from->vert_count(); ++i)
			to->add_vertex(from->vertex(i)[0], from->vertex(i)[1], from->vertex(i)[2]);
		for (size_t i = 0; i < from->face_count(); ++i)
		{
			if (!from->face_is_valid(i))
				continue;
			to->add_face(from->face(i)[0], from->face(i)[1], from->face(i)[2]);
		}
	}

}

namespace sibr
{

	PatchCloud::Component::Component(VoxelGridFRIBR *_grid)
		: grid(_grid),
		vertices_start(0)
	{
	}

	PatchCloud::Component::~Component()
	{
	}


	// works with a mesh (no code for meshing and simplification): This was used in deep-blending.
	//// [SP] To save a per-view mesh (or input tiles) as a component of the grid
	//// [SP] Each component corresponds to a particular camera
	//// [SP] and contains the input tiles (triangles corresponding to the camera)
	//// [SP] The voxel locations are found based on the camera position
	//// [SP] as they are indexed using a linear_index function based on the position of the "" as:
	//// [SP] voxel.x() + m_size.x() * (voxel.y() + m_size.y() * voxel.z());

	void PatchCloud::Component::add_camera(fribr::Mesh::Ptr mesh, 
		sibr::InputCamera::Ptr& calibrated_camera, const size_t component_index, const int num_cams)
	{
		
		//// [SP] Why is the image position used when we have camera_position?
		//// [SP] It's the same thing.
		position = calibrated_camera->position();
		resolution = Eigen::Vector2i(calibrated_camera->w(), calibrated_camera->h());

		//Eigen::Matrix3f intrinsics = calibrated_camera.get_intrinsics();
		Eigen::Matrix4f world_to_camera = calibrated_camera->view();

		assert(mesh);
		assert(!mesh->get_indices().empty());
		assert(!mesh->get_vertices().empty());


		//// [SP] What does this do?
		//// [SP] Populate the component vertices and corresponding texture coordinates.
		MxStdModel model(mesh->get_vertices().size() / 3, mesh->get_indices().size() / 3);
		for (size_t i = 0; i < mesh->get_vertices().size(); i += 3)
			model.add_vertex(mesh->get_vertices()[i + 0],
				mesh->get_vertices()[i + 1],
				mesh->get_vertices()[i + 2]);
		
		for (size_t i = 0; i < mesh->get_indices().size(); i += 3)
		{
			model.add_face(mesh->get_indices()[i + 0],
				mesh->get_indices()[i + 1],
				mesh->get_indices()[i + 2]);

			for (size_t j = 0; j < 3; ++j)
			{
				size_t ii = mesh->get_indices()[i + j];
				vertices.emplace_back(fribr::make_float3(mesh->get_vertices()[ii * 3 + 0],
					mesh->get_vertices()[ii * 3 + 1],
					mesh->get_vertices()[ii * 3 + 2]));

				//// [SP] Calculate texture coordinate in image space for the corresponding vertex we populated
				fribr::float3   v = vertices.back();
				sibr::Vector3f vertex = Vector3f(v.x, v.y, v.z);
				//Eigen::Vector4f v_cam = world_to_camera * Eigen::Vector4f(v.x, v.y, v.z, 1.0f);
				//Eigen::Vector3f v_img = intrinsics * v_cam.head<3>();
				sibr::Vector3f uv = calibrated_camera->project(vertex);
				uv = (0.5f * (uv + sibr::Vector3f(1, 1, 1)));
				//sibr::Vector3f uv = calibrated_camera->projectImgSpaceInvertY(vertex);
				fribr::float2   v_tex = fribr::make_float2(uv.x(), uv.y());
				v_tex = (uv.x() <= 0) ? fribr::make_float2(0, uv.y()) : v_tex;
				v_tex = (uv.y() <= 0) ? fribr::make_float2(uv.x(), 0) : v_tex;
				
				texcoords.emplace_back(fribr::make_float3(v_tex.x, v_tex.y, component_index));
			}
		}

		
		

		// [SP/PH] Split the per-view mesh into slices, by checking which voxels intersect with the triangle.
		// [SP/PH] A slice is a part of a per-view mesh (Component) that intersects with one world-space voxel.

		for (size_t i = 0; i < vertices.size(); i += 3)
		{
			//// [SP] We rasterize the triangle made up of 3 contiguous vertices and find the voxel in which it lies
			//// [SP] We return the voxel location as index parameter 
			//// [SP] which is used to create the map between voxel and the triangle slice
			grid->rasterize_triangle(fribr::make_vec3f(vertices[i + 0]),
				fribr::make_vec3f(vertices[i + 1]),
				fribr::make_vec3f(vertices[i + 2]),
				[&](Eigen::Vector3i index) {
				size_t linear_index = grid->linear_index(index);
				//// [SP] voxel_to_index_remap is an unordered map between voxel_index and triangle_slices
				//// [SP] Using the map, we should get the triangle_slices contained in that voxel
				//// [SP] If current voxel index is not present in the map; i.e. the voxel is encountered for the first time
				if (voxel_to_index_remap.find(linear_index) == voxel_to_index_remap.end())
				{
					//// [SP] 1. Create the triangle slice which stores the vertex location
					slice_indices.push_back(std::vector<uint32_t>());
					//// [SP] 2. Add the voxel location to the corresponding slice_voxel list to easily fetch the voxel 
					slice_voxels.push_back(index);
					//// [SP] 3. Mark the voxel as valid for rendering which will be used in culling while online rendering
					slice_valid.push_back(true);
					//// [SP] 4. Store the mean error of the slice during refinement
					slice_mean.push_back(0.0f);
					//// [SP] 5. Store the mean error of the slice during refinement
					slice_var.push_back(0.0f);
					//// [SP] 6. Store the mean error of the slice during refinement
					slice_max.push_back(0.0f);
					//// [SP] 7. Store the errors of the slice reprojected in each viewpoint
					slice_error_map.push_back(std::vector<float>(81, 0.0f));
					//// [SP] 8. Store the errors of the slice reprojected in each viewpoint
					slice_errors.push_back(std::vector<float>(num_cams, 0.0f));
					//// [SP] Create the map from voxel index to slice index
					voxel_to_index_remap[linear_index] = slice_indices.size() - 1;
				}
				//// [SP] Add the 3 vertex ids to the triangle slice
				size_t compressed_index = voxel_to_index_remap[linear_index];

				slice_indices[compressed_index].push_back(i + 0);
				slice_indices[compressed_index].push_back(i + 1);
				slice_indices[compressed_index].push_back(i + 2);
			});
		}

		// Detect voxels that have discontinuities in them, and make sure to use more slices when rendering these parts of the scene.
		MxVertexList star;
		MxFaceList faces;
		for (size_t i = 0; i < mesh->get_indices().size(); i++)
		{
			size_t ii = mesh->get_indices()[i];
			Eigen::Vector3f v(model.vertex(ii)[0], model.vertex(ii)[1], model.vertex(ii)[2]);
			Eigen::Vector3i voxel = grid->world_to_voxel(v);

			size_t linear_idx = grid->linear_index(voxel);
			if (voxel_to_index_remap.find(linear_idx) == voxel_to_index_remap.end())
				continue;

			size_t index = voxel_to_index_remap[linear_idx];
			star.reset();
			model.collect_vertex_star(ii, star);

			for (uint j = 0; j < star.length(); j++)
				if (ii < star(j))
				{
					faces.reset();
					model.collect_edge_neighbors(ii, star(j), faces);

					// number of neighbors along the edge == 1 -> This vertex is at a discontinuity.
					if (faces.length() != 1)
						continue;

					slice_valid[index] = false;

					size_t index_a = model.face(faces(0))[0];
					size_t index_b = model.face(faces(0))[1];
					size_t index_c = model.face(faces(0))[2];
					grid->rasterize_triangle(
						Eigen::Vector3f(model.vertex(index_a)[0],
							model.vertex(index_a)[1],
							model.vertex(index_a)[2]),
						Eigen::Vector3f(model.vertex(index_b)[0],
							model.vertex(index_b)[1],
							model.vertex(index_b)[2]),
						Eigen::Vector3f(model.vertex(index_c)[0],
							model.vertex(index_c)[1],
							model.vertex(index_c)[2]),
						[&](Eigen::Vector3i index) {
						size_t linear_index = grid->linear_index(index);
						size_t compressed_index = voxel_to_index_remap[linear_index];
						slice_valid[compressed_index] = false;
					});
				}
		}
	}


	bool PatchCloud::Component::is_valid(Eigen::Vector3i voxel) const
	{
		size_t linear_idx = grid->linear_index(voxel);
		if (voxel_to_index_remap.find(linear_idx) == voxel_to_index_remap.end())
			return false;

		size_t slice_index = voxel_to_index_remap.find(linear_idx)->second;
		return slice_valid[slice_index];
	}

	size_t PatchCloud::Component::draw(Eigen::Vector3i voxel) const
	{
		size_t linear_idx = grid->linear_index(voxel);
		if (voxel_to_index_remap.find(linear_idx) == voxel_to_index_remap.end())
			return 0;

		size_t slice_index = voxel_to_index_remap.find(linear_idx)->second;
		size_t num_vertices = slice_indices[slice_index].size();
		glDrawElements(GL_TRIANGLES, num_vertices, GL_UNSIGNED_INT,
			(void*)(sizeof(uint32_t) * slices_start[slice_index]));

		return num_vertices / 3;
	}

	size_t PatchCloud::Component::draw() const
	{
		glDrawArrays(GL_TRIANGLES, vertices_start, vertices.size());
		return vertices.size() / 3;
	}

	void PatchCloud::Component::save(std::ofstream &out_file) const
	{
		// Support legacy viewers, just write zeroes instead of the world_to_camera matrix
		write_float4(out_file, fribr::make_float4(0.0f, 0.0f, 0.0f, 0.0f));
		write_float4(out_file, fribr::make_float4(0.0f, 0.0f, 0.0f, 0.0f));
		write_float4(out_file, fribr::make_float4(0.0f, 0.0f, 0.0f, 0.0f));
		write_float4(out_file, fribr::make_float4(0.0f, 0.0f, 0.0f, 0.0f));

		// ... the same applies for the intrinsics matrix
		write_float3(out_file, fribr::make_float3(0.0f, 0.0f, 0.0f));
		write_float3(out_file, fribr::make_float3(0.0f, 0.0f, 0.0f));
		write_float3(out_file, fribr::make_float3(0.0f, 0.0f, 0.0f));

		write_int2(out_file, fribr::make_int2(resolution));
		write_float3(out_file, fribr::make_float3(position));

		write_int(out_file, slice_indices.size());
		for (size_t i = 0; i < slice_indices.size(); ++i)
		{
			write_int3(out_file, fribr::make_int3(slice_voxels[i]));
			write_int(out_file, int(slice_valid[i]));

			write_int(out_file, int(slice_errors[i].size()));
			out_file.write((char*)slice_errors[i].data(),
				slice_errors[i].size() * sizeof(float) / sizeof(char));

			write_int(out_file, int(slice_error_map[i].size()));
			out_file.write((char*)slice_error_map[i].data(),
				slice_error_map[i].size() * sizeof(float) / sizeof(char));

			write_int(out_file, slice_indices[i].size());
			out_file.write((char*)slice_indices[i].data(),
				slice_indices[i].size() * sizeof(int32_t) / sizeof(char));

			write_float(out_file, float(slice_mean[i]));

			write_float(out_file, float(slice_var[i]));

			write_float(out_file, float(slice_max[i]));
		}

		write_int(out_file, vertices.size());
		out_file.write((char*)vertices.data(), vertices.size() * 3 * sizeof(float) / sizeof(char));

		write_int(out_file, texcoords.size());
		out_file.write((char*)texcoords.data(), texcoords.size() * 3 * sizeof(float) / sizeof(char));
	}

	void PatchCloud::Component::load(std::ifstream &in_file)
	{
		// Support legacy file formats, just skip over the world_to_camera matrix
		read_float4(in_file);
		read_float4(in_file);
		read_float4(in_file);
		read_float4(in_file);

		// ... also skip the intrinsics matrix.
		read_float3(in_file);
		read_float3(in_file);
		read_float3(in_file);

		resolution = fribr::make_vec2i(read_int2(in_file));
		position = fribr::make_vec3f(read_float3(in_file));

		int num_slices = read_int(in_file);
		slice_indices.assign(num_slices, std::vector<uint32_t>());
		slice_errors.assign(num_slices, std::vector<float>());
		slice_error_map.assign(num_slices, std::vector<float>());
		voxel_to_index_remap.clear();
		slice_voxels.clear();
		slice_valid.clear();
		slice_mean.clear();
		slice_var.clear();
		slice_max.clear();
		for (int i = 0; i < num_slices; ++i)
		{
			fribr::int3 slice_voxel = read_int3(in_file);
			slice_voxels.push_back(fribr::make_vec3i(slice_voxel));

			size_t linear_idx = grid->linear_index(fribr::make_vec3i(slice_voxel));
			voxel_to_index_remap[linear_idx] = i;

			int is_valid = read_int(in_file);
			slice_valid.push_back(bool(is_valid));

			int num_errors = read_int(in_file);
			slice_errors[i].resize(num_errors, 0.0f);
			in_file.read((char*)slice_errors[i].data(), num_errors * sizeof(float) / sizeof(char));
			
			int num_error_map = read_int(in_file);
			slice_error_map[i].resize(num_error_map, 0.0f);
			in_file.read((char*)slice_error_map[i].data(), num_error_map * sizeof(float) / sizeof(char));
			
			int num_indices = read_int(in_file);
			slice_indices[i].resize(num_indices, 0);
			in_file.read((char*)slice_indices[i].data(), num_indices * sizeof(int32_t) / sizeof(char));

			slice_mean.push_back(read_float(in_file));

			slice_var.push_back(read_float(in_file));

			slice_max.push_back(read_float(in_file));
		}

		int num_vertices = read_int(in_file);
		vertices.resize(num_vertices, fribr::make_float3(0.0f, 0.0f, 0.0f));
		in_file.read((char*)vertices.data(), num_vertices * 3 * sizeof(float) / sizeof(char));

		int num_texcoords = read_int(in_file);
		texcoords.resize(num_texcoords, fribr::make_float3(0.0f, 0.0f, 0.0f));
		in_file.read((char*)texcoords.data(), num_texcoords * 3 * sizeof(float) / sizeof(char));
	}

	PatchCloud::ImageBin::ImageBin(int bin_x, int bin_y, int bin_z, const VoxelGridFRIBR &grid)
		: voxel(bin_x, bin_y, bin_z)
	{
		Eigen::Array3f bbox_min_grid(bin_x, bin_y, bin_z);
		Eigen::Array3f bbox_max_grid(bin_x + 1.0f, bin_y + 1.0f, bin_z + 1.0f);

		Eigen::Array3f bbox_min_unit = bbox_min_grid / grid.size().array().cast<float>();
		Eigen::Array3f bbox_max_unit = bbox_max_grid / grid.size().array().cast<float>();

		bbox_min = bbox_min_unit * (grid.max() - grid.min()) + grid.min();
		bbox_max = bbox_max_unit * (grid.max() - grid.min()) + grid.min();
	}

	PatchCloud::PatchCloud()
		: 
		m_display_layer(0),
		m_display_mode(Colors),
		m_cluster_threshold(4),
		m_cluster_k(std::max(12, m_cluster_threshold)),
		m_all_triangles(0),
		m_grid(Eigen::Vector3i(32, 32, 32)),
		m_gridFine(Eigen::Vector3i(32, 32, 32)),
		m_update_voxel_memory(true)
	{
		
	}

	PatchCloud::PatchCloud(Eigen::Vector3i _grid_size)
		: m_display_layer(0),
		m_display_mode(Colors),
		m_cluster_threshold(4),
		m_cluster_k(std::max(12, m_cluster_threshold)),
		m_all_triangles(0),
		m_grid(_grid_size),
		m_gridFine(_grid_size),
		m_update_voxel_memory(true)
	{

	}

	PatchCloud::~PatchCloud()
	{

		clear_gpu();
	}
	
	void PatchCloud::add_cameras(std::vector<sibr::InputCamera::Ptr>& cameras, std::vector<fribr::Mesh::Ptr>& meshes, MemoryPolicy policy)
	{
		m_components.clear();
		m_all_triangles = 0;

		for (size_t i = 0; i < cameras.size(); ++i)
		{
			Component::Ptr component(new Component(&m_gridFine));
			m_components.push_back(component);
			
		}

		size_t progress = 0;
		for (int i = 0; i < cameras.size(); ++i)
		{
			sibr::InputCamera::Ptr	cam = cameras[i];
			if (cam->isActive()) {
				fribr::Mesh::Ptr       mesh = meshes[i];

				//Eigen::Matrix4f world_to_cam = cam->view();
				//Eigen::Matrix4f cam_to_world = world_to_cam.inverse();
				// cv::imread(file_path, cv::IMREAD_COLOR);
				//std::string img_path = images_folder_path + cam->name();

				m_components[i]->add_camera(mesh, cameras[i], i, cameras.size());

				if (policy == DeallocateWhenDone)
					mesh.reset();

				auto critical_section = [&]()
				{
					for (Eigen::Vector3i v : m_components[i]->slice_voxels)
						m_gridFine.add_component(v, i);

					m_all_triangles += m_components[i]->vertices.size() / 3;
					progress++;
					std::cout << "\rCreating ibr meshes: " << progress * 100.0f / cameras.size()
						<< "%                      " << std::flush;
				};
#pragma omp critical
				critical_section();
			}
		}
	}

	void PatchCloud::update_components(std::vector<PatchCloud::Component::Ptr>& components)
	{
		m_components = components;
	}

	void PatchCloud::clear_gpu()
	{

	}
	
	void PatchCloud::clear()
	{
		m_scene.reset();
		m_meshes.clear();
		clear_voxel_memory();
		clear_gpu();
		m_grid.clear();
		m_components.clear();
		m_all_triangles = 0;
	}

	void PatchCloud::set_scene(sibr::Mesh::Ptr mesh)
	{
		m_grid.set_scene(mesh);
		m_gridFine.set_scene(mesh);
	}

	void PatchCloud::set_scene(sibr::Mesh::Ptr mesh, Eigen::Array3f grid_min, Eigen::Array3f grid_max) {
		m_grid.set_scene(mesh);
		m_grid.min(grid_min);
		m_grid.max(grid_max);
		m_gridFine.set_scene(mesh);
		m_gridFine.min(grid_min);
		m_gridFine.max(grid_max);
	}

	void PatchCloud::set_refinementScale(int scale)
	{
		m_refinement_scale = scale;
		m_gridFine.resize(m_grid.size() * scale);
	}

	void PatchCloud::save(const std::string& filename) const
	{
		std::ofstream out_file(filename, std::ofstream::trunc | std::ofstream::binary);

		if (!out_file.good())
		{
			std::cout << "Could not open file for writing: " << filename << std::endl;
			return;
		}
		out_file << "PC10" << std::endl;

		write_float3(out_file, fribr::make_float3(Eigen::Vector3f(m_grid.min().matrix())));
		write_float3(out_file, fribr::make_float3(Eigen::Vector3f(m_grid.max().matrix())));
		write_int3(out_file, fribr::make_int3(m_grid.size()));
		write_int(out_file, m_refinement_scale);

		// Begin components
		write_int(out_file, m_components.size());
		for (Component::Ptr component : m_components)
			component->save(out_file);
	}

	void PatchCloud::save(const std::string& filename, std::vector<PatchCloud::Component::Ptr>& m_components, sibr::VoxelGridFRIBR& m_grid, 
		int refinementScale) 
	{
		std::ofstream out_file(filename, std::ofstream::trunc | std::ofstream::binary);

		if (!out_file.good())
		{
			std::cout << "Could not open file for writing: " << filename << std::endl;
			return;
		}
		out_file << "PC10" << std::endl;

		write_float3(out_file, fribr::make_float3(Eigen::Vector3f(m_grid.min().matrix())));
		write_float3(out_file, fribr::make_float3(Eigen::Vector3f(m_grid.max().matrix())));
		write_int3(out_file, fribr::make_int3(m_grid.size()));
		write_int(out_file, refinementScale);

		// Begin components
		write_int(out_file, m_components.size());
		for (Component::Ptr component : m_components)
			component->save(out_file);
	}

	bool PatchCloud::load(const std::string& filename, std::vector<PatchCloud::Component::Ptr> &components, sibr::VoxelGridFRIBR& grid,
		sibr::VoxelGridFRIBR& fineGrid, int& refinementScale)
	{
		if (!fribr::file_exists(filename))
		{
			std::cout << "No such file: " << filename << std::endl;
			return false;
		}
		std::ifstream in_file(filename, std::ifstream::in | std::ifstream::binary);

		std::string line;
		std::getline(in_file, line);
		if (line.substr(0, 4) != "PC10")
		{
			std::cout << "Invalid file format: " << filename << std::endl;
			return false;
		}
		clear_gpu();
		clear_voxel_memory();

		Eigen::Vector3f grid_min = fribr::make_vec3f(read_float3(in_file)).array();
		Eigen::Vector3f grid_max = fribr::make_vec3f(read_float3(in_file)).array();
		Eigen::Vector3i grid_size = fribr::make_vec3i(read_int3(in_file));
		int ref_scale = read_int(in_file);
		refinementScale = ref_scale;

		grid.reset(grid_min, grid_max, grid_size);
		fineGrid.reset(grid_min, grid_max, refinementScale * grid_size);

		// Begin components
		m_all_triangles = 0;
		components.clear();
		int num_components = read_int(in_file);
		for (int i = 0; i < num_components; ++i)
		{
			Component::Ptr component(new Component(&fineGrid));
			component->load(in_file);
			components.push_back(component);
			m_all_triangles += component->vertices.size() / 3;
			for (Eigen::Vector3i v : component->slice_voxels) {
				fineGrid.add_component(v, components.size() - 1);
				
			}
		}
		return true;
	}

}
