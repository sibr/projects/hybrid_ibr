// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#include <projects/hybrid_ibr/renderer/HybridRenderer.hpp>

sibr::HybridRenderer::HybridRenderer(const std::vector<sibr::InputCamera::Ptr>& cameras, const uint w, const uint h, const std::string& fShader, const std::string& vShader, const bool facecull) :
	_grid(Eigen::Vector3i()), _fineGrid(Eigen::Vector3i())
{
	fragString = fShader;
	vertexString = vShader;
	_backFaceCulling = facecull;
	_cams = cameras;
	createCamData(cameras);
	_maxPVMCameras = _cams.size();
	// Setup shaders and uniforms.
	HybridRenderer::setupShaders(fragString, vertexString);

	// Create the intermediate rendertarget.
	_depthRT.reset(new sibr::RenderTargetRGBA32F(w, h, SIBR_GPU_LINEAR_SAMPLING, 2));

	CHECK_GL_ERROR;
}

sibr::HybridRenderer::HybridRenderer(const std::vector<sibr::InputCamera::Ptr>& cameras,
	const std::vector<PatchCloud::Component::Ptr>& components,
	const sibr::VoxelGridFRIBR& grid,
	const sibr::VoxelGridFRIBR& fineGrid,
	const uint w, const uint h,
	const std::string& fShader, const std::string& vShader, const bool facecull) :
	_grid(grid.size()), _fineGrid(fineGrid.size())
{
	_grid = grid;
	_fineGrid = fineGrid;
	_refinement_Scale = fineGrid.size().x() / grid.size().x();
	_components = components;
	_cams = cameras;

	fragString = fShader;
	vertexString = vShader;

	for (int i = 0; i < 4; ++i)
		_gpu_grid[i] = 0;

	_total_bins = _grid.size().x() * _grid.size().y() * _grid.size().z();
	_fat_grid.resize(_total_bins, false);
	_max_bins = 0.2 * (_total_bins);
	_commands.resize(_components.size() * _max_bins * std::pow(_refinement_Scale, 3));
	_command_to_layer.resize(_components.size() * _max_bins * std::pow(_refinement_Scale, 3));
	_visibleVoxels.resize(_max_bins * std::pow(_refinement_Scale, 3));
	_bestCameras.resize(12 * _max_bins * std::pow(_refinement_Scale, 3));
	
	if (_components.empty()) {
		SIBR_ERR << "No components to initialize renderer!!" << std::endl;
		return;
	}


	// Create the intermediate rendertarget.
	_depthRT.reset(new sibr::RenderTargetRGBA32F(w, h, SIBR_GPU_LINEAR_SAMPLING, 2));
	_patchDepthRT.reset(new sibr::RenderTargetRGBA32F(w, h));
	_sliceRT.reset(new sibr::RenderTargetRGBA32F(w, h));
	_ulrRT.reset(new sibr::RenderTargetRGBA32F(w, h));
	_maskRT.reset(new sibr::RenderTargetRGBA32F(w, h));
	_voxelRT.reset(new sibr::RenderTargetRGBA32F(w, h, SIBR_GPU_LINEAR_SAMPLING, 4));
	_spatialFilterRT.reset(new sibr::RenderTargetRGBA32F(w, h));
	_compositeRT.reset(new sibr::RenderTargetRGBA32F(w, h));
	_filterRT.reset(new sibr::RenderTargetRGBA32F(w, h));
	_patchArray1.reset(new sibr::Texture2DArrayRGBA32F(w, h, 12, SIBR_GPU_LINEAR_SAMPLING));
	_patchArray2.reset(new sibr::Texture2DArrayRGBA32F(w, h, 12, SIBR_GPU_LINEAR_SAMPLING));
	_layerDepthRT.reset(new sibr::RenderTargetRGBA32F(w, h, SIBR_GPU_LINEAR_SAMPLING, 6));

	glGenTextures(1, &_patchDepthArray);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _patchDepthArray);
	CHECK_GL_ERROR;
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	CHECK_GL_ERROR;
	glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_DEPTH_COMPONENT24, _patchArray1->w(), _patchArray1->h(), 12);
	CHECK_GL_ERROR;

	createCamData(cameras);
	createComponentsData(components);
	createGridOccupancyData();
	HybridRenderer::setupShaders(fragString, vertexString);

	resetSliceImage();

	for (int i = 0; i < NUM_LAYERS; ++i) {
		_layersRT[i].reset(new sibr::RenderTargetRGBA32F(w, h));
	}

	glGenBuffers(1, &_rankBuffer);
	glGenTextures(1, &_rankTextureBuffer);
	glGenBuffers(1, &_visibleVoxelsBuffer);
	glGenTextures(1, &_visibleVoxelsTextureBuffer);

	glBindBuffer(GL_TEXTURE_BUFFER, _rankBuffer);
	glBufferData(GL_TEXTURE_BUFFER, 12 * _max_bins * std::pow(_refinement_Scale, 3) * sizeof(int), NULL, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_TEXTURE_BUFFER, 0);
	CHECK_GL_ERROR;

	glBindBuffer(GL_TEXTURE_BUFFER, _visibleVoxelsBuffer);
	glBufferData(GL_TEXTURE_BUFFER, _max_bins * std::pow(_refinement_Scale, 3) * sizeof(unsigned int), NULL, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_TEXTURE_BUFFER, 0);
	CHECK_GL_ERROR;

	std::vector<int> selectedCameras;
	selectedCameras.resize(_cameraInfos.size(), 0);
	for (int i = 0; i < _cameraInfos.size(); i++) selectedCameras[i] = _cameraInfos[i].selected;
	glBindBuffer(GL_TEXTURE_BUFFER, _selectedCamerasBuffer);
	glBufferData(GL_TEXTURE_BUFFER, selectedCameras.size() * sizeof(int), selectedCameras.data(), GL_STATIC_DRAW);

	std::vector<Vector3f> componentsPosition;
	componentsPosition.resize(_components.size());
	for (int i = 0; i < componentsPosition.size(); i++) componentsPosition[i] = _components[i]->position;
	glBindBuffer(GL_TEXTURE_BUFFER, _componentsPositionBuffer);
	glBufferData(GL_TEXTURE_BUFFER, componentsPosition.size() * sizeof(Vector3f), componentsPosition.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_TEXTURE_BUFFER, 0);
}


void sibr::HybridRenderer::setupShaders(const std::string& fShader, const std::string& vShader)
{
	// Init uniforms
	_nCamPos.resize(3);
	_nWorldToCam.resize(6);
	_nCam2Clip.resize(6);

	// Create ULR+TM Composite shaders.
	std::cout << "[HybridRenderer] Setting up shaders for " << _maxNumCams << " cameras." << std::endl;
	GLShader::Define::List defines;
	defines.emplace_back("NUM_CAMS", _maxNumCams);

	CHECK_GL_ERROR;

	_ulrShader.init("ULRV3",
		sibr::loadFile(sibr::getShadersDirectory("") + "/" + vShader + ".vert"),
		sibr::loadFile(sibr::getShadersDirectory("ulr") + "/ulr_v3_fast.frag", defines));

	_camsCount.init(_ulrShader, "camsCount");
	_nCamPos[1].init(_ulrShader, "ncam_pos");
	_occTest.init(_ulrShader, "occ_test");
	_epsilonOcclusion.init(_ulrShader, "epsilonOcclusion");
	_flipRGBs.init(_ulrShader, "flipRGBs");
	_showWeights.init(_ulrShader, "showWeights");

	CHECK_GL_ERROR;

	_depthShader.init("ULRV3Depth",
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/global_intersect.vert"),
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/global_intersect.frag", defines));

	// Setup uniforms.
	_nCamProj[0].init(_depthShader, "proj");
	
	CHECK_GL_ERROR;

	_hybridCompShader.init("HybridComposite",
		sibr::loadFile(sibr::getShadersDirectory("") + "/" + vShader + ".vert"),
		sibr::loadFile(sibr::getShadersDirectory("") + "/" + fShader + ".frag", defines));

	_toggleMask.init(_hybridCompShader, "toggleMask");
	_renderMode.init(_hybridCompShader, "renderMode");
	_maskKernelSize.init(_hybridCompShader, "kernelSize");

	CHECK_GL_ERROR;
	_textureShader.init("Texture",
		sibr::loadFile(sibr::getShadersDirectory("core") + "/texture.vert"),
		sibr::loadFile(sibr::getShadersDirectory("core") + "/texture.frag"));

	// Create Difference shaders.
	_bgCompo.init("Difference",
		sibr::loadFile(sibr::getShadersDirectory("") + "/" + vShader + ".vert"),
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/difference.frag"));

	_grid_min_comp.init(_bgCompo, "grid_min");
	_grid_max_comp.init(_bgCompo, "grid_max");
	_compoMargin.init(_bgCompo, "compositingMargin");
	CHECK_GL_ERROR;

	// Create Clear Bin shader
	GLShader::Define::List definesGrid;
	definesGrid.emplace_back("GRID_SIZE_X", _grid.size().x());
	definesGrid.emplace_back("GRID_SIZE_Y", _grid.size().y());
	definesGrid.emplace_back("GRID_SIZE_Z", _grid.size().z());
	_clearBinShader.init("ClearBins",
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/gpu_grid_clear.vert"),
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/gpu_grid_clear.frag", definesGrid));
	// Setup Uniforms
	_resolution.init(_clearBinShader, "resolution");

	// Create Depth Bin shader
	_depthBinShader.init("DepthBins",
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/voxel_fetch.vert"),
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/voxel_fetch.frag", definesGrid));

	_nCamPos[2].init(_depthBinShader, "ncam_pos");
	_nWorldToCam[2].init(_depthBinShader, "world_to_camera");
	_nCam2Clip[2].init(_depthBinShader, "camera_to_clip");
	_nCamProj[1].init(_depthBinShader, "proj");
	_grid_min.init(_depthBinShader, "grid_min");
	_grid_max.init(_depthBinShader, "grid_max");
	_tolerance.init(_depthBinShader, "tolerance");
	_thresh.init(_depthBinShader, "threshold");
	_neighborhood.init(_depthBinShader, "neighborhood");
	_compositingMargin.init(_depthBinShader, "compositingMargin");

	// Create GPU ranking shader
	_gpuSortShader.init("RankingShader",
		sibr::loadFile(sibr::getShadersDirectory("") + "/" + vShader + ".vert"),
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/gpuRanking.frag"));

	// Create patch depth shader
	_patchDepthShader.init("PatchDepth",
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/patch_intersect.vert"),
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/patch_intersect.frag"),
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/patch_intersect.geom"));
	_nWorldToCam[1].init(_patchDepthShader, "world_to_camera");
	_nCam2Clip[1].init(_patchDepthShader, "camera_to_clip");


	// Create consensus filtering shader
	_consensusFilterShader.init("ConsensusFilter",
		sibr::loadFile(sibr::getShadersDirectory("") + "/" + vShader + ".vert"),
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/consensus_filter.frag", defines));
	_nWorldToCam[4].init(_consensusFilterShader, "world_to_camera");
	_nCam2Clip[4].init(_consensusFilterShader, "camera_to_clip");
	_displayLayer.init(_consensusFilterShader, "selected_layer");
	_numIter.init(_consensusFilterShader, "num_iter");
	_ibrSigma.init(_consensusFilterShader, "sigma");
	_ibrResAlpha.init(_consensusFilterShader, "resolution_alpha");
	_nCamPos[0].init(_consensusFilterShader, "ncam_pos");
	_ibrDepthAlpha.init(_consensusFilterShader, "depth_alpha");
	_threshold.init(_consensusFilterShader, "threshold_photo");
	_threshold_geo.init(_consensusFilterShader, "threshold_geo");


	// Create spatial consensus filtering shader
	_spatialFilterShader.init("SpatialFilter",
		sibr::loadFile(sibr::getShadersDirectory("") + "/" + vShader + ".vert"),
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/spatial_filter.frag"));
	_nWorldToCam[5].init(_spatialFilterShader, "world_to_camera");
	_nCam2Clip[5].init(_spatialFilterShader, "camera_to_clip");
	_kernelSize.init(_spatialFilterShader, "kernelSize");
	_alpha.init(_spatialFilterShader, "alpha");
	_weightExps.init(_spatialFilterShader, "exps");

	// Create average and dump shader
	_averageShader.init("AverageShader",
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/average.vert"),
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/average.frag"));

	_pvMeshShader.init("PerViewMesh",
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/perview_mesh.vert"),
		sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/perview_mesh.frag"));

	_nCam2Clip[3].init(_pvMeshShader, "camera_to_clip");
	_nWorldToCam[3].init(_pvMeshShader, "world_to_camera");


	// initialize rank compute shader
	// SIBR expects "frag"/"geo"/"vert" extensions, otherwise nothing works
	GLShader::Define::List rankDefines;
	rankDefines.emplace_back("MAX_IN_CAMERAS", _maxPVMCameras);
	const std::string shaderString = sibr::loadFile(sibr::getShadersDirectory("hybrid") + "/rank_cameras.frag", rankDefines);
	const char* shaderCString = shaderString.c_str();
	GLuint rankShader = glCreateShader(GL_COMPUTE_SHADER);
	glShaderSource(rankShader, 1, &shaderCString, NULL);
	glCompileShader(rankShader);
	
	int rvalue;
	glGetShaderiv(rankShader, GL_COMPILE_STATUS, &rvalue);
	if (!rvalue) {
		fprintf(stderr, "Error in compiling the compute shader\n");
		GLchar log[10240];
		GLsizei length;
		glGetShaderInfoLog(rankShader, 10239, &length, log);
		fprintf(stderr, "Compiler log:\n%s\n", log);
		exit(40);
	}
	
	_rankProgram = glCreateProgram();
	glAttachShader(_rankProgram, rankShader);
	glLinkProgram(_rankProgram);

	_fineGridSizeUniform = glGetUniformLocation(_rankProgram, "fineGridSize");
	_fineGridMinUniform = glGetUniformLocation(_rankProgram, "fineGridMin");
	_fineGridMaxUniform = glGetUniformLocation(_rankProgram, "fineGridMax");
	_cameraPositionUniform = glGetUniformLocation(_rankProgram, "cameraPosition");

}

void sibr::HybridRenderer::process(
	const sibr::Mesh& mesh,
	const sibr::Camera& eye,
	IRenderTarget& dst,
	const uint textureID, const uint specTextureID,
	const sibr::Texture2DArrayRGB::Ptr& inputRGBs,
	const sibr::Texture2DArrayLum32F::Ptr& inputDepths,
	bool passthroughDepth
) {
	_fat_grid.clear();
	
	
	// Render textured mesh and ibrNext
	_compositeRT->clear();
	renderAll(eye, mesh, textureID, specTextureID, inputRGBs);
		
	if (_showSlices) {
		_sliceIdx = (_sliceIdx > 0) ? _sliceIdx : 0;
		_sliceIdx = (_sliceIdx < _components.size()-1) ? _sliceIdx : _components.size()-1;
		renderSlice(mesh, eye, _compositeRT, textureID, specTextureID, inputRGBs, _sliceIdx);
		blit(*_compositeRT, dst);
		return;
	}

	// Perform blending.
	renderBlending(eye, dst, inputRGBs, inputDepths, passthroughDepth);
	
}

void sibr::HybridRenderer::process(const sibr::Mesh& mesh, const sibr::Camera& eye, 
	IRenderTarget& dst, IRenderTarget& tex1, sibr::Texture2DRGBA::Ptr tex2)
{

	renderProxyDepth(mesh, eye);

	glViewport(0, 0, dst.w(), dst.h());
	dst.clear();
	blit(tex1, dst);
	dst.bind();

	_bgCompo.begin();


	_grid_min_comp.set(_grid.min());
	_grid_max_comp.set(_grid.max());
	_compoMargin.send();

	// Send Textures
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _depthRT->handle());

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, tex2->handle());

	// Render 
	RenderUtility::renderScreenQuad();

	_bgCompo.end();
	dst.unbind();
}

void sibr::HybridRenderer::renderSlicePerVoxel(const sibr::Camera& eye, const float w, const float h, const int sliceIdx, const Eigen::Vector3i& selectedVoxel, IRenderTarget& sliceProjections, const sibr::Texture2DArrayRGB::Ptr& inputRGBs, bool passthroughDepth)
{
}

float sibr::HybridRenderer::refineSlices(const sibr::Camera& eye, const float w, const float h, const int sliceIdx, const sibr::Texture2DRGB::Ptr& outView, const Eigen::Vector3i& selectedVoxel, IRenderTarget& dst, IRenderTarget& mipMap, const sibr::Texture2DArrayRGB::Ptr& inputRGBs, bool passthroughDepth)
{
	return 0.0f;
}



void sibr::HybridRenderer::renderSlice(const sibr::Mesh& mesh, const sibr::Camera& eye, RenderTargetRGBA32F::Ptr& dst,
	const uint textureID, const uint specTextureID,
	const sibr::Texture2DArrayRGB::Ptr& inputRGBs, const int sliceIdx,
	bool passthroughDepth)
{
	_fat_grid.clear();
	_bins.clear();
	_commands.clear();
	_command_index = 0;
	_total_triangles = 0;

	// 1. Render-pass: Fetch visible voxles
	fetchVisibleVoxels(mesh, eye, textureID, specTextureID);

	_commands.resize(_components.size() * _bins.size() * std::pow(_refinement_Scale, 3));

	size_t max_bins = _bins.size();

	populateCommandPerSlice(max_bins, eye, sliceIdx);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
	// Extra:
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glClearDepth(1.0);


	CHECK_GL_ERROR;

	// initialize commands buffer
	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, _draw_calls);
	glBufferData(GL_DRAW_INDIRECT_BUFFER, _command_index * sizeof(DrawElementsIndirectCommand),
		_commands.data(), GL_STATIC_DRAW);

	// 2. Render-pass: Get screen space depth of the patches selected to be drawn
	dst->bind();

	glViewport(0, 0, dst->w(), dst->h());
	glDisable(GL_BLEND);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	_pvMeshShader.begin();

	CHECK_GL_ERROR;
	_nWorldToCam[3].set(eye.view());
	_nCam2Clip[3].set(eye.proj());

	CHECK_GL_ERROR;
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, inputRGBs->handle());

	glBindVertexArray(_vao);
	CHECK_GL_ERROR;
	glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, 0, _command_index, 0);
	CHECK_GL_ERROR;
	glBindVertexArray(0);

	if (_profiling) {
		std::cout << "\nDrawing " << _total_triangles << "/" << _all_triangles << " ("
			<< _total_triangles * 100.0f / _all_triangles << "%)               " << std::flush;
	}

	_pvMeshShader.end();
	dst->unbind();


}

void sibr::HybridRenderer::renderSliceError(IRenderTarget& dst)
{
	dst.clear();
	dst.bind();
	_textureShader.begin();
	sibr::Texture2DRGB32F::Ptr sliceErr(new Texture2DRGB32F(_sliceErrImg->resized(dst.w(), dst.h()), SIBR_GPU_LINEAR_SAMPLING | SIBR_CLAMP_UVS | SIBR_FLIP_TEXTURE));
	glViewport(0, 0, dst.w(), dst.h());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, sliceErr->handle());
	RenderUtility::renderScreenQuad();
	_textureShader.end();
	dst.unbind();
}

void sibr::HybridRenderer::startProfiling()
{
	_profiling = true;
	_fetchVoxel1.clear();
	_fetchVoxel2.clear();
	_rankVoxels.clear();
	_pvMeshRaster.clear();
	_clusterBlend.clear();
	_spFilter.clear();
	_ulrCost.clear();
	_compositing.clear();
}

void sibr::HybridRenderer::stopProfiling()
{
	// Compute statistic on the timings for each step and display them.
	const std::vector<std::string> names = { "Fetch1", "Fetch2", "Rank", "PVRaster", 
		"Cluster+Blend", "Spatial Fltr", "ULR", "Comp" };
	const  std::vector<std::vector<float>> counts = {
		_fetchVoxel1, _fetchVoxel2, _rankVoxels, _pvMeshRaster, 
		_clusterBlend, _spFilter, _ulrCost, _compositing};
	std::string profileStr = "";

	for (int i = 0; i < names.size(); ++i) {
		// Compute metrics: min, max, avg, variance.
		double miniF = std::numeric_limits<double>::max();
		double maxiF = 0.0;
		double avgF = 0.0;
		for (size_t tid = 0; tid < counts[i].size(); ++tid) {
			const double ft = double(counts[i][tid]);
			avgF += ft;
			miniF = std::min(miniF, ft);
			maxiF = std::max(maxiF, ft);
		}
		avgF /= double(counts[i].size());
		double varF = 0.0;
		for (size_t tid = 0; tid < counts[i].size(); ++tid) {
			const double residualF = double(counts[i][tid]) - avgF;
			varF += residualF * residualF;
		}
		varF /= double(int(counts[i].size()) - 1);
		profileStr += "-----------\n";
		profileStr += names[i] + " num frames: " + std::to_string(counts[i].size()) + "\n";
		profileStr += names[i] + " min/max: " + std::to_string(miniF) + "/" + std::to_string(maxiF) + "\n";
		profileStr += names[i] + " avg/stddev: " + std::to_string(avgF) + "/" + std::to_string(std::sqrt(varF)) + "\n";
	}
	std::cout << profileStr;
	_fetchVoxel1.clear();
	_fetchVoxel2.clear();
	_rankVoxels.clear();
	_pvMeshRaster.clear();
	_clusterBlend.clear();
	_spFilter.clear();
	_ulrCost.clear();
	_compositing.clear();
}

void sibr::HybridRenderer::renderAll(const sibr::Camera& eye,
	const sibr::Mesh& mesh,
	const uint textureID, const uint specTextureID,
	const sibr::Texture2DArrayRGB::Ptr& inputRGBs,
	bool passthroughDepth)
{
	_fat_grid.clear();
	_bins.clear();
	_command_index = 0;
	_total_triangles = 0;
	_voxel_index = 0;

	if (_profiling) {
		glFinish();
		_timer1.tic();
	}

	// 1. Render-pass: Fetch visible voxles
	fetchVisibleVoxels(mesh, eye, textureID, specTextureID);
	
	if (_profiling) {
		glFinish();
		//std::cout << "\nFetch visible voxels CPU: " << _timer2.deltaTimeFromLastTic() << "ms" << std::flush;
		_fetchVoxel2.push_back(_timer2.deltaTimeFromLastTic());
	}

	if (_profiling) {
		glFinish();
		_timer3.tic();
		//_timerTemp.tic();
	}

	// rank the input tiles in each voxels and populate the indirect draw commands
	rankVoxelsAndPopulateCommands(eye);

	if (_profiling) {
		glFinish();
		//std::cout << "\nRank and populate command: " << _timer3.deltaTimeFromLastTic() << "ms" << std::flush;
		_rankVoxels.push_back(_timer3.deltaTimeFromLastTic());
	}

	if (_profiling) {
		glFinish();
		_timer4.tic();
	}

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	
	// Create Vertex Buffers.
	glBindBuffer(GL_TEXTURE_BUFFER, _command_to_layer_buffer);
	glBufferData(GL_TEXTURE_BUFFER, sizeof(int) * _command_index, _command_to_layer.data(), GL_STATIC_DRAW);
	glGenTextures(1, &_command_to_layer_texture);
	glBindBuffer(GL_TEXTURE_BUFFER, 0);
	CHECK_GL_ERROR;
	// initialize commands buffer
	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, _draw_calls);
	glBufferData(GL_DRAW_INDIRECT_BUFFER, _command_index * sizeof(DrawElementsIndirectCommand),
		_commands.data(), GL_STATIC_DRAW);

	// 2. Render-pass: Get screen space depth of the patches selected to be drawn
	//---- this should be factored out of the loop - probably into a new array rendertarget function ------
	GLenum drawbuffers[SIBR_MAX_SHADER_ATTACHMENTS];
	glBindFramebuffer(GL_FRAMEBUFFER, _patchFrameBuffer);
	for (uint i = 0; i < SIBR_MAX_SHADER_ATTACHMENTS; i++)
		drawbuffers[i] = GL_COLOR_ATTACHMENT0 + i;
	glDrawBuffers(2, drawbuffers);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _patchArray1->handle());
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, _patchArray1->handle(), 0);
	
	glBindTexture(GL_TEXTURE_2D_ARRAY, _patchArray2->handle());
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, _patchArray2->handle(), 0);
	CHECK_GL_ERROR;
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _patchDepthArray, 0);

	CHECK_GL_ERROR;
	//-------------------------------------------------

	glViewport(0, 0, _patchDepthRT->w(), _patchDepthRT->h());

	glDisable(GL_BLEND);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	_patchDepthShader.begin();
	CHECK_GL_ERROR;
	_nWorldToCam[1].set(eye.view());
	_nCam2Clip[1].set(eye.proj());

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_BUFFER, _command_to_layer_texture);
	glTexBuffer(GL_TEXTURE_BUFFER, GL_R32F, _command_to_layer_buffer);
	CHECK_GL_ERROR;


	glBindVertexArray(_vao);
	glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, 0, _command_index, 0);
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_BUFFER, 0);

	CHECK_GL_ERROR;
	_patchDepthShader.end();
	glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
	if (_profiling) {
		glFinish();
		//std::cout << "\nPVMesh Rasterization: " << _timer4.deltaTimeFromLastTic() << "ms" << std::flush;
		_pvMeshRaster.push_back(_timer4.deltaTimeFromLastTic());
	}

	if (_profiling) {
		glFinish();
		_timer5.tic();
	}

	
	CHECK_GL_ERROR;
	// 3. Do consensus filtering / clustering on slices
	_layerDepthRT->clear();
	_layerDepthRT->bind();


	glViewport(0, 0, _layerDepthRT->w(), _layerDepthRT->h());
	glDisable(GL_BLEND);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	_consensusFilterShader.begin();
	CHECK_GL_ERROR;
	_nWorldToCam[4].set(eye.view());
	_nCam2Clip[4].set(eye.proj());
	_displayLayer.send();
	_numIter.send();
	_ibrSigma.send(); 
	_nCamPos[0].set(eye.position());
	_ibrDepthAlpha.send();

	_threshold.send();
	_threshold_geo.send();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _patchArray1->handle());
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _patchArray2->handle());
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D_ARRAY, inputRGBs->handle());
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_1D, _camera_pos_texture);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, _voxelRT->handle(3));
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, _voxelRT->handle(1));
	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, _voxelRT->handle(2));

	sibr::RenderUtility::renderScreenQuad();
	CHECK_GL_ERROR;


	_consensusFilterShader.end();
	CHECK_GL_ERROR;


	_layerDepthRT->unbind();
	
	if (_profiling) {
		glFinish();
		//std::cout << "\nDepth Clustering + ULR BLending: " << _timer5.deltaTimeFromLastTic() << "ms" << std::flush;
		_clusterBlend.push_back(_timer5.deltaTimeFromLastTic());
	}

	if (_profiling) {
		glFinish();
		_timer6.tic();
	}

	_compositeRT->clear();
	_compositeRT->bind();
	
	glViewport(0, 0, _compositeRT->w(), _compositeRT->h());
	glDisable(GL_BLEND);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	
	_spatialFilterShader.begin();
	CHECK_GL_ERROR;
	_nWorldToCam[5].set(eye.view());
	_nCam2Clip[5].set(eye.proj());
	_kernelSize.send();
	_weightExps.send();

	CHECK_GL_ERROR;
	_alpha.send();
	

	CHECK_GL_ERROR;

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _layerDepthRT->handle(0));
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, _layerDepthRT->handle(1));
	
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, _layerDepthRT->handle(2));
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, _layerDepthRT->handle(3));

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, _voxelRT->handle(0));
	CHECK_GL_ERROR;
	
	sibr::RenderUtility::renderScreenQuad();
		CHECK_GL_ERROR;
	
	
	_spatialFilterShader.end();
	
	_compositeRT->unbind();

	if (_profiling) {
		glFinish();
		//std::cout << "\nSpatial Filter: " << _timer6.deltaTimeFromLastTic() << "ms" << std::flush;
		_spFilter.push_back(_timer6.deltaTimeFromLastTic());
	}


}

void sibr::HybridRenderer::renderProxyDepth(const sibr::Mesh& mesh, const sibr::Camera& eye)
{
	// Bind and clear RT.
	_depthRT->bind();
	glViewport(0, 0, _depthRT->w(), _depthRT->h());
	glClearColor(0, 0, 0, 1);
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Render the mesh from the current viewpoint, output positions.
	_depthShader.begin();
	_nCamProj[0].set(eye.viewproj());

	mesh.render(true, _backFaceCulling);

	_depthShader.end();
	_depthRT->unbind();
}

void sibr::HybridRenderer::updateCameras(const std::vector<uint>& camIds) {
	// Reset all cameras.
	for (auto& caminfos : _cameraInfos) {
		caminfos.selected = 0;
	}

	// Enabled the ones passed as indices.
	for (const auto& camId : camIds) {
		_cameraInfos[camId].selected = 1;
	}

	// Update the content of the UBO.
	glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(CameraUBOInfos) * _maxNumCams, &_cameraInfos[0]);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}


void sibr::HybridRenderer::renderBlending(
	const sibr::Camera& eye,
	IRenderTarget& dst,
	const sibr::Texture2DArrayRGB::Ptr& inputRGBs,
	const sibr::Texture2DArrayLum32F::Ptr& inputDepths,
	bool passthroughDepth
) {
	if (_profiling) {
		glFinish();
		_timer0.tic();
	}
	// Render ULR to ULR RT
	glViewport(0, 0, _ulrRT->w(), _ulrRT->h());
	_ulrRT->clear();
	blit(*_voxelRT, *_ulrRT);
	_ulrRT->bind();
	
	_ulrShader.begin();
	
	// Uniform values.
	_camsCount.send();
	_nCamPos[1].set(eye.position());
	_occTest.send();
	_flipRGBs.send();
	_showWeights.send();
	_epsilonOcclusion.send();
	
	// Textures.
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _layerDepthRT->handle(4));
	
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D_ARRAY, inputRGBs->handle());
	
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D_ARRAY, inputDepths->handle());
	
	// Bind UBO to shader, after all possible textures.
	glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
	glBindBufferBase(GL_UNIFORM_BUFFER, 4, _uboIndex);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	
	if (passthroughDepth) {
		glEnable(GL_DEPTH_TEST);
	}
	else {
		glDisable(GL_DEPTH_TEST);
	}
	
	// Perform ULR rendering.
	RenderUtility::renderScreenQuad();
	glDisable(GL_DEPTH_TEST);
	
	_ulrShader.end();
	_ulrRT->unbind();

	if (_profiling) {
		glFinish();
		//std::cout << "\nGlobal ULR Cost: " << _timer0.deltaTimeFromLastTic() << "ms" << std::flush;
		_ulrCost.push_back(_timer0.deltaTimeFromLastTic());
	}

	
	if (_profiling) {
		glFinish();
		_timer8.tic();
	
	}
	// Bind and clear destination rendertarget.
	glViewport(0, 0, dst.w(), dst.h());
	dst.clear();
	dst.bind();
	
	_hybridCompShader.begin();
	
	// Uniform values.
	_renderMode.send();
	_toggleMask.send();
	_maskKernelSize.send();
	
	// Textures.
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _compositeRT->handle());
	
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, _voxelRT->handle(0));
	
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, _ulrRT->handle());
	
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, _layerDepthRT->handle(0));
	
	
	if (passthroughDepth) {
		glEnable(GL_DEPTH_TEST);
	}
	else {
		glDisable(GL_DEPTH_TEST);
	}
	
	RenderUtility::renderScreenQuad();
	glDisable(GL_DEPTH_TEST);
	
	_hybridCompShader.end();
	dst.unbind();
	
	if (_profiling) {
		glFinish();
		//std::cout << "\nCompositing: " << _timer8.deltaTimeFromLastTic() << "ms" << std::flush;
		//std::cout << "\n============================" << std::flush;
		_compositing.push_back(_timer8.deltaTimeFromLastTic());
	}

}


void sibr::HybridRenderer::resetSliceImage()
{
	_sliceErrImg.reset(new ImageRGB32F(_sliceSize, _sliceSize));
	for (int img_i = 0; img_i < _sliceErrImg->w(); ++img_i) {
		for (int img_j = 0; img_j < _sliceErrImg->h(); ++img_j) {
			_sliceErrImg(img_i, img_j) = Vector3f(0.0, 0.0, 0.0);
		}
	}
}

Eigen::Vector2f sibr::HybridRenderer::vec2devs(Eigen::Vector3f& inputVec, Eigen::Vector3f& normalVec, Eigen::Vector3f& upVec)
{
	const Vector3f u = normalVec;
	const Vector3f v = upVec - ((upVec.dot(normalVec) / normalVec.squaredNorm()) * normalVec);
	const Vector3f w = u.cross(v);
	//std::cout << "u: " << u << std::endl;
	//std::cout << "v: " << v << std::endl;
	//std::cout << "w: " << w << std::endl;
	const float x = inputVec.dot((u / u.norm()));
	const float y = inputVec.dot((v / v.norm()));
	const float z = inputVec.dot((w / w.norm()));
	//std::cout << "x: " << x << std::endl;
	//std::cout << "y: " << y << std::endl;
	//std::cout << "z: " << z << std::endl;
	const float theta = SIBR_RADTODEG(acosf(z / std::sqrt(x * x + y * y + z * z))) - 90;
	const float phi = ((y - 0) < 0.0001) ? 0.0 : SIBR_RADTODEG(atanf(y / x));
	//std::cout << "theta: " << theta << std::endl;
	//std::cout << "phi: " << phi << std::endl;

	return Eigen::Vector2f(theta, phi);
}

Eigen::Vector2f sibr::HybridRenderer::vec2devs(Eigen::Vector3f& inputVec)
{
	const float x = inputVec.x();
	const float y = inputVec.y();
	const float z = inputVec.z();

	const float theta = SIBR_RADTODEG(acosf(z / std::sqrt(x * x + y * y + z * z)));
	const float phi = SIBR_RADTODEG(atanf(y / x));

	return Eigen::Vector2f(theta, phi);
}

void sibr::HybridRenderer::createCamData(const std::vector<sibr::InputCamera::Ptr>& cameras)
{
	_maxNumCams = cameras.size();
	_camsCount = int(_maxNumCams);

	// Populate the cameraInfos array (will be uploaded to the GPU).
	_cameraInfos.clear();
	_cameraInfos.resize(_maxNumCams);
	for (size_t i = 0; i < _maxNumCams; ++i) {
		const auto& cam = *cameras[i];
		_cameraInfos[i].vp = cam.viewproj();
		_cameraInfos[i].pos = cam.position();
		_cameraInfos[i].dir = cam.dir();
		_cameraInfos[i].selected = cam.isActive();
	}

	// Compute the max number of cameras allowed.
	GLint maxBlockSize = 0, maxSlicesSize = 0, maxTextureBuffer = 0;
	glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &maxBlockSize);
	glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &maxSlicesSize);
	glGetIntegerv(GL_MAX_TEXTURE_BUFFER_SIZE, &maxTextureBuffer);
	// For each camera we store a matrix, 2 vecs3, 2 floats (including padding).
	const unsigned int bytesPerCamera = 4 * (16 + 2 * 3 + 2);
	const unsigned int maxCamerasAllowed = std::min((unsigned int)maxSlicesSize, (unsigned int)(maxBlockSize / bytesPerCamera));
	std::cout << "[HybridRenderer] " << "MAX_UNIFORM_BLOCK_SIZE: " << maxBlockSize << ", MAX_ARRAY_TEXTURE_LAYERS: " << maxSlicesSize << ", meaning at most " << maxCamerasAllowed << " cameras." << std::endl;
	std::cout << "[HybridRenderer] " << "GL_MAX_TEXTURE_BUFFER_SIZE: " << maxTextureBuffer << std::endl;

	// Create UBO.
	_uboIndex = 0;
	glGenBuffers(1, &_uboIndex);
	glBindBuffer(GL_UNIFORM_BUFFER, _uboIndex);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(CameraUBOInfos) * _maxNumCams, &_cameraInfos[0], GL_DYNAMIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void sibr::HybridRenderer::createComponentsData(const std::vector<PatchCloud::Component::Ptr>& components)
{
	size_t total_indices = 0;
	size_t total_vertices = 0;
	size_t total_texcoords = 0;

	// Calculate total number of vertex indices, vertex, and texture coordinates 
	for (size_t i = 0; i < _components.size(); ++i)
	{
		sibr::PatchCloud::Component::Ptr component = _components[i];
		component->slices_start.clear();
		for (size_t i = 0; i < component->slice_indices.size(); ++i)
		{
			component->slices_start.push_back(total_indices);
			total_indices += component->slice_indices[i].size();
		}
		component->vertices_start = total_vertices;
		total_vertices += component->vertices.size();
		total_texcoords += component->texcoords.size();
	}

	_all_triangles = total_vertices / 3;
	// Create Index Buffers.
	glGenBuffers(1, &_index_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _index_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * total_indices, 0, GL_STATIC_DRAW);
	for (size_t i = 0; i < _components.size(); ++i)
	{
		sibr::PatchCloud::Component::Ptr component = _components[i];
		for (size_t i = 0; i < component->slice_indices.size(); ++i)
		{
			std::vector<uint32_t> offset_indices = component->slice_indices[i];
			for (uint32_t& idx : offset_indices)
				idx += component->vertices_start;
			glBufferSubData(GL_ELEMENT_ARRAY_BUFFER,
				component->slices_start[i] * sizeof(uint32_t),
				offset_indices.size() * sizeof(uint32_t),
				offset_indices.data());
		}
	}
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// Create Vertex Buffers.
	glGenBuffers(1, &_vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, _vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(fribr::float3) * total_vertices, 0, GL_STATIC_DRAW);
	for (size_t i = 0; i < _components.size(); ++i)
	{
		sibr::PatchCloud::Component::Ptr component = _components[i];
		glBufferSubData(GL_ARRAY_BUFFER,
			component->vertices_start * sizeof(fribr::float3),
			component->vertices.size() * sizeof(fribr::float3),
			component->vertices.data());
	}

	// Create Texture Coordinate Buffers.
	glGenBuffers(1, &_texcoord_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, _texcoord_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(fribr::float3) * total_texcoords, 0, GL_STATIC_DRAW);
	for (size_t i = 0; i < _components.size(); ++i)
	{
		sibr::PatchCloud::Component::Ptr component = _components[i];
		glBufferSubData(GL_ARRAY_BUFFER,
			component->vertices_start * sizeof(fribr::float3),
			component->texcoords.size() * sizeof(fribr::float3),
			component->texcoords.data());
	}

	// Create VAO.
	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);

	glBindBuffer(GL_ARRAY_BUFFER, _vertex_buffer);
	glEnableVertexAttribArray(VERTEX_LOCATION);
	glVertexAttribPointer(VERTEX_LOCATION, 3, GL_FLOAT, 0, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, _texcoord_buffer);
	glEnableVertexAttribArray(TEXCOORD_LOCATION);
	glVertexAttribPointer(TEXCOORD_LOCATION, 3, GL_FLOAT, 0, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _index_buffer);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// Only generate the draw calls buffer. No need to allocate or upload here. 
	// We have no idea how large the buffer is
	glGenBuffers(1, &_draw_calls);
	glGenBuffers(1, &_command_to_layer_buffer);

	//// [SP] Uploading camera position as a texture
	glGenTextures(1, &_camera_pos_texture);
	glBindTexture(GL_TEXTURE_1D, _camera_pos_texture);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB32F, _components.size(), 0, GL_RGB, GL_FLOAT, 0);
	glBindTexture(GL_TEXTURE_1D, 0);

	//_patchColorBuffer = 0;
	glGenFramebuffers(1, &_patchFrameBuffer);
	
	// Encode camera positions as texture and initialize the buffer
	std::vector<fribr::float3> component_positions(_components.size());
	for (size_t i = 0; i < _components.size(); ++i)
	{
		PatchCloud::Component::Ptr component = _components[i];
		component_positions[i] = fribr::make_float3(component->position);
	}
	CHECK_GL_ERROR;
	glBindTexture(GL_TEXTURE_1D, _camera_pos_texture);
	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB32F, _components.size(),
		0, GL_RGB, GL_FLOAT, component_positions.data());
	glBindTexture(GL_TEXTURE_1D, 0);
}

void sibr::HybridRenderer::createGridOccupancyData()
{
	//_grid_component_indices = 0;
	Eigen::Vector3i gSize = _fineGrid.size();
	size_t linear_grid_size = gSize.x() * gSize.y() * gSize.z();
	int voxel_size = 0;
	size_t prev_idx = 0;
	for (size_t gridIdx = 0; gridIdx < linear_grid_size; gridIdx++) {
		voxel_size = _fineGrid.get_components(gridIdx).size();
		
		if (voxel_size > 0) {
			_fineVoxelComponentIndices.push_back(prev_idx + voxel_size);
			//_grid_component_indices.push_back(_grid_component_occupancy.size());
			for (size_t grid_compIdx = 0; grid_compIdx < voxel_size; grid_compIdx++) {
				_fineVoxelComponents.push_back(_fineGrid.get_components(gridIdx)[grid_compIdx]);
			}
		}
		else {
			_fineVoxelComponentIndices.push_back(prev_idx);
		}
		prev_idx = _fineVoxelComponentIndices[gridIdx];
	}
	std::cout << "[Grid Components] Sizes size = " << sizeof(size_t) * _fineVoxelComponentIndices.size() << " Bytes" << std::endl;
	std::cout << "[Grid Components] Occup size = " << sizeof(int) * _fineVoxelComponents.size() << " Bytes" << std::endl;

	// determine maximum number of cameras per per-view mesh
	// TODO: merge with loops above
	_maxPVMCameras = 0;
	for (int i = 0; i < _fineVoxelComponentIndices.size(); i++)
	{
		size_t start_component_index = (i == 0) ? 0 : _fineVoxelComponentIndices[i - 1];
		size_t end_component_index = _fineVoxelComponentIndices[i];
		int pvmCams = end_component_index - start_component_index;
		_maxPVMCameras = std::max(_maxPVMCameras, pvmCams);
	}
	SIBR_LOG << "maxPVMCameras: " << _maxPVMCameras << std::endl;



	// Create the buffers for voxel indices here:
		
	glGenBuffers(1, &_fineVoxelComponentIndicesBuffer);
	glGenBuffers(1, &_fineVoxelComponentsBuffer);
	glGenBuffers(1, &_selectedCamerasBuffer);
	glGenBuffers(1, &_componentsPositionBuffer);
	glGenTextures(1, &_fineVoxelComponentIndicesTextureBuffer);
	glGenTextures(1, &_fineVoxelComponentsTextureBuffer);
	glGenTextures(1, &_selectedCamerasTextureBuffer);
	glGenTextures(1, &_componentsPositionTextureBuffer);
	glBindBuffer(GL_TEXTURE_BUFFER, _fineVoxelComponentIndicesBuffer);
	glBufferData(GL_TEXTURE_BUFFER, _fineVoxelComponentIndices.size() * sizeof(unsigned int), _fineVoxelComponentIndices.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_TEXTURE_BUFFER, _fineVoxelComponentsBuffer);
	glBufferData(GL_TEXTURE_BUFFER, _fineVoxelComponents.size() * sizeof(int), _fineVoxelComponents.data(), GL_STATIC_DRAW);


}

void sibr::HybridRenderer::createGPUGrid()
{
	if (_gpu_grid[0]) {
		return;
	}

	for (int i = 0; i < 4; ++i)
	{
		glGenTextures(1, &_gpu_grid[i]);
		glBindTexture(GL_TEXTURE_2D, _gpu_grid[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R32UI,
			_grid.size().y(), _grid.size().z(),
			0, GL_RED_INTEGER, GL_UNSIGNED_INT, 0);
	}

	glBindTexture(GL_TEXTURE_2D, 0);

}

void sibr::HybridRenderer::fetchVisibleVoxels(const sibr::Mesh& mesh,
	const sibr::Camera& eye, const uint textureID, const uint specTextureID)
{
	// Intialize the voxel grid textures
	createGPUGrid();
	
	glViewport(0, 0, _depthRT->w(), _depthRT->h());

	// Intialize the grid texture images
	// We don't really care where we are rendering as we discard all fragments after writing to the image objects.
	_clearBinShader.begin();

	for (int i = 0; i < 4; ++i)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		glBindImageTexture(i, _gpu_grid[i], 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32UI);
	}

	_resolution.set(Vector2i(_depthRT->w(), _depthRT->h()));

	sibr::RenderUtility::renderScreenQuad();

	_clearBinShader.end();

	// Render the voxel bins in the grid texture images
	glClearDepth(1.0);
	_voxelRT->clear();
	_voxelRT->bind();
	_depthBinShader.begin();

	for (int i = 0; i < 4; ++i)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		glBindImageTexture(i, _gpu_grid[i], 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);
	}

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, textureID);
	
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, specTextureID);

	_nWorldToCam[2].set(eye.view());
	_nCam2Clip[2].set(eye.proj());
	_nCamPos[2].set(eye.position());
	_grid_min.set(_grid.min());
	_grid_max.set(_grid.max());
	_tolerance.send();
	_thresh.send();
	_neighborhood.send();
	_compositingMargin.send();

	mesh.render(true, _backFaceCulling);

	for (int i = 0; i < 4; ++i)
		glBindImageTexture(i, 0, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);


	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, 0);

	_depthBinShader.end();
	_voxelRT->unbind();

	if (_profiling) {
		glFinish();
		//std::cout << "\nFetch visible voxels GPU: " << _timer1.deltaTimeFromLastTic() << "ms" << std::flush;
		_fetchVoxel1.push_back(_timer1.deltaTimeFromLastTic());
	}

	if (_profiling) {
		glFinish();
		_timer2.tic();
	}
	// Now, Download bit mask
	std::vector<int32_t> gpu_grid[4] = {
		std::vector<int32_t>(_grid.size().y() * _grid.size().z()),
		std::vector<int32_t>(_grid.size().y() * _grid.size().z()),
		std::vector<int32_t>(_grid.size().y() * _grid.size().z()),
		std::vector<int32_t>(_grid.size().y() * _grid.size().z())
	};
	for (int i = 0; i < 4; ++i)
	{
		glBindTexture(GL_TEXTURE_2D, _gpu_grid[i]);
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, (void*)gpu_grid[i].data());
	}
	glBindTexture(GL_TEXTURE_2D, 0);

	_numVisibleBins = 0;
	_fat_grid.clear();
	_fat_grid.resize(_grid.size().x() * _grid.size().y() * _grid.size().z(), false);
	// Inflate the grid occupancy as a linear vector
	for (int bin_id = 0; bin_id < 4; ++bin_id) {
		for (int bin_z = 0; bin_z < _grid.size().z(); ++bin_z) {
			for (int bin_y = 0; bin_y < _grid.size().y(); ++bin_y)
			{
				size_t bf = gpu_grid[bin_id][bin_y + _grid.size().y() * bin_z];

				int db_cnt2 = 0;
				size_t prev_bit = 0;

				for (size_t bit = next_bit(bf, 0); bit < 32; bit = next_bit(bf, bit + 1))
				{
					db_cnt2++;
					if (db_cnt2 > 32) {
						//std::cerr << "ERROR bit= " << bit << " prev bit = " << prev_bit << " dbcnt  " << db_cnt2 << " BF = " << bf << std::endl;
						break;
					}

					if (_neighborhood.get()) {
						int bin_x = bin_id * 32 + bit;
						for (int dz = -1; dz <= 1; ++dz)
							for (int dy = -1; dy <= 1; ++dy)
								for (int dx = -1; dx <= 1; ++dx)
								{
									int xx = bin_x + dx;
									int yy = bin_y + dy;
									int zz = bin_z + dz;
									if (xx < 0 || yy < 0 || zz < 0 ||
										xx >= _grid.size().x() ||
										yy >= _grid.size().y() ||
										zz >= _grid.size().z())
										continue;

									int bin_idx = xx + _grid.size().x() * (yy + _grid.size().y() * zz);
									_fat_grid[bin_idx] = true;
								}
					}
					else {
						int xx = bin_id * 32 + bit;
						int yy = bin_y;
						int zz = bin_z;
						int bin_idx = xx + _grid.size().x() * (yy + _grid.size().y() * zz);
						_fat_grid[bin_idx] = true;
					}
					prev_bit = bit;
				}
			}
		}
	}


	// Create the grid occupancy bins (voxels) information
	//std::vector<PatchCloud::ImageBin> bins;
	
	_bins.clear();
	for (int bin_z = 0; bin_z < _grid.size().z(); ++bin_z) {
		for (int bin_y = 0; bin_y < _grid.size().y(); ++bin_y) {
			for (int bin_x = 0; bin_x < _grid.size().x(); ++bin_x)
			{
				int bin_idx = bin_x + _grid.size().x() * (bin_y + _grid.size().y() * bin_z);
				bool not_here = !_fat_grid[bin_idx];
				// Check if the voxel was rendered in previous VOXEL_MEMORY_SIZE render passes
				// If it was, do not discard that voxel for temporal coherency
				for (int i = 0; i < VOXEL_MEMORY_SIZE; ++i)
				{
					not_here = not_here && (_voxel_memory[i].empty() || !_voxel_memory[i][bin_idx]);
				}

				// Populate bins with the voxels that needs to be rendered
				if (!not_here) {
					_bins.push_back(PatchCloud::ImageBin(bin_x, bin_y, bin_z, _grid));
				}
			}
		}
	}

	if (_update_voxel_memory)
	{
		for (int i = 1; i < VOXEL_MEMORY_SIZE; ++i)
			_voxel_memory[i - 1].swap(_voxel_memory[i]);
		_voxel_memory[VOXEL_MEMORY_SIZE - 1].swap(_fat_grid);
	}

	_numVisibleBins = _bins.size();
	// Reallocate memory size of commands and commands to layer 
	// if number if visible bins exceeds current limit
	if (_numVisibleBins > _max_bins) {
		_max_bins = 2 * _max_bins;
		std::cout << "Changing #voxel limit to: " << _max_bins << " voxels!!!!!!!" << std::endl;
		_commands.resize(_components.size() * _max_bins * std::pow(_refinement_Scale, 3));
		_command_to_layer.resize(_components.size() * _max_bins * std::pow(_refinement_Scale, 3));
		_visibleVoxels.resize(_max_bins* std::pow(_refinement_Scale, 3));
		_bestCameras.resize(12 * _max_bins * std::pow(_refinement_Scale, 3));
		glBindBuffer(GL_TEXTURE_BUFFER, _rankBuffer);
		glBufferData(GL_TEXTURE_BUFFER, 12 * _max_bins * std::pow(_refinement_Scale, 3) * sizeof(int), NULL, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_TEXTURE_BUFFER, _visibleVoxelsBuffer);
		glBufferData(GL_TEXTURE_BUFFER, _max_bins * std::pow(_refinement_Scale, 3) * sizeof(unsigned int), NULL, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_TEXTURE_BUFFER, 0);
		CHECK_GL_ERROR;
	}

	//std::cout << "#Visible voxels: " << _bins.size() << std::flush;
	
}

void sibr::HybridRenderer::rankVoxelsAndPopulateCommands(const sibr::Camera& eye)
{
	

	if (_gpuSorting.get()) {

#pragma omp parallel for 
		
		for (int i = 0; i < _numVisibleBins; ++i)
		{
			// iterate through each component in the voxel and compute the IBR cost
			const Eigen::Vector3i coarseVoxel = _bins[i].voxel;
			for (size_t xx = 0; xx < _refinement_Scale; xx++) {
				for (size_t yy = 0; yy < _refinement_Scale; yy++) {
					for (size_t zz = 0; zz < _refinement_Scale; zz++) {
						
						const Eigen::Vector3i fineVoxel = Vector3i(xx, yy, zz);
						const Eigen::Vector3i selectedVoxel = (coarseVoxel * _refinement_Scale) + fineVoxel;
						const size_t linIdx = _fineGrid.linear_index(selectedVoxel);
						size_t start_component_index = (linIdx == 0) ? 0 : _fineVoxelComponentIndices[linIdx - 1];
						size_t end_component_index = _fineVoxelComponentIndices[linIdx];
						if (end_component_index - start_component_index > 0)
						{
							// thread-save append
							size_t vi = doAtomicOp(&_voxel_index, 1);
							_visibleVoxels[vi] = linIdx;
						}
					}
				}
			}
		}

		

		const int maxCams = 12; 

		

		//=========================================


		glUseProgram(_rankProgram);

		// prepare output buffer
		glBindBuffer(GL_TEXTURE_BUFFER, _rankBuffer);
		glBindTexture(GL_TEXTURE_BUFFER, _rankTextureBuffer);
		glTexBufferEXT(GL_TEXTURE_BUFFER, GL_R32I, _rankBuffer);
		glBindImageTexture(0, _rankTextureBuffer, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32I);
		CHECK_GL_ERROR;

		// upload visible voxels buffer and expose to shader
		glBindBuffer(GL_TEXTURE_BUFFER, _visibleVoxelsBuffer);
		glBufferSubData(GL_TEXTURE_BUFFER, 0, _voxel_index * sizeof(unsigned int), _visibleVoxels.data());	
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_BUFFER, _visibleVoxelsTextureBuffer);
		glTexBuffer(GL_TEXTURE_BUFFER, GL_R32UI, _visibleVoxelsBuffer);
		CHECK_GL_ERROR;

		// expose index buffers
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_BUFFER, _fineVoxelComponentIndicesTextureBuffer);
		glTexBuffer(GL_TEXTURE_BUFFER, GL_R32UI, _fineVoxelComponentIndicesBuffer);
		CHECK_GL_ERROR;
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_BUFFER, _fineVoxelComponentsTextureBuffer);
		glTexBuffer(GL_TEXTURE_BUFFER, GL_R32I, _fineVoxelComponentsBuffer);
		CHECK_GL_ERROR;
		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_BUFFER, _selectedCamerasTextureBuffer);
		glTexBuffer(GL_TEXTURE_BUFFER, GL_R32I, _selectedCamerasBuffer);
		CHECK_GL_ERROR;
		glActiveTexture(GL_TEXTURE5);
		glBindTexture(GL_TEXTURE_BUFFER, _componentsPositionTextureBuffer);
		glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F, _componentsPositionBuffer);
		CHECK_GL_ERROR;
		
		// set uniforms
		glUniform3i(_fineGridSizeUniform, _fineGrid.size().x(), _fineGrid.size().y(), _fineGrid.size().z());
		glUniform3f(_fineGridMinUniform, _fineGrid.min().x(), _fineGrid.min().y(), _fineGrid.min().z());
		glUniform3f(_fineGridMaxUniform, _fineGrid.max().x(), _fineGrid.max().y(), _fineGrid.max().z());
		glUniform3f(_cameraPositionUniform, eye.position().x(), eye.position().y(), eye.position().z());
		
		// run compute shader
		const int workGroupSize = 128;
		const int numWorkGroups = int(ceil(_voxel_index / float(workGroupSize)));
		glDispatchCompute(numWorkGroups, 1, 1);
		CHECK_GL_ERROR;

		// make sure writing to image has finished before read
		glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

		// download data
		glBindBuffer(GL_TEXTURE_BUFFER, _rankBuffer);
		glGetBufferSubData(GL_TEXTURE_BUFFER, 0, 12 * _voxel_index * sizeof(int), &_bestCameras[0]);
		CHECK_GL_ERROR;

		// clean up
		glUseProgram(0);
		glBindTexture(GL_TEXTURE_BUFFER, 0);
		glBindBuffer(GL_TEXTURE_BUFFER, 0);
		CHECK_GL_ERROR;

		//if (_profiling) {
		//	glFinish();
		//	std::cout << "\n---Part 2 (GPU): " << _timerTemp.deltaTimeFromLastTic() << "ms" << std::flush;
		//	_timerTemp.tic();
		//}

		// Populating Commands
#pragma omp parallel for 
		for (int k = 0; k < _voxel_index; ++k)
		{
			const size_t linear_idx = _visibleVoxels.at(k);
			for (size_t cam = 0; cam < maxCams; cam++)
			{
				const size_t component_index = _bestCameras[maxCams * k + cam];
				if (component_index == -1) break;

				const size_t slice_index = _components[component_index]->voxel_to_index_remap.find(linear_idx)->second;

				DrawElementsIndirectCommand command;
				command.count = _components[component_index]->slice_indices[slice_index].size();
				command.instanceCount = 1;
				// point to first slice index
				command.firstIndex = _components[component_index]->slices_start[slice_index];
				command.baseVertex = 0;
				command.baseInstance = 0;
				size_t ci = doAtomicOp(&_command_index, 1);
				_commands[ci] = command;
				_command_to_layer[ci] = cam;
				doAtomicOp(&_total_triangles, command.count / 3);
				// If the slice is valid (i.e. the slice is "complete" [no holes or dangling edge])
				//if (_components[component_index]->is_valid(selectedVoxel))
				//	valid_slices++;

				//// Number of tiles to draw criteria = min(cluster_threshold complete tile, k tiles)
				//// If we increase number of slices to draw per voxel, it will take more time to draw 
				//// [SP] Stop drawing until m_cluster_threshold tiles (paper:3) is drawn or m_cluster_k tiles (paper:12) have been drawn
				if (/*valid_slices >= _cluster_threshold ||*/ cam + 1 >= static_cast<size_t>(_cluster_k))
					break;
			}
		}

		//if (_profiling) {
		//	std::cout << "\n---Part 3: " << _timerTemp.deltaTimeFromLastTic() << "ms" << std::flush;
		//	_timerTemp.tic();
		//}

	}
	else {
#pragma omp parallel for 
		for (int i = 0; i < _numVisibleBins; ++i)
		{
			//PatchCloud::ImageBin& bin = _bins[i];


			std::string log = "";


			// iterate through each component in the voxel and compute the IBR cost
			const Eigen::Vector3i coarseVoxel = _bins[i].voxel;
			for (size_t xx = 0; xx < _refinement_Scale; xx++) {
				for (size_t yy = 0; yy < _refinement_Scale; yy++) {
					for (size_t zz = 0; zz < _refinement_Scale; zz++) {

						const Eigen::Vector3i fineVoxel = Vector3i(xx, yy, zz);
						const Eigen::Vector3i selectedVoxel = (coarseVoxel * _refinement_Scale) + fineVoxel;

						if (_fineGrid.get_components(selectedVoxel).size() > 0) {
							const PatchCloud::ImageBin& fineBin = PatchCloud::ImageBin(selectedVoxel.x(), selectedVoxel.y(), selectedVoxel.z(), _fineGrid);

							const Eigen::Array3f center = (fineBin.bbox_max + fineBin.bbox_min) * 0.5;
							const Eigen::Array3f delta = (fineBin.bbox_max - fineBin.bbox_min) * 0.5;
							const float voxel_radius = delta.matrix().norm();

							const Eigen::Array3f zero(0.0f, 0.0f, 0.0f);
							const Eigen::Vector3f camera_position = eye.position();

							// compute distance from current voxel center to camera
							const Eigen::Array3f virt_to_center = camera_position.array() - center;
							const Eigen::Array3f min_diff_virt = (virt_to_center.abs() - delta).max(zero);
							const float          min_dist_to_virt = min_diff_virt.matrix().norm();
							const float          v2c_dist = virt_to_center.matrix().norm();
							const float          inv_v2c_dist = 1.0f / v2c_dist;


							std::vector<CostIndex>	bin_list;
							// Input Camera Ranking
							int num_bins_per_voxel = 0;
							for (int j : _fineGrid.get_components(selectedVoxel))
							{
								// skip component if it (the camera) is not selected 
								// or ignored or exceeds components size (false cam)
								if (_cameraInfos[j].selected) {
									// Compute distance of component to voxel center
									const Eigen::Array3f real_to_center = _components[j]->position.array() - center;
									const Eigen::Array3f max_diff_real = real_to_center.abs() + delta;
									const float          max_dist_to_real = max_diff_real.matrix().norm();

									// Compute minimum angular distance between eye and voxel
									const Eigen::Vector3f virt_to_real = (_components[j]->position - camera_position).normalized();
									float min_angle_virt = 0.0f;

									if (voxel_radius < v2c_dist) {
										min_angle_virt = acosf(-virt_to_center.matrix().dot(virt_to_real) * inv_v2c_dist)
											- asinf(voxel_radius * inv_v2c_dist);
									}

									// Compute minimum angular distance between eye and component (camera) position
									float min_angle_real = 0.0f;
									const float r2c_dist = real_to_center.matrix().norm();
									const float inv_r2c_dist = 1.0f / r2c_dist;

									if (voxel_radius < r2c_dist) {
										min_angle_real = acosf(-real_to_center.matrix().dot(-virt_to_real) * inv_r2c_dist)
											- asinf(voxel_radius * inv_r2c_dist);
									}

									const float max_angle_real_virt = 3.1415926536f - min_angle_real - min_angle_virt;
									const float max_distance_cost = std::max(0.0f, 1.0f - min_dist_to_virt / max_dist_to_real);

									float max_cost = (1.0f - _ibrResAlpha) * max_angle_real_virt / 3.1415926536f +
										_ibrResAlpha * max_distance_cost;

									// Pushback max cost of each component (camera) whose slice is present in the voxel
									//CostIndex component_cost(new CostIndex(max_cost, j));

									bin_list.push_back(CostIndex(max_cost, j));

								}
							}



							// [Hedman 16] Sec* 5.2.2: sort the input tiles within each visible grid cell 
							// according to their worst-case IBR cost
							std::sort(bin_list.begin(), bin_list.end());


							const std::vector<int> slices = _fineGrid.get_components(selectedVoxel);
							//  Pack all draw command in a single glDrawIndirect instruction for the current voxel***
							int valid_slices = 0;
							int total_tri_per_voxel = 0;

							// Populating Commands
							for (size_t k = 0; k < bin_list.size(); ++k)
							{
								const size_t component_index = bin_list[k].index;

								const size_t linear_idx = _fineGrid.linear_index(selectedVoxel);
								const size_t slice_index = _components[component_index]->voxel_to_index_remap.find(linear_idx)->second;

								DrawElementsIndirectCommand command;
								command.count = _components[component_index]->slice_indices[slice_index].size();
								command.instanceCount = 1;
								// point to first slice index
								command.firstIndex = _components[component_index]->slices_start[slice_index];
								command.baseVertex = 0;
								command.baseInstance = 0;
								size_t ci = doAtomicOp(&_command_index, 1);
								_commands[ci] = command;
								_command_to_layer[ci] = k;
								doAtomicOp(&_total_triangles, command.count / 3);
								// If the slice is valid (i.e. the slice is "complete" [no holes or dangling edge])
								//if (_components[component_index]->is_valid(selectedVoxel))
								//	valid_slices++;

								//// Number of tiles to draw criteria = min(cluster_threshold complete tile, k tiles)
								//// If we increase number of slices to draw per voxel, it will take more time to draw 
								//// [SP] Stop drawing until m_cluster_threshold tiles (paper:3) is drawn or m_cluster_k tiles (paper:12) have been drawn
								if (/*valid_slices >= _cluster_threshold ||  */k + 1 >= static_cast<size_t>(_cluster_k))
									break;


							}
						}
					}
				}
			}
		}
	}
	//if (_profiling) {
	//	glFinish();
	//
	//	std::cout << "\nSorting: " << _timerTemp.deltaTimeFromLastTic() << "ms" << std::flush;
	//	std::cout << "\n---------------" << std::flush;
	//	//std::cout << "\nFetch visible voxels GPU: " << _query1.value() / 10e6 << "ms" << std::flush;
	//}
}

void sibr::HybridRenderer::populateCommandPerSlice(const size_t max_bins, const sibr::Camera& eye, const int sliceIdx)
{
#pragma omp parallel for
	for (int i = 0; i < max_bins; ++i)
	{
		PatchCloud::ImageBin& bin = _bins[i];


		Eigen::Array3f center = (bin.bbox_max + bin.bbox_min) * 0.5;
		Eigen::Vector3f eye_to_center = _components[sliceIdx]->position.array() - center;

		const Eigen::Vector3i coarseVoxel = _bins[i].voxel;
		for (size_t xx = 0; xx < _refinement_Scale; xx++) {
			for (size_t yy = 0; yy < _refinement_Scale; yy++) {
				for (size_t zz = 0; zz < _refinement_Scale; zz++) {

					const Eigen::Vector3i fineVoxel = Vector3i(xx, yy, zz);
					const Eigen::Vector3i selectedVoxel = (coarseVoxel * _refinement_Scale) + fineVoxel;

					std::vector<DeviationIndex>	bin_list_dev;
					for (int j : _fineGrid.get_components(selectedVoxel))
					{
						// Compute distance of component to voxel center
						Eigen::Vector3f real_to_center = _components[j]->position.array() - center;
						float val = -real_to_center.dot(-eye_to_center) / (real_to_center.norm() * eye_to_center.norm());

						val = (val > 1.0f) ? 1.0f : val;
						val = (val < -1.0f) ? -1.0f : val;

						float component_to_slice_dev = acosf(val);
						bin_list_dev.push_back(DeviationIndex(component_to_slice_dev, j));

					}


					std::sort(bin_list_dev.begin(), bin_list_dev.end());

					for (size_t k = 0; k < bin_list_dev.size(); ++k)
					{
						//if (_debugPrint) {
						//	std::cout << "Deviation b/n camera " << sliceIdx << " & camera " << bin_list_dev[k].index << " = " << bin_list_dev[k].dev << std::endl;
						//}

						size_t component_index = bin_list_dev[k].index;
						if (sliceIdx == component_index) {
							size_t linear_idx = _fineGrid.linear_index(selectedVoxel);

							// fetch the slice index given the voxel and the particular component
								// remember each slice can have multiple triangles
							size_t slice_index = _components[component_index]->voxel_to_index_remap.find(linear_idx)->second;

							DrawElementsIndirectCommand command;
							// number of vertex in the slice
							command.count = _components[component_index]->slice_indices[slice_index].size();
							command.instanceCount = 1;
							// point to first slice index
							command.firstIndex = _components[component_index]->slices_start[slice_index];
							command.baseVertex = 0;
							command.baseInstance = 0;
							size_t ci = doAtomicOp(&_command_index, 1);
							_commands[ci] = command;

							doAtomicOp(&_total_triangles, command.count / 3);
						}
					}
				}
			}
		}
		//if (_debugPrint) {
		//	std::cout << "Number of cameras visible in voxel " << bin.voxel << " = " << bin_list_dev.size() << std::endl;
		//	std::cout << "==================================================================================" << std::endl;
		//}
	}

	//_debugPrint = false;
}

void sibr::HybridRenderer::resize(const unsigned w, const unsigned h) {
	_depthRT.reset(new sibr::RenderTargetRGBA32F(w, h, SIBR_GPU_LINEAR_SAMPLING, 2));
}
