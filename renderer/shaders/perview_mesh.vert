// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 420

layout(location = 0) in vec3 in_vertex;
layout(location = 1) in vec3 vertex_texcoord;

uniform mat4 camera_to_clip;
uniform mat4 world_to_camera;

out vec3 vertex_coord;
out vec3 tex_coord;
void main(void) {
    vertex_coord  = (world_to_camera * vec4(in_vertex, 1.0)).xyz;

	gl_Position = camera_to_clip * vec4(vertex_coord,1.0);

	tex_coord = vertex_texcoord;
}
