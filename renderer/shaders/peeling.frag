// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 450
#define NUM_CAMS (12)

in  vec2  screen_space_position;
in  vec3  world_space_position;
in  float depth;
in  vec3  texture_coordinates;
out vec4  frag_color;

uniform float          sigma;
uniform float          depth_alpha;
uniform float          resolution_alpha;

// Input cameras.
//struct CameraInfos
//{
//  mat4 vp;
//  vec3 pos;
//  int selected;
//  vec3 dir;
//};
//// CameraInfos are stored in a contiguous buffer (UBO), lifting most limitations on the number of uniforms.
//layout(std140, binding=3) uniform InputCameras
//{
//  CameraInfos cameras[NUM_CAMS];
//};

uniform int            blend_cost_idx;
uniform int            depth_cost_idx;
uniform int            display_mode;
uniform bool           useIOWeight;
uniform vec3           ncam_pos;
//uniform int			   selected_layer;

uniform int			   shader_idx;

uniform mat4           old_world_to_camera;
uniform mat4           camera_to_clip;
uniform vec2           resolution;

layout(binding=0) uniform sampler2DArray diffuse_sampler;
layout(binding=1) uniform sampler2D		 depth_sampler;
layout(binding=2) uniform sampler2D      previous_sampler;
layout(binding=3) uniform sampler1D      camera_sampler;


float compute_depth_cost_DB(float current_depth, float closest_depth);
float compute_depth_cost_fuzzy(float current_depth, float closest_depth);
float compute_blend_cost_ULR(vec3 world_space_position,
			      vec3 world_space_normal,
			      vec3 output_camera_position,
			      vec3 input_camera_position);
float cost_to_weight_IO(float cost, float prev_cost);
float cost_to_weight(float cost, float prev_cost);

void main()
{
	vec3 output_camera_position = ncam_pos;
	vec4 pixel_color = texture(diffuse_sampler, texture_coordinates);
	
	// comment it out in inputRGBs
    if (pixel_color.a < 0.5) { discard; }

    float current_depth = depth;
	float closest_depth = texture(depth_sampler, screen_space_position).x;
	//float closest_depth = texelFetch(depth_sampler, ivec3(gl_FragCoord.xy, selected_layer), 0).r;//texture(depth_sampler, vec3(screen_space_position, 0)).x;
	//frag_color = vec4(closest_depth, 0.0, 0.0, 1.0);
	//return;

	float depth_cost    = 0.0;
	
	if(depth_cost_idx == 0){ depth_cost = compute_depth_cost_DB(current_depth, closest_depth); }
	else if(depth_cost_idx == 1){ depth_cost = compute_depth_cost_fuzzy(current_depth, closest_depth); }

	if (depth_cost < 0.0) { discard; }

	vec3  world_space_normal    = normalize(cross(dFdx(world_space_position), 
												  dFdy(world_space_position)));
	
	//highp int camera_idx = int(texture_coordinates.z);
    //vec3  input_camera_position = cameras[camera_idx].pos;
	vec3  input_camera_position = texture(camera_sampler, (0.5f + texture_coordinates.z) / NUM_CAMS).xyz;
	float blend_cost            = 0.0;
	
	if(blend_cost_idx == 0) 
	{ 
		blend_cost = compute_blend_cost_ULR(world_space_position,
							  world_space_normal,
							  output_camera_position,
							  input_camera_position);
	}

	float cost = depth_alpha * depth_cost + (1.0f - depth_alpha) * blend_cost;

	if(shader_idx == 0)// Peeling Shader
	{
		// Default assumption is display_mode == 0 for Colors in Peeling shader
		if (display_mode == 1) // Depth
		{
		    float clamped_depth = max(0.1, depth);
		    float disparity = 1.0f / (10.0f * clamped_depth);
		    pixel_color.rgb = vec3(closest_depth, 0.0, 0.0);
		}
		else if (display_mode == 2) // Normals
		{
		    pixel_color.rgb = 0.5f * (1.0f + world_space_normal);
		}
		else if (display_mode == 3) // Output dir
		{
		    vec3 output_dir = normalize(output_camera_position - world_space_position);
		    pixel_color.rgb = 0.5f * (1.0f + output_dir);
		}
		else if (display_mode == 4) // Input dir
		{
		    vec3 input_dir = normalize(input_camera_position - world_space_position);
		    pixel_color.rgb = 0.5f * (1.0f + input_dir);
		}
		else if (display_mode == 5) // Flow
		{
		    vec4 old_position_clip         = camera_to_clip * old_world_to_camera * vec4(world_space_position, 1.0);
		    vec2 old_screen_space_position = (vec2(1.0, 1.0) + (old_position_clip.xy / old_position_clip.w)) * 0.5;
		    old_screen_space_position *= resolution;

		    vec2 flow_to_old = clamp(old_screen_space_position - screen_space_position * resolution,
					      vec2(-512.0, -512.0), vec2(512.0, 512.0));
		    flow_to_old /= 512.0f;
		    flow_to_old = 0.5f * (flow_to_old + 1.0f);
		    pixel_color.rgb = vec3(flow_to_old, 0.5f);
		}

		vec4 previous_color = texture(previous_sampler, screen_space_position);
		if (previous_color.a > 0.0 && cost <= previous_color.a){ discard; }
		pixel_color.a = cost;
	 }
	 else{ // Blend Shader
		float prev_cost = texture(previous_sampler, screen_space_position).a;
		float weight;


		if(useIOWeight){
			weight = cost_to_weight_IO(cost, prev_cost);
		}
		else{
			weight = cost_to_weight(cost, prev_cost);
		}
		
		pixel_color.a = weight;
		pixel_color.xyz *= pixel_color.a;
	 }

	 frag_color = pixel_color; //vec4(current_depth - closest_depth, 0.0, 0.0, 1.0);
	 gl_FragDepth = cost;
	 
}

//==========================================================
// Depth Costs

float compute_depth_cost_DB(float current_depth, float closest_depth)
{
    float min_depth = closest_depth;
    float max_depth = 2.0f * closest_depth;
    return clamp((current_depth - min_depth) / (max_depth - min_depth), 0.0f, 1.0f);
}

float compute_depth_cost_fuzzy(float current_depth, float closest_depth)
{
    float depth_threshold = 1.1 * closest_depth;
    return current_depth > depth_threshold ? -1.0 : 0.0;
}

//==========================================================
// Blend Costs

float compute_blend_cost_ULR(vec3 world_space_position,
			      vec3 world_space_normal,
			      vec3 output_camera_position,
			      vec3 input_camera_position)
{
    float view_angle     = 1.0 / 3.145926536
                         * acos(clamp(dot(normalize(output_camera_position - world_space_position),
                                          normalize(input_camera_position  - world_space_position)), -0.99, 0.99));
    float distance_delta = 1.0 / max(0.01, length(input_camera_position - world_space_position))
                         * max(0.0, length(input_camera_position  - world_space_position) -
                            length(output_camera_position - world_space_position));
    return (1.0 - resolution_alpha) * view_angle + resolution_alpha * distance_delta;
}

//==========================================================
//Cost to weight functions
float cost_to_weight_IO(float cost, float prev_cost)
{
    float weight = 1.0;
    if (prev_cost > 0.0)
        weight = exp(-cost / (sigma * prev_cost));
    return weight;
}

float cost_to_weight(float cost, float prev_cost)
{
    return exp(-cost / sigma);
}



