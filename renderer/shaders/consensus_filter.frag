// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 460
#define NUM_CAMS (12)

layout(location= 0) out vec4 out_color;
layout(location= 1) out vec4 out_layer1;
layout(location= 2) out vec4 out_layer2;
layout(location= 3) out vec4 out_layer3;
layout(location= 4) out vec4 out_layer4;
layout(location= 5) out vec4 out_layer5;

in vec2				   vertex_coord;
uniform int			   selected_layer;
uniform int			   num_iter;
uniform float          resolution_alpha;
uniform vec3           ncam_pos;
uniform float          sigma;
uniform float		   depth_alpha;
uniform float threshold_photo = 0.8;
uniform float threshold_geo = 0.03;

layout(binding=0) uniform sampler2DArray depth_array;
layout(binding=1) uniform sampler2DArray texture_array;
layout(binding=2) uniform sampler2DArray diffuse_sampler;
layout(binding=3) uniform sampler1D      camera_sampler;
layout(binding=4) uniform sampler2D		 mask;
layout(binding=5) uniform sampler2D		 global_depth;
layout(binding=6) uniform sampler2D		 global_buffer;

float cauchyWeight(float x, float c){
    return 1. / (1 + (x*x) / (c*c));
}

float getConsensus(float x[12], float w[12], bool u[12]){

	float sum = 0.0;
	for(int i=0; i<12; i++){
		if(u[i])
		{
			sum = sum + (x[i] * w[i]);
		}
	}
	return sum;
}

float compute_blend_cost_ULR(vec3 world_space_position,
			      vec3 world_space_normal,
			      vec3 output_camera_position,
			      vec3 input_camera_position)
{
    float view_angle     = 1.0 / 3.145926536
                         * acos(clamp(dot(normalize(output_camera_position - world_space_position),
                                          normalize(input_camera_position  - world_space_position)), -0.99, 0.99));
    float distance_delta = 1.0 / max(0.01, length(input_camera_position - world_space_position))
                         * max(0.0, length(input_camera_position  - world_space_position) -
                            length(output_camera_position - world_space_position));
    return (1.0 - resolution_alpha) * view_angle + resolution_alpha * distance_delta;
}



void main(void) {

	ivec2 sampleCoord = ivec2(gl_FragCoord.xy);
	vec4 maskColor =  texelFetch(mask, sampleCoord, 0);
	vec4 g_buffer = texelFetch(global_buffer, sampleCoord, 0);

	if(maskColor.z == 0)
	{
		int numIter = 5;
		
		float depth[12];
		vec3 pix_color[12];
		vec3 world_coord[12];
		vec3 texture_coord[12];
		int weights[12];
		float estimate[12];
		bool used[12];

		float est = 0.0;
		float sum = 0.0;
		int numDataPoints = 0;

		vec2 screen_coord = gl_FragCoord.xy;

		for (int i = 0; i < 12; i++)
		{
			pix_color[i] = vec3(0.0);
		    depth[i] = 0.0;
			used[i] = false;
			weights[i] = 2;
			estimate[i] = 0.0;
		}
		// Read  color and depths from depth_array and fill layer occupancy 
		for (int i = 0; i < 12; i++)
		{
		    depth[i] = texelFetch(depth_array, ivec3(screen_coord, i), 0).a;
			if(depth[i] > 0){
				world_coord[i] = texelFetch(depth_array, ivec3(screen_coord, i), 0).rgb;
				texture_coord[i] = texelFetch(texture_array, ivec3(screen_coord, i), 0).rgb;
				pix_color[i] = texture(diffuse_sampler, texture_coord[i]).rgb;
				used[i] = true;
				//sum += data[i];
				numDataPoints++;
			}
		}

		// Perform KMeans Clustering with K = 2
		int k = 2;

		//int cluster[12];
		float centroids[2] = {0,0};
		float minDist[12];
		int cl=0;
		
		//initialize
		for(int i=0 ; i<12; i++){
			if(used[i]){
				minDist[i] = 100000.0;		
				if(cl < k){
					centroids[cl] = depth[i];
					cl++;
				}
			}
		}
		
		float old_C[2];
		float sumsCluster[2];
		int numCluster[2];
		old_C[0] = centroids[0];
		old_C[1] = centroids[1];
		
		int iter = 0;
		while(iter < 5){
			float dist = 0.0;
			sumsCluster[0] = sumsCluster[1] = 0;
			numCluster[0] = numCluster[1] = 0;
		
			// Cluster Assignment
			for(int cID=0; cID < k; cID++){
				for(int i=0; i<12; i++){
					if(used[i]){
						dist = (depth[i] - centroids[cID]) * (depth[i] - centroids[cID]);
						if(dist < minDist[i]){
							minDist[i] = dist;
							weights[i] = cID;
						}
					}
				}
			}
		
			for(int i=0;i < 12; i++){
				if(used[i]){
					int cID = int(weights[i]);
					sumsCluster[cID] += depth[i];
					numCluster[cID] += 1;
					minDist[i] = 100000.0;	
				}
			}
		
			// Update Cluster Centers
			old_C[0] = centroids[0];
			old_C[1] = centroids[1];
		
			if(numCluster[0] > 0){
				centroids[0] = sumsCluster[0] / (numCluster[0]);
			}
			if(numCluster[1] > 0){
				centroids[1] = sumsCluster[1] / (numCluster[1]);
			}
			iter++;
		
			// if(centroids[0] == old_C[0] && centroids[1] == old_C[1])
			//	  break;
		}

		int winner_cluster;
		float weight = 0.0;
		
		int min_cluster = (centroids[0] <= centroids[1])? 0 : 1;
		int max_cluster = (min_cluster+1)%2;
		winner_cluster = min_cluster;
		
		if(numCluster[max_cluster] >= depth_alpha * numDataPoints){
			winner_cluster = max_cluster;
		}
		
		est = centroids[winner_cluster];
		
		// Geometric uncertainty check
		//float gDepth = -1.0*g_buffer.z;
		//float dep = depth[0];
		//out_layer5 = vec4(dep, dep, dep, 1.0);
		if(abs(est - (-1.0*g_buffer.z)) > threshold_geo){ //  Do PVM blending
		//if(abs(centroids[0] - centroids[1]) > threshold_geo){ //  Do PVM blending
			vec3 final_colour_0 = vec3(0.0);
			vec3 final_colour_1 = vec3(0.0);
			float blend_cost            = 0.0;
			float sumWt[2];
			sumWt[0] = sumWt[1] = 0;
			
			// Blend the slices in each layer by weighting them with consensus weights
			for(int i=0; i<12; i++)
			{
				//final_colour = final_colour + (pix_color[i]);
				//ct++;
				
				vec3  input_camera_position = texture(camera_sampler, 
					(0.5f + texture_coord[i].z) / NUM_CAMS).xyz;
				vec3  world_normal    = normalize(cross(dFdx(world_coord[i]), 
													  dFdy(world_coord[i])));
				blend_cost = compute_blend_cost_ULR(world_coord[i],
								  world_normal,
								  ncam_pos,
								  input_camera_position);
				float weight = exp(-blend_cost/sigma);
				if(used[i] && weights[i] == 0)
				{
					final_colour_0 = final_colour_0 + (weight * pix_color[i]);
					sumWt[0] += weight;
				}
				if(used[i] && weights[i] == 1)
				{		
					final_colour_1 = final_colour_1 + (weight * pix_color[i]);
					sumWt[1] += weight;
				}
				
			}
			

			out_color  = vec4(1.0, 0.0, 0.0, 1.0);
			out_layer1 = vec4(centroids[0], centroids[1], numCluster[0], numCluster[1]);
			out_layer2 = vec4(final_colour_0 / sumWt[0], est);
			out_layer3 = vec4(final_colour_1 / sumWt[1], 1.0);
			out_layer4 = vec4(vec3(0.0), 2.0);
		}
		else{
			
			// Photometric Check
			if(g_buffer.w > threshold_photo){	// DO Textured Mesh
				out_color  = vec4(0.0, 1.0, 0.0, 1.0);
				out_layer1 = vec4(centroids[0], centroids[1], numCluster[0], numCluster[1]);
				out_layer2 = vec4(vec3(0.0), est);
				out_layer3 = vec4(vec3(0.0), 1.0);
				out_layer4 = vec4(vec3(0.0), 2.0);
			}
			else{	// DO ULR
				out_color  = vec4(0.0, 0.0, 1.0, 1.0);
				out_layer1 = vec4(centroids[0], centroids[1], numCluster[0], numCluster[1]);
				out_layer2 = vec4(vec3(0.0), est);
				out_layer3 = vec4(vec3(0.0), 1.0);
				out_layer4 = texelFetch(global_depth, sampleCoord, 0);
			}
		}
		
	}
	else{
		out_color  = vec4(0.0, 0.0, 1.0, 1.0);
		out_layer1 = vec4(0.0);
		out_layer4 = texelFetch(global_depth, sampleCoord, 0);;
		//out_layer2 = vec4(final_colour_0 / sumWt[0], est);
		//out_layer3 = vec4(final_colour_1 / sumWt[1], 1.0);
		//out_layer4 = vec4(weights[9], weights[10], weights[11], 1.0);
	}

}
