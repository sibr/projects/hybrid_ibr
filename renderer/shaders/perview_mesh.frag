// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 420

layout(location= 0) out vec4 out_color;

in vec3 vertex_coord;
in vec3 tex_coord;


layout(binding=0) uniform sampler2DArray diffuse_sampler;

void main(void) {
	vec4 slice_color = texture(diffuse_sampler, tex_coord);
	out_color = vec4(slice_color.rgb, 1.0);
}
