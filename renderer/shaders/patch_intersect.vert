// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 450
#extension ARB_shader_draw_parameters: enable

layout(location = 0) in vec3 in_vertex;
layout(location = 1) in vec3 vertex_texcoord;

uniform mat4 camera_to_clip;
uniform mat4 world_to_camera;
layout(binding=0) uniform isamplerBuffer command_to_layer;

flat out int layer;
out vec3 v_vertex_coordinate;
out vec3 v_tex_coordinate;
out vec3 v_world_coordinate;

void main(void) {

	int id = gl_DrawIDARB;
	layer = texelFetch(command_to_layer, id).r;
	//layer = 0;
    v_vertex_coordinate  = (world_to_camera * vec4(in_vertex, 1.0)).xyz;
	v_tex_coordinate = vertex_texcoord;
	v_world_coordinate = in_vertex;
	gl_Position = camera_to_clip * vec4(v_vertex_coordinate,1.0);
}
