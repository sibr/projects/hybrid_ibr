// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 420

#define EPS 1e-3
in vec2 vertex_coord;
layout(location = 0) out vec4 out_color;
layout(location = 1) out vec4 out_depth;

uniform int kernelSize = 10;				// filter radius, large enough to cover areas of low confidence, decoupled from domain std devs!

// Bound Textures
//layout(binding=0) uniform sampler2D texturedMeshTex;

layout(binding=0) uniform sampler2D maskTex;
layout(binding=1) uniform sampler2D	global_depth;


void main(void){
    ivec2 sampleCoord = ivec2(gl_FragCoord.xy);
	ivec2 fSize = textureSize(maskTex, 0);
	float sigma = kernelSize/3.0f;
		
	vec4 sum = vec4(0);
	float weightSum = 0;
	
	for (int x = -kernelSize; x <= kernelSize; x++) {
		for (int y = -kernelSize; y <= kernelSize; y++)
		{
			ivec2 coord = sampleCoord + ivec2(x, y);
			if (coord.x >= 0 && coord.x < fSize.x &&
				coord.y >= 0 && coord.y < fSize.y)
			{
				vec4 value = texelFetch(maskTex, coord, 0);
				float weight = exp(-0.5*(x*x + y*y) / (sigma*sigma));
				
				sum += value * vec4(weight);
				weightSum += weight;
			}
		}
	}
	
	if (weightSum == 0) 
		weightSum = 1;
	
	out_color =  vec4((sum / weightSum).xyz, 1.0);
	//out_color.xyz = texelFetch(maskTex, sampleCoord, 0).xyz;
	if(out_color.z > 0.0){
		out_depth = texelFetch(global_depth, sampleCoord, 0);
	}
	else{
		out_depth = vec4(vec3(0.0), 2.0);
	}
}



//==========Helpers.===========================================

