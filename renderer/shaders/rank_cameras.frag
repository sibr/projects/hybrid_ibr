// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 430

// this will be set automatically
#define MAX_IN_CAMERAS 8

#define MAX_OUT_CAMERAS 12
#define M_PI 3.1415926536

layout(local_size_x = 128) in;

layout(binding = 0, r32i) writeonly uniform iimageBuffer outputBuffer;

layout(binding = 1) uniform usamplerBuffer visibleVoxels;
layout(binding = 2) uniform usamplerBuffer fineVoxelComponentIndices;
layout(binding = 3) uniform isamplerBuffer fineVoxelComponents;
layout(binding = 4) uniform isamplerBuffer selectedCameras;
layout(binding = 5) uniform samplerBuffer componentsPosition;


uniform ivec3 fineGridSize;
uniform vec3 fineGridMin;
uniform vec3 fineGridMax;
uniform vec3 cameraPosition;
uniform float resolutionAlpha = 0.1; // TODO: link this to the outside

//============================================================

struct CostIndex
{
	float cost;
	int index;
};

CostIndex bin_list[MAX_IN_CAMERAS];

//============================================================

ivec3 index1Dto3D(uint linear)
{
	if (linear < fineGridSize.x * fineGridSize.y * fineGridSize.z && linear > 0)
	{
		return ivec3(
			mod(linear, fineGridSize.x),
			mod(linear / fineGridSize.x, fineGridSize.y),
			linear / (fineGridSize.y * fineGridSize.x));
	}
	return ivec3(0);
}

//============================================================

void bubbleSort(int fullSize, int sortSize)
{
	CostIndex temp;
	for (int i = 0; i < sortSize; i++)
	{
		bool sorted = true;
		for (int j = fullSize - 1; j > 0; j--)
		{
			if (bin_list[j].cost < bin_list[j-1].cost)
			{
				temp = bin_list[j];
				bin_list[j] = bin_list[j-1];
				bin_list[j-1] = temp;
				sorted = false;
			}
		}
		if (sorted) return;
	}
}

//============================================================

void main()
{
	int threadID = int(gl_GlobalInvocationID.x);
	
	if (threadID >= textureSize(visibleVoxels)) return;
	
	int linIdx = int(texelFetch(visibleVoxels, threadID).r);
	
	uint start_component_index = (linIdx == 0) ? 0 : texelFetch(fineVoxelComponentIndices, linIdx - 1).r;
	uint end_component_index = texelFetch(fineVoxelComponentIndices, linIdx).r;
	
	ivec3 selectedVoxel = index1Dto3D(linIdx);
	
	vec3 bbox_min_grid = vec3(selectedVoxel);
	vec3 bbox_max_grid = bbox_min_grid + vec3(1.);
	vec3 bbox_min_unit = bbox_min_grid / fineGridSize;
	vec3 bbox_max_unit = bbox_max_grid / fineGridSize;
	vec3 bbox_min = bbox_min_unit * (fineGridMax - fineGridMin) + fineGridMin;
	vec3 bbox_max = bbox_max_unit * (fineGridMax - fineGridMin) + fineGridMin;

	vec3 center = 0.5 * (bbox_max + bbox_min);
	vec3 delta  = 0.5 * (bbox_max - bbox_min);
	float voxel_radius = length(delta);

	// compute distance from current voxel center to camera
	vec3 virt_to_center = cameraPosition - center;
	vec3 min_diff_virt = max(abs(virt_to_center) - delta, vec3(0));
	float min_dist_to_virt = length(min_diff_virt);
	float v2c_dist = length(virt_to_center);
	float inv_v2c_dist = 1. / v2c_dist;

	int counter = 0;

	for (uint components = start_component_index; components < end_component_index; components++)
	{
		int j = texelFetch(fineVoxelComponents, int(components)).x;

		// skip component if the camera is not selected or ignored or exceeds components size (false cam)
		bool selected = bool(texelFetch(selectedCameras, j).x);
		if (selected)
		{
			vec3 compPosition = texelFetch(componentsPosition, j).xyz;
			
			// Compute distance of component to voxel center
			vec3 real_to_center = compPosition - center;
			vec3 max_diff_real = abs(real_to_center) + delta;
			float max_dist_to_real = length(max_diff_real);

			// Compute minimum angular distance between eye and voxel
			vec3 virt_to_real = normalize(compPosition - cameraPosition);
			float min_angle_virt = 0;

			if (voxel_radius < v2c_dist)
			{
				min_angle_virt = 
					  acos(-dot(virt_to_center, virt_to_real) * inv_v2c_dist)
					- asin(voxel_radius * inv_v2c_dist);
			}

			// Compute minimum angular distance between eye and component (camera) position
			float min_angle_real = 0;
			float r2c_dist = length(real_to_center);
			float inv_r2c_dist = 1. / r2c_dist;
			if (voxel_radius < r2c_dist)
			{
				min_angle_real = 
					  acos(-dot(real_to_center, -virt_to_real) * inv_r2c_dist)
					- asin(voxel_radius * inv_r2c_dist);
			}
			float max_angle_real_virt = M_PI - min_angle_real - min_angle_virt;
			float max_distance_cost = max(0, 1 - min_dist_to_virt / max_dist_to_real);
			float max_cost = 
				(1 - resolutionAlpha) * max_angle_real_virt / M_PI
				+ resolutionAlpha * max_distance_cost;
			
			// store max cost of each component (camera) whose slice is present in the voxel
			bin_list[counter] = CostIndex(max_cost, j);
			counter++;
		}
	}

	bubbleSort(counter, MAX_OUT_CAMERAS);
		
	for (int i = 0; i < MAX_OUT_CAMERAS; i++)
	{
		int idx = (i < counter) ? bin_list[i].index : -1;
		imageStore(outputBuffer, threadID * MAX_OUT_CAMERAS + i, ivec4(idx));
	}

}