// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 420

in vec2 vertex_coord;
layout(location = 0) out vec4 out_color;


// Input textures.
layout(binding=0) uniform sampler2D ibrNextTex;
layout(binding=1) uniform sampler2D texturedMeshTex;
layout(binding=2) uniform sampler2D ulrTex;
layout(binding=3) uniform sampler2D maskTex;

// Uniforms.
uniform bool renderMode = true;
uniform bool toggleMask = false;
uniform int kernelSize = 10;				// filter radius, large enough to cover areas of low confidence, decoupled from domain std devs!


vec3 computeWeightedMask();

void main(void){
    vec3 ibrNext = texture(ibrNextTex, vertex_coord).rgb;
	vec3 ulr     = texture(ulrTex, vertex_coord).rgb;
	vec3 tm      = texture(texturedMeshTex, vertex_coord).rgb;
	
	vec3 maskColr= computeWeightedMask();

	if(renderMode){
		out_color.xyz = ibrNext;
	}
	else{
		if(toggleMask){
			out_color.xyz = maskColr;
		}
		else
		{
			out_color.xyz = ibrNext * maskColr.x + 
							tm * maskColr.y +
							ulr * maskColr.z;
			
		}
	}

	out_color.w = 1.0;
}

//==========Helpers.===========================================
vec3 computeWeightedMask(){
	if(kernelSize ==  0)
	{
		return texture(maskTex, vertex_coord).xyz;
	}

	ivec2 sampleCoord = ivec2(gl_FragCoord.xy);
	ivec2 fSize = textureSize(maskTex, 0);
	float sigma = kernelSize/3.0f;
		
	vec4 sum = vec4(0);
	float weightSum = 0;
	
	for (int x = -kernelSize; x <= kernelSize; x++) {
		for (int y = -kernelSize; y <= kernelSize; y++)
		{
			ivec2 coord = sampleCoord + ivec2(x, y);
			if (coord.x >= 0 && coord.x < fSize.x &&
				coord.y >= 0 && coord.y < fSize.y)
			{
				vec4 value = texelFetch(maskTex, coord, 0);
				float weight = exp(-0.5*(x*x + y*y) / (sigma*sigma));
				
				sum += value * vec4(weight);
				weightSum += weight;
			}
		}
	}
	
	if (weightSum == 0) 
		weightSum = 1;
	
	return vec4(sum / weightSum).xyz;
	
}
