// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 420


uniform mat4 camera_to_clip;
uniform mat4 world_to_camera;

layout(location = 0) in vec3 in_vertex;
layout(location = 1) in vec3 in_color;
layout(location = 2) in vec2 in_texcoord;
layout(location = 3) in vec3 in_normal;

out vec3 vertex_coord;
out vec3 vertex_col;
out vec2 tex_coord;
out vec3 vertex_normal;
out vec3 world_coord;

void main(void) {
	vertex_coord  = (world_to_camera * vec4(in_vertex, 1.0)).xyz;
    world_coord  = in_vertex;
	vertex_col = in_color;
	vec2 uv = in_texcoord;
	uv.y = 1.0 - uv.y ;
	tex_coord = uv;
	vertex_normal = in_normal;
	gl_Position = camera_to_clip * vec4(vertex_coord,1.0);
}
