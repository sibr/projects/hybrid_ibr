// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 420

#define EPS 1e-3

in vec2 vertex_coord;
layout(location = 0) out vec4 out_color;

// filter radius, large enough to cover areas of low confidence, decoupled from domain std devs!
uniform int kernelSize;
uniform float alpha;
uniform float exps;

// Input textures.
layout(binding=0) uniform sampler2D mask;
layout(binding=1) uniform sampler2D layerDepthsCentroids;
layout(binding=2) uniform sampler2D outColor0;
layout(binding=3) uniform sampler2D outColor1;
layout(binding=4) uniform sampler2D outColor2;

void main() {


	ivec2 sampleCoord = ivec2(gl_FragCoord.xy);
	vec4 maskColor =  texelFetch(mask, sampleCoord, 0);
	
	if(maskColor.r == 1){
		ivec2 fSize = textureSize(layerDepthsCentroids, 0);
		float sigma = kernelSize/3.0f;
		vec2 weightCluster = vec2(0.0);
		weightCluster[0] = weightCluster[1] = 0;
		vec2 sum = vec2(0.0);
		vec2 weightSum = vec2(0.0);
		vec4 cluster0Col = texelFetch(outColor0, sampleCoord, 0);
		vec4 cluster1Col = texelFetch(outColor1, sampleCoord, 0);
		vec2 depthCandidate = texelFetch(layerDepthsCentroids, sampleCoord, 0).xy;
		
		for (int x = -kernelSize; x <= kernelSize; x++) 
		{
			for (int y = -kernelSize; y <= kernelSize; y++)
			{
				ivec2 coord = sampleCoord + ivec2(x, y);
				if (coord.x >= 0 && coord.x < fSize.x &&
					coord.y >= 0 && coord.y < fSize.y)
				{
					vec4 dNbr = texelFetch(layerDepthsCentroids, coord, 0);
					//vec2 nNbr = texelFetch(layerDepthsCentroids, coord, 0).zw;
					for(int k = 0; k < 2; k++){
						float depthNbr = dNbr[k];
						float numClusterNbr = dNbr[k+2];
						//float depthW = 1.0;
						vec2 depthW = exp(-1.0 * alpha * abs(depthNbr - depthCandidate));
						float weight =  exp(-0.5*(x*x + y*y) / (sigma*sigma));
						vec2 weightedVote = weight * depthW * numClusterNbr;
						sum += weightedVote;
						weightSum += depthW * weight;
					}
				}
			}
		}
		weightCluster = vec2(sum.x, sum.y); 
		
		weightCluster.x = pow(weightCluster.x, exps);
		weightCluster.y = pow(weightCluster.y, exps);
		float totalwt = weightCluster.x + weightCluster.y;
		
		out_color = ((weightCluster.x * cluster0Col) + (weightCluster.y * cluster1Col)) / totalwt;
		
	}
	else{
		out_color = texelFetch(outColor2, sampleCoord, 0);
		out_color.w = 1.0;
	}
}