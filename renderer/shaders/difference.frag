// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 420

in vec2 vertex_coord;
layout(location = 0) out vec4 out_color;


uniform vec3 grid_min;
uniform vec3 grid_max;
uniform float compositingMargin;

// Input textures.
layout(binding=0) uniform sampler2D gDepth;
layout(binding=1) uniform sampler2D texture2;

void main(void){
  	
    vec3 world_coord = texture(gDepth, vertex_coord).rgb;
    vec3 dbColor = texture(texture2, vertex_coord).rgb;
    //out_color.xyz = dbColor;
    //out_color.w = 1.0;
    //return;
    vec3 borderSize = (grid_max - grid_min) * compositingMargin;
	if (world_coord.x < borderSize.x + grid_min.x || world_coord.x > grid_max.x - borderSize.x  ||
		world_coord.y < borderSize.y + grid_min.y || world_coord.y > grid_max.y - borderSize.y  ||
		world_coord.z < borderSize.z + grid_min.z || world_coord.z > grid_max.z - borderSize.z )
	{
        discard;
    }
    out_color.xyz = dbColor;
    out_color.w = 1.0;
}