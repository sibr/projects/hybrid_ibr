// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 450

layout(location=0) out vec4 out_depth;
layout(location=1) out vec4 out_texture;

flat in int layer;
in vec3 vertex_coordinate;
in vec3 texture_coordinates;
in vec3 world_coordinates;
void main(void) {
	//vec4 pixel_color = texture(diffuse_sampler, texture_coordinates);
	out_depth = vec4(world_coordinates, -vertex_coordinate.z);
	out_texture = vec4(texture_coordinates, 1.0);
	//out_color = vec4(-vertex_coordinate.z, 0.0, 0.0, 1.0);
	
}
