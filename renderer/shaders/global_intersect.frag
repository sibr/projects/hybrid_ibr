// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 420

layout(location = 0) out vec4 out_depth;
in vec3 world_coord;

void main(void) {
	out_depth =  vec4(world_coord, gl_FragCoord.z);
}
