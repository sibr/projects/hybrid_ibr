// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 420

#define EPS 1e-3
in vec2 vertex_coord;
layout(location = 0) out vec4 out_color;

uniform float threshold = 0.25;

// Input textures.
layout(binding=0) uniform sampler2D texture1;


void main(void){
  	
    vec3 texture1Color = texture(texture1, vertex_coord).rgb;
    normalize(texture1Color);
    // Output the absolute difference in pixel values for the two input textures
    out_color.w = 1.0;
    // Y = 0.299*R+0.587*G+0.114*B
    float lum = 0.299*texture1Color.x + 0.587*texture1Color.y + 0.114*texture1Color.z;

    if(lum - threshold < EPS){
        discard;
    }

    //out_color.xyz = vec3(lum, lum/0.5, 1.0 - lum);
    out_color.xyz = vec3(lum, lum, lum);
}