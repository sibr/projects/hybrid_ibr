// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 450

layout (triangles) in;
layout (triangle_strip, max_vertices=3) out;

flat in int layer[];
in vec3 v_vertex_coordinate[];
in vec3 v_tex_coordinate[];
in vec3 v_world_coordinate[];

out vec3 vertex_coordinate;
out vec3 texture_coordinates;
out vec3 world_coordinates;

flat out int lay;
void main(void) {
	//gl_Layer = 0;
	// Output all three vertices
	for (int i = 0; i < 3; i++)
	{
		gl_Layer = layer[i];
		//lay = layer[i];
		gl_Position = gl_in[i].gl_Position;
		vertex_coordinate = v_vertex_coordinate[i];
		texture_coordinates = v_tex_coordinate[i];
		world_coordinates = v_world_coordinate[i];
		EmitVertex();
	}
	EndPrimitive();
}