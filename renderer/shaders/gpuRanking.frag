// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 420

#define EPS 1e-3

in vec2 vertex_coord;
layout(location = 0) out vec4 out_color;


void main() {
	
	out_color = vec4(1.0);
}
/*
ivec2 getIndex1Dto2D(int i, ivec2 res) {
	if (i < res.x * res.y && i >= 0)
		return ivec2(mod(i, res.x), i / res.x);
	else
		return ivec2(0);
}


int getIndex2Dto1D(ivec2 v, ivec2 res) {
	if (v.x >= 0 && v.x < res.x && v.y >= 0 && v.y < res.y)
		return v.y * res.x + v.x;
	else
		return 0;
}


ivec3 getIndex1Dto3D(int i, ivec3 res) {
	if (i < res.x  res.y  res.z && i >= 0)
		return ivec3(
			mod(i, res.z),
			mod(i / res.z, res.y),
			i / (res.y * res.z));	
	else
		return ivec3(0);
}
*/