// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 450

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_texcoord;

uniform mat4 world_to_camera;
uniform mat4 camera_to_clip;

out vec2  screen_space_position;
out vec3  world_space_position;
out float depth;
out vec3  texture_coordinates;

void main()
{
    vec4 position_cam  = world_to_camera * vec4(vertex_position, 1.0);
    vec4 position_clip = camera_to_clip * position_cam;

    world_space_position  = vertex_position;
    texture_coordinates   = vertex_texcoord;
    depth                 = -position_cam.z;
    screen_space_position = (vec2(1.0, 1.0) + (position_clip.xy / position_clip.w)) * 0.5;

    gl_Position = position_clip;
}
