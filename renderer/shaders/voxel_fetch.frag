// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 450
#extension GL_ARB_shader_image_load_store : enable
#define GRID_SIZE_X (32)
#define GRID_SIZE_Y (32)
#define GRID_SIZE_Z (32)

layout(r32ui, binding = 0) uniform uimage2D gpu_grid_a;
layout(r32ui, binding = 1) uniform uimage2D gpu_grid_b;
layout(r32ui, binding = 2) uniform uimage2D gpu_grid_c;
layout(r32ui, binding = 3) uniform uimage2D gpu_grid_d;

layout(binding = 4) uniform sampler2D tex;
layout(binding = 5) uniform sampler2D spectex;


uniform vec3 ncam_pos;
uniform vec3 grid_min;
uniform vec3 grid_max;
uniform float tolerance;
uniform float threshold;
uniform bool neighborhood;
uniform float compositingMargin;

in vec3 world_coord;
in vec3 vertex_coord;
in vec3 vertex_col;
in vec2 tex_coord;
in vec3 vertex_normal;


layout(location= 0) out vec4 out_color;
layout(location= 1) out vec4 out_pos;
layout(location= 2) out vec4 out_norm;
layout(location= 3) out vec4 out_mask;

void set_voxel(uint grid_x, uint grid_y, uint grid_z);

void main(void) {
	vec4 position = vec4(world_coord, gl_FragCoord.z);
	//vec2 texcoord = tex_coord ;
	out_color = texture(tex, tex_coord);
	out_pos = position;
	out_norm = vec4(vertex_coord, texture(spectex, tex_coord).x);	

    vec3 borderSize = (grid_max - grid_min) * compositingMargin;
	if (world_coord.x < borderSize.x + grid_min.x || world_coord.x > grid_max.x - borderSize.x  ||
		world_coord.y < borderSize.y + grid_min.y || world_coord.y > grid_max.y - borderSize.y  ||
		world_coord.z < borderSize.z + grid_min.z || world_coord.z > grid_max.z - borderSize.z )
	{
	
		//out_color.w = 1.0;
		//out_pos = position;
		
		out_mask = vec4(0.0, 0.0, 1.0,1.0);
	}
	else if (world_coord.x > grid_min.x || world_coord.x < grid_max.x  ||
			 world_coord.y > grid_min.y || world_coord.y < grid_max.y  ||
			 world_coord.z > grid_min.z || world_coord.z < grid_max.z )
	{
		if(length(position.xyz - ncam_pos) < (threshold * length(grid_max - grid_min))) { 
			float radius_sfm = -position.z * tolerance;

			vec3 normalized_position = (position.xyz - grid_min) / (grid_max - grid_min);
			if (min(normalized_position.x, min(normalized_position.y, normalized_position.z)) < 0.0 ||
				max(normalized_position.x, max(normalized_position.y, normalized_position.z)) > 1.0)
				discard;

			normalized_position = clamp(normalized_position, vec3(0, 0, 0), vec3(1, 1, 1));
			
			uint grid_x = min(GRID_SIZE_X - 1, int(normalized_position.x * GRID_SIZE_X));
			uint grid_y = min(GRID_SIZE_Y - 1, int(normalized_position.y * GRID_SIZE_Y));
			uint grid_z = min(GRID_SIZE_Z - 1, int(normalized_position.z * GRID_SIZE_Z));

			float grid_radius = length(vec3(GRID_SIZE_X, GRID_SIZE_Y, GRID_SIZE_Z))
				* radius_sfm / length(grid_max - grid_min);
			float gridf_x = normalized_position.x * GRID_SIZE_X;
			float gridf_y = normalized_position.y * GRID_SIZE_Y;
			float gridf_z = normalized_position.z * GRID_SIZE_Z;

			set_voxel(grid_x, grid_y, grid_z);

			if(neighborhood){
				uint next_x = -1;
				if (gridf_x - grid_x > 0.5 && grid_x < GRID_SIZE_X - 1)
					next_x = grid_x + 1;
				else if (grid_x > 0)
					next_x = grid_x - 1;

				uint next_y = -1;
				if (gridf_y - grid_y > 0.5 && grid_y < GRID_SIZE_Y - 1)
					next_y = grid_y + 1;
				else if (grid_y > 0)
					next_y = grid_y - 1;

				uint next_z = -1;
				if (gridf_z - grid_z > 0.5 && grid_z < GRID_SIZE_Z - 1)
					next_z = grid_z + 1;
				else if (grid_z > 0)
					next_z = grid_z - 1;

				if (next_x >= 0 && abs(gridf_x - next_x) < grid_radius)
					set_voxel(next_x, grid_y, grid_z);
				if (next_y >= 0 && abs(gridf_y - next_y) < grid_radius)
					set_voxel(grid_x, next_y, grid_z);
				if (next_z >= 0 && abs(gridf_z - next_z) < grid_radius)
					set_voxel(grid_x, grid_y, next_z);

				if (next_x >= 0 && next_y >= 0 &&
					length(vec2(gridf_x, gridf_y) - vec2(next_x, next_y)) < grid_radius)
					set_voxel(next_x, next_y, grid_z);
				if (next_x >= 0 && next_z >= 0 &&
					length(vec2(gridf_x, gridf_z) - vec2(next_x, next_z)) < grid_radius)
					set_voxel(next_x, grid_y, next_z);
				if (next_x >= 0 && next_z >= 0 &&
					length(vec2(gridf_y, gridf_z) - vec2(next_y, next_z)) < grid_radius)
					set_voxel(grid_x, next_y, next_z);

				if (next_x >= 0 && next_y >= 0 && next_z >= 0 &&
					length(vec3(gridf_x, gridf_y, gridf_z) - vec3(next_x, next_y, next_z)) < grid_radius)
					set_voxel(next_x, next_y, next_z);
			}
		}
		
		//out_pos.xyz = position.xyz;
		//out_pos.w = 2.0;
		//out_norm.xyz = vec3(vertex_coord)
		out_mask = vec4(1.0,0.0,0.0,1.0);
	}
}

//===============Helpers=============================

void set_voxel(uint grid_x, uint grid_y, uint grid_z)
{
	uint channel_idx = grid_x / 32;
	uint channel_bit = grid_x - channel_idx * 32;
	uint channel_mask = 1 << channel_bit;

	if (channel_idx == 0)
		imageAtomicOr(gpu_grid_a, ivec2(grid_y, grid_z), channel_mask);
	if (channel_idx == 1)
		imageAtomicOr(gpu_grid_b, ivec2(grid_y, grid_z), channel_mask);
	if (channel_idx == 2)
		imageAtomicOr(gpu_grid_c, ivec2(grid_y, grid_z), channel_mask);
	if (channel_idx == 3)
		imageAtomicOr(gpu_grid_d, ivec2(grid_y, grid_z), channel_mask);
}
