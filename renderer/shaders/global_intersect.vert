// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 420

uniform mat4 proj;

layout(location = 0) in vec3 in_vertex;

out vec3 world_coord;

void main(void) {
	world_coord = in_vertex; 
	gl_Position = proj * vec4(in_vertex,1.0);
}
