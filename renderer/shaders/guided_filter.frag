// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#version 420

#define EPS 1e-3

in vec2 vertex_coord;
layout(location = 0) out vec4 out_color;

uniform int kernelSize = 10;				// filter radius, large enough to cover areas of low confidence, decoupled from domain std devs!
uniform bool variableSigma = false;
uniform float threshold = 0.1;

// Input textures.
layout(binding=0) uniform sampler2D ibrNextTex;
layout(binding=1) uniform sampler2D texturedMeshTex;

void main() {
	ivec2 sampleCoord = ivec2(gl_FragCoord.xy);
	ivec2 fSize = textureSize(ibrNextTex, 0);
	float sigma = kernelSize/3.0f;
	float var = texelFetch(texturedMeshTex, sampleCoord, 0).w;
	if(var - threshold  > EPS){
		if(variableSigma){	sigma = sigma * var;}
		
		vec4 sum = vec4(0);
		float weightSum = 0;

		for (int x = -kernelSize; x <= kernelSize; x++) {
			for (int y = -kernelSize; y <= kernelSize; y++)
			{
				ivec2 coord = sampleCoord + ivec2(x, y);
				if (coord.x >= 0 && coord.x < fSize.x &&
					coord.y >= 0 && coord.y < fSize.y)
				{
					vec4 value = texelFetch(ibrNextTex, coord, 0);
					float weight = exp(-0.5*(x*x + y*y) / (sigma*sigma));
					// *texelFetch(ibrNextTex, coord, 0).a;
					
					sum += value * vec4(weight);
					weightSum += weight;
				}
			}
		}
		
		if (weightSum == 0) weightSum = 1;
		
		out_color = vec4(sum / weightSum);
	}	
	else{
		out_color = texelFetch(ibrNextTex, sampleCoord, 0);
	}
}