// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#include "SpecularMaskGenerator.hpp"


#define LARGE_PENALTY 10.0
#define ALPHA 1.0		// Variance
#define BETA  2.5		// Difference
#define GAMMA 1.0		// Smoothness
#define DELTA 2.0		// Spatial distance

namespace sibr {

	SpecularMaskGenerator::SpecularMaskGenerator(sibr::ImageRGB32F& image, sibr::ImageRGB32F& varMap, sibr::ImageRGB32F& diffMap, sibr::ImageRGB32F& gradX, sibr::ImageRGB32F& gradY)
	{
		_image = image.clone();
		_varMap = varMap.clone();
		_diffMap = diffMap.clone();
		_gradX = gradX.clone();
		_gradY = gradY.clone();

		_w = image.w();
		_h = image.h();
		_wExtMargin = uint(0.01 * _w);
		_hExtMargin = uint(0.01 * _h);
		_wIntMargin = uint(0.35 * _w);
		_hIntMargin = uint(0.35 * _h);

		using namespace std::placeholders;

		std::shared_ptr< FuncItoF > unaryLabelOnly_ptr;
		std::shared_ptr< Func2ItoF > unaryFull_ptr;
		std::shared_ptr< Func2ItoF > pairwiseLabelsOnly_ptr;
		std::shared_ptr< Func4ItoF > pairwiseFull_ptr;

		unaryLabelOnly_ptr = std::shared_ptr< FuncItoF >();
		pairwiseLabelsOnly_ptr = std::shared_ptr< Func2ItoF >();
		unaryFull_ptr = std::shared_ptr<Func2ItoF >(new Func2ItoF(std::bind(&SpecularMaskGenerator::unaryCost, this, _1, _2)));
		pairwiseFull_ptr = std::shared_ptr< Func4ItoF >(new Func4ItoF(std::bind(&SpecularMaskGenerator::binaryCost, this, _1, _2, _3, _4)));
		
		// Fill the neighbours map, this is a grid where each pixel is linked to its four neighbours.
		_neighboursMap.resize(_w * _h);
		for (uint y = 0; y < _h; ++y) {
			for (uint x = 0; x < _w; ++x) {
				const int currentIndex = pixelToArray(x, y);
				std::vector<int> neighbours;
				if (x > 0) {
					neighbours.push_back(pixelToArray(x - 1, y));
				}
				if (y > 0) {
					neighbours.push_back(pixelToArray(x, y - 1));
				}
				if (x < _w - 1) {
					neighbours.push_back(pixelToArray(x + 1, y));
				}
				if (y < _h - 1) {
					neighbours.push_back(pixelToArray(x, y + 1));
				}
				_neighboursMap[currentIndex] = neighbours;
			}
		}
		// labels for the optimisation
		std::vector<int> labels = { 1,2 };

		_mrfSolver = std::shared_ptr<sibr::MRFSolver>(new sibr::MRFSolver(labels, &_neighboursMap, 1,
			unaryLabelOnly_ptr,
			unaryFull_ptr,
			pairwiseLabelsOnly_ptr,
			pairwiseFull_ptr
		));
	}

	double SpecularMaskGenerator::unaryCost(int p, int l)
	{
		const sibr::Vector2i pixel = arrayToPixel(p);


		const sibr::Vector2i centre_pos = Vector2i(_w / 2, _h / 2);
		const sibr::Vector2f cur_pos = Vector2f(abs(pixel.x() - centre_pos.x()) * 1.0 / _w , 
												abs(pixel.y() - centre_pos.y()) * 1.0 / _h);
		const double distance = cur_pos.norm();// *1.0 / centre_pos.squaredNorm();
		// Spatial position constraint
		if (distance > 0.5) {
			return (l == 2) ? 0.0 : distance;// exp(distance - 0.4) - 1;
		}
				
		// if a pixel is assigned label 2 and has high intensity (var or diff) it should have a high cost, 
		// and if low intensity (var or diff) then it should have a low cost. 
		
		const double variance = _varMap(pixel.x(), pixel.y()).cast<double>().x();
		const double difference = _diffMap(pixel.x(), pixel.y()).cast<double>().x();

		//const double alpha = 1.0, beta = 1.0;

		const double g1 = ALPHA * variance;
		const double g2 = BETA * difference;

		if (l == 2) {
			return exp((g1 + g2));
		}
		else {
			return exp(1.0 - ((g1 + g2)));// *(1 + (DELTA * distance));
		}
	}

	double SpecularMaskGenerator::binaryCost(int p1, int p2, int l1, int l2)
	{
		//if (l1 == l2) { return 0.0; }
		//
		//return 1.0;

		const sibr::Vector2i pixel1 = arrayToPixel(p1);
		const sibr::Vector2i pixel2 = arrayToPixel(p2);

		//const sibr::Vector3d g1X = _gradX(pixel1.x(), pixel1.y()).cast<double>() ;
		//const sibr::Vector3d g2X = _gradX(pixel2.x(), pixel2.y()).cast<double>() ;
		//const sibr::Vector3d g1Y = _gradY(pixel1.x(), pixel1.y()).cast<double>() ;
		//const sibr::Vector3d g2Y = _gradY(pixel2.x(), pixel2.y()).cast<double>() ;
		//
		//const sibr::Vector3d g_flowX = g1X - g2X;
		//const sibr::Vector3d g_flowY = g1Y - g2Y;
		const sibr::Vector3f img1 = _image(pixel1);
		const sibr::Vector3f img2 = _image(pixel2);

		const double mag_flow = GAMMA * (img1 - img2).squaredNorm();

		
		if (l1 == l2) { return exp(mag_flow); }

		
		return exp(1.0 - mag_flow);//*LARGE_PENALTY;
		
	}

	void SpecularMaskGenerator::solve()
	{
		_mrfSolver->solveBinaryLabels();
	}

	const int SpecularMaskGenerator::pixelToArray(const int x, const int y) const
	{
		return y * _w + x;
	}

	const sibr::Vector2i SpecularMaskGenerator::arrayToPixel(const int i) const
	{
		return sibr::Vector2i(i % _w, i / _w);
	}

	sibr::ImageL8 SpecularMaskGenerator::getResult()
	{
		std::vector<int> labellingSP = _mrfSolver->getLabels();

		sibr::ImageL8 result(_w, _h);
		sibr::ColorRGBA zeroP(0.0f, 0.0f, 0.0f, 0.0f);
		sibr::ColorRGBA fullP(1.0f, 1.0f, 1.0f, 1.0f);
		for (size_t id = 0; id < labellingSP.size(); ++id) {
			sibr::Vector2i pixel = arrayToPixel((int)id);
			result.color(pixel.x(), pixel.y(), labellingSP[id] > 1 ? fullP : zeroP);

		}
		return result;
	}

	sibr::ImageRGB32F SpecularMaskGenerator::getOverlayResult()
	{
		sibr::ImageRGB32F result = _image.clone();
		std::vector<int> labellingSP = _mrfSolver->getLabels();

		for (size_t id = 0; id < labellingSP.size(); ++id) {
			sibr::Vector2i pixel = arrayToPixel((int)id);
			result(pixel) = result(pixel.x(), pixel.y()) / (labellingSP[id] > 1 ? 1 : 2);
		}
		return result;
	}

}
