// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#pragma once

#include "Config.hpp"

#include "projects/hybrid_ibr/renderer/patch_cloud.h"
#include "projects/hybrid_ibr/renderer/mesh_ray_tracer.h"


#include "projects/fribr_framework/renderer/tools/string_tools.h"
#include "projects/fribr_framework/renderer/io/bundler.h"
#include "core/scene/BasicIBRScene.hpp"
#include "core/graphics/Texture.hpp"
#include "core/graphics/Window.hpp"
#include <Eigen/Core>


namespace sibr {

	struct HybridAppArgs :
		virtual sibr::BasicIBRAppArgs {
		Arg<std::string> meshPath = { "mesh", "" };
		Arg<std::string> texturePath = { "texture", "" };
		Arg<bool> compareMode = { "compare" };
		Arg<bool> compositeMode = { "composite" };
		Arg<bool> param_sweep = { "sweep" };
		Arg<bool> jitter_mode = { "jitter" };
		Arg<bool> test_mode = { "gt" };
		Arg<bool> topView = { "topView" };
		Arg<bool> meshDebugView = { "meshDebug" };
		Arg<bool> noPC = { "noPC" };
		Arg<bool> gui = { "gui" };
		Arg<bool> exposure = { "exp" };

		Arg<std::string> pcloudPath = { "pcloud", "output_hybrid.pcloud" };
		Arg<std::string> fullMeshPath = { "fullMesh", "" };
		Arg<std::string> harmonizedPath = { "harmonized", "hybrid/harmonized_images" };
		Arg<std::string> outputPath = { "output", "" };
		Arg<int>		 cams_spec = { "cams", 0 };
		Arg<int>		 vert_spec = { "verts", 1 };
		Arg<int>		 start_cam = { "start", 0 };
		Arg<int>		 mode = { "mode", 2 };
		Arg<bool> baselineComp = { "baseComp" };
	};

	struct HybridSceneOpts :
		virtual sibr::BasicIBRScene::SceneOptions {
		bool		harmonizedPatchCloud = false;
		bool		patchClouds = true;
	};

	class SIBR_EXP_HYBRID_EXPORT HybridScene : public sibr::BasicIBRScene
	{
		SIBR_DISALLOW_COPY(HybridScene);

	public:
		typedef std::shared_ptr<HybridScene>		Ptr;
		//DeepBlendingScene::DeepBlendingScene();
		HybridScene::HybridScene(HybridAppArgs& myArgs, sibr::HybridSceneOpts myOpts);
		HybridScene::HybridScene(const BasicIBRScene::Ptr& scene, HybridAppArgs& myArgs);

		/**
		 * \brief Getter for the pointer holding the variance Mesh.
		 *
		 */
		const sibr::Mesh::Ptr						varianceMesh() const;
		const sibr::HybridAppArgs					sceneArgs() const;

		sibr::PatchCloud&							get_patch_cloud(void) ;
		sibr::MeshRayTracer&						get_ray_tracer(void) ;
		const fribr::Scene::Ptr						get_fribr_scene(void) const;
		std::vector<PatchCloud::Component::Ptr>&	get_components(void) ;
		sibr::VoxelGridFRIBR&						get_grid(void) ;
		sibr::VoxelGridFRIBR&						get_fine_grid(void);
		std::vector<sibr::Vector3f>&				get_exposures(void);
		
		void										set_fribr_scene(const std::string& path);
		void										set_pcloud_path(const std::string& path);
		const bool&									pCloudType() { return _harmonizedCloud; }
		const int&									get_ref_scale(void) { return _refinementScale; }
		Texture2DRGB::Ptr&							inputSpecularTextures(void);
		~HybridScene() {};

	protected:
		
		sibr::PatchCloud							_patchCloud;
		std::string									_pCloudPath;
		HybridScene::HybridAppArgs					_hybridSceneArgs;
		std::vector<sibr::Vector3f>					_exposureFactors;
		sibr::Mesh::Ptr								_varianceMesh;

		void										populateExternalData();
		void										initializeScene();
		
		fribr::Scene::Ptr							_fribrScene;
		sibr::MeshRayTracer							_rayTracer;
		HybridSceneOpts								_hybridOpts;

		sibr::VoxelGridFRIBR						_grid;
		sibr::VoxelGridFRIBR						_fineGrid;
		int											_refinementScale = 1;
		std::vector<PatchCloud::Component::Ptr>		_components;

		
	private:
		Texture2DRGB::Ptr							_inputSpecTexture;
		std::vector<sibr::Vector3f>					_exposures;
		bool										_harmonizedCloud = false;
		int											_maxCams = 2000;

		bool										readCameraFile(std::string &cameraFileName);
	};

	inline const sibr::Mesh::Ptr			HybridScene::varianceMesh(void) const
	{
		return _varianceMesh;
	}

	inline const sibr::HybridAppArgs		HybridScene::sceneArgs(void) const 
	{
		return _hybridSceneArgs;
	}

	inline sibr::PatchCloud& HybridScene::get_patch_cloud(void) 
	{
		return _patchCloud;
	}

	inline sibr::MeshRayTracer& HybridScene::get_ray_tracer(void) 
	{
		return _rayTracer;
	}

	inline const fribr::Scene::Ptr HybridScene::get_fribr_scene(void) const
	{
		return _fribrScene;
	}

	inline std::vector<PatchCloud::Component::Ptr>& HybridScene::get_components(void) 
	{
		return _components;
	}

	inline sibr::VoxelGridFRIBR& HybridScene::get_grid(void) 
	{
		return _grid;
	}

	inline sibr::VoxelGridFRIBR& HybridScene::get_fine_grid(void)
	{
		return _fineGrid;
	}

	inline std::vector<sibr::Vector3f>& HybridScene::get_exposures(void)
	{
		return _exposures;
	}

	inline void HybridScene::set_fribr_scene(const std::string& path)
	{
		_fribrScene.reset(new fribr::Scene(path));
	}

	inline void HybridScene::set_pcloud_path(const std::string& path)
	{
		_pCloudPath = path;
	}

	inline Texture2DRGB::Ptr& HybridScene::inputSpecularTextures(void)
	{
		return _inputSpecTexture;
	}
		
}