// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#pragma once

#include "Config.hpp"
#include "projects/hybrid_ibr/renderer/HybridScene.hpp"
#include "core/graphics/Texture.hpp"
#include "core/graphics/Window.hpp"

#include <core/system/SimpleTimer.hpp>
#include <Eigen/Core>


namespace sibr {

	class SIBR_EXP_HYBRID_EXPORT VarianceMeshGenerator
	{
		SIBR_DISALLOW_COPY(VarianceMeshGenerator);

	public:
		typedef std::shared_ptr<VarianceMeshGenerator>		Ptr;
		//DeepBlendingScene::DeepBlendingScene();
		VarianceMeshGenerator::VarianceMeshGenerator(sibr::HybridScene::Ptr scene, sibr::Mesh::Ptr originalMesh);
		

		/**
		 * \brief Getter for the pointer holding the variance Mesh.
		 *
		 */
		const sibr::Mesh::Ptr							varianceMesh() const;

		const std::map<int,
			std::map<int,
			sibr::ImageRGB32F::Pixel>>					getVisibility() const;

		/**
		 * \brief Getter for the mesh color array
		 *
		 */
		const std::vector<sibr::Mesh::Colors>			getMeshColor() const;

		std::vector<std::vector<float>>					diffArea() ;

		// Compute visibility of the vertices specified (N=num_vertex_considered) in the number of cameras specified (M=cams_considered
		void											computeVisibility();

		void											computePerVertexVariance(sibr::Mesh::Colors& new_mesh_color, sibr::Mesh::Colors& per_vertex_variance);

		void											printCorrespondence(std::string& outPath);

		~VarianceMeshGenerator() {};


	private:
		sibr::HybridScene::Ptr				_scene;
		sibr::Mesh::Ptr						_originalMesh;
		sibr::Mesh::Ptr						_varianceMesh;
		std::map<int, 
			std::map<int, 
				sibr::ImageRGB32F::Pixel>>	_visibility;

		std::vector<sibr::Mesh::Colors>		_unique_color;

		std::vector<std::vector<float>>		_diffArea;

		uint								_start_cam;
		uint								_numCams;
		uint								_step_size;
		
		int									_num_vertex_considered;
		int									_numVertex;


		// Calculate the differential area of the mesh vertex on each pixel, as computed in [Zhang et. al. 2016]
		// claimed to be analogous to the differential form factor b/n camera pixel and mesh vertex in [Cohen et. al. 1993]
		void								_computeDifferentialArea();


		float weightedMean(std::vector<std::pair<float, float>> radianceSet);

		float weightedMedian(std::vector<std::pair<float, float>> radianceSet);

		static inline double computeSquare(double x) { return x * x; }

		float weightedVariance(std::vector<std::pair<float, float>> radianceSet);

		float simpleVariance(std::vector<std::pair<float, float>> radianceSet);
	};

	inline const sibr::Mesh::Ptr			VarianceMeshGenerator::varianceMesh(void) const
	{
		return _varianceMesh;
	}

	inline const std::map<int, std::map<int, sibr::ImageRGB32F::Pixel>> VarianceMeshGenerator::getVisibility() const
	{
		return _visibility;
	}

	inline const std::vector<sibr::Mesh::Colors>			VarianceMeshGenerator::getMeshColor() const {
		return _unique_color;
	}
}