// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#pragma once

# include "Config.hpp"
# include "projects/hybrid_ibr/renderer/HybridRenderer.hpp"
# include "projects/hybrid_ibr/renderer/HybridScene.hpp"
# include <core/graphics/Mesh.hpp>
# include <core/view/ViewBase.hpp>
# include <core/graphics/Texture.hpp>
# include <core/raycaster/VoxelGrid.hpp>
# include <core/renderer/ColoredMeshRenderer.hpp>
# include <core/renderer/TexturedMeshRenderer.hpp>
# include <core/renderer/PoissonRenderer.hpp>
# include <core/view/SceneDebugView.hpp>


namespace sibr { 

	/**
	 * \class ULRV3View
	 * \brief Wrap a ULR renderer with additional parameters and information.
	 */
	class SIBR_EXP_HYBRID_EXPORT PerViewMeshDebugView : public sibr::ViewBase
	{
		SIBR_CLASS_PTR(PerViewMeshDebugView);

		/// Rendering mode: default, use only one camera, use all cameras but one.
		enum RenderMode { ALL_CAMS, ONE_CAM, LEAVE_ONE_OUT, EVERY_N_CAM };

		/// Blending mode: keep the four best values per pixel, or aggregate them all.
		enum WeightsMode { ULR_W , VARIANCE_BASED_W, ULR_FAST};

	public:

		/**
		 * Constructor
		 * \param ibrScene The scene to use for rendering.
		 * \param render_w rendering width
		 * \param render_h rendering height
		 * \param viewName name of the view
		 */
		PerViewMeshDebugView(const sibr::HybridScene::Ptr& ibrScene, uint render_w, uint render_h, const std::string viewName = "PVMesh Debug View Settings");

		/** Replace the current scene.
		 *\param newScene the new scene to render */
		void setScene(const sibr::HybridScene::Ptr & newScene);

		/**
		 * Perform rendering. Called by the view manager or rendering mode.
		 * \param dst The destination rendertarget.
		 * \param eye The novel viewpoint.
		 */
		void onRenderIBR(sibr::IRenderTarget& dst, const sibr::Camera& eye) override;

		/**
		 * Update inputs (do nothing).
		 * \param input The inputs state.
		 */
		void onUpdate(Input& input) override;

		/**
		 * Update the GUI.
		 */
		void onGUI() override;

		void registerDebugObjects(MultiMeshManager::Ptr debugView, InteractiveCameraHandler::Ptr handler);


		/** \return a reference to the scene */
		//const std::shared_ptr<sibr::BasicIBRScene> & getScene() const { return _scene; }

		void refine();
	protected:

		void refineGUI();
		void time_now();
		void save_pcloud();
		void debugSelectedVoxel();
		void renderVisualisation();
		void initializeVoxelGrid();
		void updateHandler();
		void updateVoxel();
		float simpleVariance(std::vector<float> errors, const float mean);
		Eigen::Vector2f vec2devs(Eigen::Vector3f& inputVec, Eigen::Vector3f& normalVec, Eigen::Vector3f& upVec);


		sibr::MultiMeshManager::Ptr			_debugView;
		sibr::InteractiveCameraHandler::Ptr _handler;
		
		std::string							_viewName;
		//std::shared_ptr<sibr::BasicIBRScene>_scene; ///< The current basic scene.
		std::shared_ptr<sibr::HybridScene>	_hybridScene; ///< The current hybrid scene.

		
		HybridRenderer::Ptr					_hybridRenderer;  /// <Hybrid Renderer 
		ColoredMeshRenderer::Ptr			_coloredMeshRenderer; /// <Colored Mesh renderer
		TexturedMeshRenderer::Ptr			_texturedRenderer; /// <Textured Mesh renderer
		PoissonRenderer::Ptr				_poissonRenderer; ///< The poisson filling renderer.

		bool								_sliceErrMap = false;
		bool								_logSelectedSlice = false;
		std::vector<std::map<float, std::pair<float, int>>>		_result;
		std::vector<sibr::Mesh>				_pvMeshes;	///< Loads and stores the per-view meshes
		std::vector<sibr::Texture2DRGB::Ptr>_inputTextures;
		int									_numMeshes;
		int									_selectedView = 0;
		int									_selectedSlice = 0;
		int									_numSlices = -1;
		int									_numFineVoxels;
		sibr::Vector3i						_selectedFineVoxel;
		sibr::Vector3i						_selectedVoxel;
		std::vector<int>					_sliceIndices;
		int									_meshID = 0;
		int									_voxX = 24;
		int									_voxY = 18;
		int									_voxZ = 5;

		int									_vX = 0;
		int									_vY = 0;
		int									_vZ = 0;

		sibr::VoxelGrid<BasicVoxelType>::Ptr _vGrid;
		sibr::VoxelGrid<BasicVoxelType>::Ptr _vGridFine;
		size_t								_voxel;
	};

} /*namespace sibr*/ 
