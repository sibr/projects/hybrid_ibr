// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#include "HybridScene.hpp"

sibr::HybridScene::HybridScene(HybridAppArgs& myArgs, sibr::HybridSceneOpts myOpts):
 _grid(sibr::Vector3i(32, 32, 32)),
	_fineGrid(sibr::Vector3i(32, 32, 32)),
 _hybridSceneArgs(myArgs)
{
	_hybridOpts = myOpts;
	_currentOpts = myOpts;
	// Check the scene type
	std::string cameraFile = myArgs.dataset_path.get() + "/camerafile.cam";
	// If Zhang 2016 scene,
	if (sibr::fileExists(cameraFile)) {
		// populate _data accordingly
		populateExternalData();
	}
	else {
		// else use default ParseData::getParsedData() function to populate data
		_data->getParsedData(myArgs);
	}


	// Create scene from custom data
	createFromData(myArgs.texture_width);

	initializeScene();
}

sibr::HybridScene::HybridScene(const BasicIBRScene::Ptr& scene, HybridAppArgs& myArgs) : BasicIBRScene(*scene),
_grid(sibr::Vector3i(32, 32, 32)),
_fineGrid(sibr::Vector3i(32, 32, 32)),
_hybridSceneArgs(myArgs)
{
	initializeScene();
}

void sibr::HybridScene::initializeScene()
{
	_harmonizedCloud = _hybridOpts.harmonizedPatchCloud;
	std::string texImgPath, meshPath, specMaskTexturePath;
	sibr::ImageRGB inputTextureImg, specMaskTexImg;;
		texImgPath = _hybridSceneArgs.texturePath.get();
		SIBR_LOG << "Checking texture file " << texImgPath << " exists!!" << std::endl;

		if (sibr::fileExists(texImgPath)) {
			SIBR_LOG << "Using texture file " << texImgPath << "!!" << std::endl;
			inputTextureImg.load(texImgPath);
			_inputMeshTexture.reset(new sibr::Texture2DRGB(inputTextureImg, SIBR_GPU_LINEAR_SAMPLING));
		}
		else if (_inputMeshTexture == NULL) {
			texImgPath = removeExtension(_data->meshPath()) + "_u1_v1.png";
			SIBR_LOG << "Checking texture file " << texImgPath << " exists!!" << std::endl;
			if (sibr::fileExists(texImgPath)) {
				SIBR_LOG << "Using texture file " << texImgPath << "!!" << std::endl;
				inputTextureImg.load(texImgPath);
				_inputMeshTexture.reset(new sibr::Texture2DRGB(inputTextureImg, SIBR_GPU_LINEAR_SAMPLING));
			}
			else if (sibr::fileExists(parentDirectory(_data->meshPath()) + "/textured_u1_v1.png")) {
				SIBR_LOG << "Using texture file " << parentDirectory(_data->meshPath()) + "/textured_u1_v1.png" << "!!" << std::endl;
				inputTextureImg.load(parentDirectory(_data->meshPath()) + "/textured_u1_v1.png");
				_inputMeshTexture.reset(new sibr::Texture2DRGB(inputTextureImg, SIBR_GPU_LINEAR_SAMPLING));
			}
			else if (sibr::fileExists(parentDirectory(_data->meshPath()) + "/texture.png")) {
				SIBR_LOG << "Using texture file " << parentDirectory(_data->meshPath()) + "/texture.png" << "!!" << std::endl;
				inputTextureImg.load(parentDirectory(_data->meshPath()) + "/texture.png");
				_inputMeshTexture.reset(new sibr::Texture2DRGB(inputTextureImg, SIBR_GPU_LINEAR_SAMPLING));
			}
			else {
				SIBR_ERR << "No textured mesh file found!!" << std::endl;
			}

		}
	
	specMaskTexturePath = _hybridSceneArgs.dataset_path.get() + "/" + _hybridSceneArgs.harmonizedPath.get() + "/specTexture_u1_v1.png";
	if (fileExists(specMaskTexturePath)) {
		specMaskTexImg.load(specMaskTexturePath);
		_inputSpecTexture.reset(new sibr::Texture2DRGB(specMaskTexImg, SIBR_GPU_LINEAR_SAMPLING));
		SIBR_LOG << "Specular mask textures loaded from " << specMaskTexturePath << std::endl;
	}
	else {
		SIBR_WRG << "No view-dependent mask file found at " << specMaskTexturePath << std::endl;
	}

	// Load the mesh showing color variance per-vertex, if available
	std::string varMeshPath = _hybridSceneArgs.dataset_path.get() + "/" + _hybridSceneArgs.harmonizedPath.get() + "/mesh_perVertexVariance.ply";
	if (sibr::fileExists(varMeshPath)) {
		_varianceMesh.reset(new sibr::Mesh);
		_varianceMesh->load(varMeshPath);

		if(!_varianceMesh->hasNormals())
		{
			SIBR_WRG << "generating normals for the proxy (no normals found in the proxy file)" << std::endl;
			_varianceMesh->generateNormals();
		}
	}

	if (_hybridSceneArgs.noPC) {
		_hybridOpts.patchClouds = false;
	}

	if (_hybridOpts.patchClouds) {
		
		// Read the patch cloud data structure
		_pCloudPath = _hybridSceneArgs.pcloudPath;
		std::string pcloud_path = _hybridSceneArgs.dataset_path.get() + "/hybrid/" + _pCloudPath;
		if (boost::filesystem::exists(pcloud_path)) {
			SIBR_LOG << "Loading patch cloud!!!" << std::endl;
			
			if (!_patchCloud.load(pcloud_path, _components, _grid, _fineGrid, _refinementScale)){
					SIBR_ERR << "Patch cloud could not be loaded successfully from " << pcloud_path << std::endl;
			}
			SIBR_LOG << "Patch cloud loaded successfully from location " << pcloud_path << " with refinement scale " << _refinementScale << std::endl;
		}
		else {			
			SIBR_ERR << "Patch cloud could not be loaded successfully from " << pcloud_path << std::endl;
		}
	}
}


bool sibr::HybridScene::readCameraFile(std::string& cameraFileName)
{
	std::ifstream cameraFile(cameraFileName);
	if (!cameraFile.is_open()) {
		SIBR_ERR << "Unable to load camerafile.cam!! " << std::endl;
	}
	std::string line;

	int numFrames = 0;
	int width, height;
	float fovy, gamma;
	std::vector<ImageListFile::Infos> imgInfos;
	std::vector<InputCamera::Ptr> cams;
	int camId = 0;

	while (std::getline(cameraFile, line)) {
		if (line.empty() || line.compare("CAMFILE_HEADER") == 0 || line.compare("end_header") == 0) {
			continue;
		}


		std::vector<std::string> tokens = sibr::split(line, ' ');

		if (tokens[0].compare("FrameCount") == 0) {
			numFrames = std::stoi(tokens[1]) / 3;
			continue;
		}

		if (tokens[0].compare("Width") == 0) {
			width = std::stoi(tokens[1]);
			continue;
		}

		if (tokens[0].compare("Height") == 0) {
			height = std::stoi(tokens[1]);
			continue;
		}
		if (tokens[0].compare("Vfov") == 0) {
			fovy = SIBR_DEGTORAD(std::stof(tokens[1]));
			continue;
		}

		if (tokens[0].compare("Gamma") == 0) {
			gamma = std::stof(tokens[1]);
			continue;
		}

		if ( camId < _maxCams) {
			InputCamera::Ptr cam;
			ImageListFile::Infos infos;
			// Read image infos
			infos.camId = camId;
			infos.filename = tokens[0];
			infos.width = width;
			infos.height = height;

			// Read Camera intrinsic and extrinsic params
			Vector3f Eye(std::stof(tokens[1]), std::stof(tokens[2]), std::stof(tokens[3]));
			Vector3f Up(std::stof(tokens[4]), std::stof(tokens[5]), std::stof(tokens[6]));
			Vector3f At(std::stof(tokens[7]), std::stof(tokens[8]), std::stof(tokens[9]));
			Vector3f Exposure(std::stof(tokens[10]), std::stof(tokens[11]), std::stof(tokens[12]));

			_exposures.push_back(Exposure);
			imgInfos.push_back(infos);
			cam.reset(new InputCamera(fovy, 0.0, 0.0, infos.width, infos.height, infos.camId));
			cam->fovy(fovy);
			cam->setLookAt(Eye, Eye + At, Up);
			cam->name(infos.filename);
			cam->znear(0.1f);
			cam->zfar(100.0f);
			cams.push_back(cam);
		}
		camId++;
		std::getline(cameraFile, line);
		std::getline(cameraFile, line);
	}


	std::vector<bool> activeCams, excludeCams;
	std::vector<InputCamera::Z> clipPlanes;

	// Setup active/exclude cams
	if (activeCams.empty()) {
		if (excludeCams.empty()) {

			activeCams.resize(imgInfos.size());
			excludeCams.resize(imgInfos.size());

			for (int i = 0; i < imgInfos.size(); i++) {
				activeCams[i] = true;
				excludeCams[i] = false;
			}
		}
		else {
			activeCams.resize(imgInfos.size());
			for (int i = 0; i < imgInfos.size(); i++)
				activeCams[i] = !excludeCams[i];
		}
	}

	// Setup clipping planes
	if (clipPlanes.empty()) {
		clipPlanes.resize(numFrames);
		for (int i = 0; i < numFrames; i++) {
			clipPlanes[i].near = 0.1f;
			clipPlanes[i].far = 100.0f;
		}
	}
	if (numFrames > _maxCams) {
		numFrames = _maxCams;
	}

	_data->numCameras(numFrames);
	_data->imgInfos(imgInfos);
	_data->activeImages(activeCams);
	_data->cameras(cams);
	return true;
}


void sibr::HybridScene::populateExternalData()
{
	// set the base path name
	std::string basePath = _hybridSceneArgs.dataset_path;
	_data->basePathName(basePath);
	_data->imgPath(basePath);
	// set the mesh path
	std::string meshPath = basePath + "/recon.obj";
	if (!fileExists(meshPath)) {
		meshPath = basePath + "/poisson.ply";
	}

	_data->meshPath(meshPath);

	// read the intrinsic and extrinsic camera parameters
	if (!readCameraFile(basePath + "/camerafile.cam")) {
		SIBR_ERR << "Camerafile.cam text file does not exist at " + basePath << std::endl;
	}

}
