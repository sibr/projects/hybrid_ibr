// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#include "VarianceMeshGenerator.hpp"

namespace sibr {
	sibr::VarianceMeshGenerator::VarianceMeshGenerator(sibr::HybridScene::Ptr scene, sibr::Mesh::Ptr originalMesh) :
		_scene(scene),
		_originalMesh(originalMesh)
	{
		_start_cam = scene->sceneArgs().start_cam;
		int cams_specified = scene->sceneArgs().cams_spec;
		_numCams = (cams_specified > 0 && cams_specified <= scene->cameras()->inputCameras().size()) ? cams_specified : scene->cameras()->inputCameras().size();

		_numVertex = _originalMesh->vertices().size();
		int verts_specified = scene->sceneArgs().vert_spec;
		_num_vertex_considered = verts_specified >= 100 ? (verts_specified <= _numVertex ? verts_specified : _numVertex) : _numVertex / verts_specified;
		_step_size = _numVertex / _num_vertex_considered;

	}

	void sibr::VarianceMeshGenerator::computeVisibility()
	{

		// Assign random colors to each camera
		SIBR_LOG << "[ColorHarmonize] Assign random colors to each camera" << std::endl;
		std::vector<sibr::Vector3f> colors;
		colors.resize(_numCams);

		for (int cam_id = _start_cam; cam_id < _start_cam + _numCams; cam_id++) {
			float r1 = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX));
			float r2 = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX));
			float r3 = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX));

			colors[cam_id] = Vector3f(r1, r2, r3);
		}

		SIBR_LOG << "[ColorHarmonize] Compute the visibility of the vertices specified:(N=" << _num_vertex_considered << ") in the number of cameras specified  (M=" << _numCams << ")" << std::endl;
		sibr::Raycaster raycaster;
		raycaster.init();
		raycaster.addMesh(*_originalMesh);

		_unique_color.resize(_numCams);

#pragma omp parallel for
		for (int cam_id = _start_cam; cam_id < _start_cam + _numCams; cam_id++) {

			_unique_color[cam_id].resize(_numVertex);
			sibr::InputCamera cam = *_scene->cameras()->inputCameras()[cam_id];
			if (cam.isActive()) {
				sibr::RaycastingCamera::Ptr rayCastCam(new sibr::RaycastingCamera(cam));
				sibr::Vector3f cam_center = cam.position();
				int num_hits = 0;
				std::map<int, sibr::ImageRGB32F::Pixel> isVisible;
				for (int vert_id = 0; vert_id < _num_vertex_considered; vert_id++) {
					// [SP] \todo Can be optimized to use "specially selected" vertex only
					sibr::Vector3f vert_pos = _originalMesh->vertices()[vert_id * _step_size];
					if (cam.frustumTest(vert_pos)) {

						sibr::Vector3f ray_dir = vert_pos - cam_center;
						sibr::RayHit hit = raycaster.intersect(sibr::Ray(cam_center, ray_dir));
						if (!hit.hitSomething()) { continue; }
						//unique_color[vert_id] = color;							// [DEBUG] if the vertex is inside view frustum of the camera and hit anything, color it red

						float hit_dist = hit.dist();
						float ray_dist = ray_dir.norm();
						float eps = 0.000001;

						if (abs(hit_dist - ray_dist) <= eps) {
							_unique_color[cam_id][vert_id * _step_size] = colors[cam_id];							// [DEBUG] if the vertex is inside view frustum of the camera and hit only the vertex in question, color it 
							isVisible[vert_id * _step_size] = Vector3f(0.0f, 0.0f, 0.0f);
						}
					}
				}
				_visibility[cam_id] = isVisible;
			}
		}


	}

	void VarianceMeshGenerator::computePerVertexVariance(sibr::Mesh::Colors& new_mesh_color, sibr::Mesh::Colors& per_vertex_variance)
	{

		_computeDifferentialArea();
		// Compute the final radiance and the weights per vertex
		// Takes ~0.2 sec. for 68311 vertex [recon_half_sofa.obj]
		// Takes ~1.5 sec. for full mesh [recon.obj]
		SIBR_LOG << "[ColorHarmonize] Compute the final radiance and the weights per vertex " << std::endl;
		std::vector<std::vector<float>> weights, radiances;

		std::vector<std::vector<std::map<sibr::Vector3f, sibr::Vector3f>>> weightedRads;
		weightedRads.resize(_numVertex);
		new_mesh_color.resize(_numVertex);
		per_vertex_variance.resize(_numVertex);

		sibr::Timer profile;
		profile.tic();

#pragma omp parallel for
		for (int vertex_id = 0; vertex_id < _numVertex; vertex_id++) {
			if (vertex_id % 1000 == 0) { std::cout << "." << std::flush; }

			const sibr::Vector3f& vert_pos = _originalMesh->vertices()[vertex_id];

			std::vector<std::pair<float, float>> weightedRadianceR, weightedRadianceG, weightedRadianceB;

			for (int cam_id = _start_cam; cam_id < _start_cam + _numCams; cam_id++) {
				const sibr::InputCamera& cam = *_scene->cameras()->inputCameras()[cam_id];
				if (cam.isActive()) {
					sibr::ImageRGB::Ptr img = _scene->images()->inputImages()[cam_id];

					float expR = 1.0f;
					float expG = 1.0f;
					float expB = 1.0f;
					const float difW = _diffArea[cam_id][vertex_id];

					const std::map<int, sibr::ImageRGB32F::Pixel>& visible_verts = _visibility[cam_id];

					if (visible_verts.find(vertex_id) != visible_verts.end()) {

						//SIBR_LOG << "[ColorHarmonize] Vertex id: " << vertex_id << std::endl;
						const Vector3f& pixel_coord = cam.projectImgSpaceInvertY(_originalMesh->vertices()[vertex_id]);
						const sibr::ColorRGBA& pix_color = img->color(pixel_coord.x(), pixel_coord.y());

						sibr::Vector3f final_Rad = Vector3f(pix_color.x() / expR, pix_color.y() / expG, pix_color.z() / expB);
					
						//final_weight = difW * confidence(pix_color);
						//final_weight = confidence(pix_color);
						//std::cout << final_Rad << std::endl;
						//sibr::Vector3f final_weight = Vector3f(1.0f, 1.0f, 1.0f);

						sibr::Vector3f final_weight = difW * Vector3f(1.0f, 1.0f, 1.0f);
						weightedRadianceR.push_back(std::make_pair(final_weight.x(), final_Rad.x()));
						weightedRadianceG.push_back(std::make_pair(final_weight.y(), final_Rad.y()));
						weightedRadianceB.push_back(std::make_pair(final_weight.z(), final_Rad.z()));

					}

				}
			}

			//SIBR_LOG << "[ColorHarmonize] Weighted Radiance : " << weightedMedian(weightedRadianceR) << ", " << weightedMedian(weightedRadianceG) << ", " << weightedMedian(weightedRadianceB) << std::endl;
			const int numRadSamples = weightedRadianceB.size();

			const float r = weightedMean(weightedRadianceR);// / numRadSamples;
			const float g = weightedMean(weightedRadianceG);// / numRadSamples;
			const float b = weightedMean(weightedRadianceB);// / numRadSamples;


			//sibr::Vector3f new_color((r <= 1.0f) ? r : 1.0f, (g <= 1.0f) ? g : 1.0f, (b <= 1.0f) ? b : 1.0f);
			//SIBR_LOG << "[ColorHarmonize] Vertex colors: " << new_color << std::endl;
			sibr::Vector3f new_color(r, g, b);

			//new_color = (numRadSamples > 0) ? new_color / numRadSamples : new_color;
			new_mesh_color[vertex_id] = new_color;

			const float r_var = weightedVariance(weightedRadianceR);
			const float g_var = weightedVariance(weightedRadianceG);
			const float b_var = weightedVariance(weightedRadianceB);

			sibr::Vector3f pv_var(r_var, g_var, b_var);

			//pv_var = (numRadSamples > 0) ? pv_var / numRadSamples : pv_var;
			per_vertex_variance[vertex_id] = pv_var;
		}

		SIBR_LOG << "\n[Time taken] to compute per-vertex variance: " << profile.deltaTimeFromLastTic() << " ms." << std::endl;
	}

	void VarianceMeshGenerator::printCorrespondence(std::string& outPath)
	{
		std::ofstream output_correspondence;

		std::vector<std::vector<std::vector<float>>> vertex_corres;
		vertex_corres.resize(_num_vertex_considered);

		output_correspondence.open(outPath + "/correspondence_" + std::to_string(_num_vertex_considered) + ".txt");
		for (int cam_id = _start_cam; cam_id < _start_cam + _numCams; cam_id++) {
			sibr::InputCamera cam = *_scene->cameras()->inputCameras()[cam_id];
			if (cam.isActive()) {
				sibr::ImageRGB    image = _scene->images()->image(cam_id).clone();
				sibr::ImageRGB32F linImage;
				std::ostringstream ssZeroPad;
				ssZeroPad << std::setw(8) << std::setfill('0') << cam_id;
				const std::string fName = outPath + "/" + ssZeroPad.str() + "." + getExtension(cam.name());
				image.save(fName);
				image.toOpenCV().convertTo(linImage.toOpenCVnonConst(), CV_32FC3, (1.0f / 255.0f));
				sRGB2Lin(linImage);

				for (auto& it : _visibility[cam_id]) {
					std::vector<float> data;
					int vertex_id = it.first;
					Vector3f pixel_coord = cam.projectImgSpaceInvertY(_originalMesh->vertices()[vertex_id]);
					it.second = linImage(int(pixel_coord.x()), int(pixel_coord.y()));
					_unique_color[cam_id][vertex_id] = it.second;
					
					//Vector3f pix_color = Vector3f(it.second.x(), it.second.y(), it.second.z());
					//const float magnitude = pix_color.norm();
					//std::cout << magnitude << std::endl;
					data.push_back(cam_id);
					data.push_back(it.second.x());
					data.push_back(it.second.y());
					data.push_back(it.second.z());
					data.push_back(pixel_coord.x());
					data.push_back(pixel_coord.y());
					vertex_corres[vertex_id / _step_size].push_back(data);
					/*output_correspondence << cam_id << "," << vertex_id / _step_size << "," << 
						it.second.x() << "," <<	it.second.y() << "," << it.second.z() << "," << 
						int(pixel_coord.x()) << "," << int(pixel_coord.y()) << "," <<
						vert_col.x() << "," << vert_col.y() << "," << vert_col.z() << std::endl;*/
				}
			}
			else {
				sibr::ImageRGB    image(cam.w(), cam.h());
				std::ostringstream ssZeroPad;
				ssZeroPad << std::setw(8) << std::setfill('0') << cam_id;
				const std::string fName = outPath + "/" + ssZeroPad.str() + "." + getExtension(cam.name());
				image.save(fName);
			}

		}
		output_correspondence << "ImageID" << "," << "X_COORD" << "," << "Y_COORD" << "," << "VERT_RED" << "," << "VERT_GREEN" << "," << "VERT_BLUE" << std::endl;

		for (int verts = 0; verts < _num_vertex_considered; verts++) {
			const sibr::Vector3f vert_col = _originalMesh->colors()[verts * _step_size];
			for (int cams = 0; cams < vertex_corres[verts].size(); cams++) {
				output_correspondence <<int(vertex_corres[verts][cams][0]) << "," <<
					int(vertex_corres[verts][cams][4]) << "," << int(vertex_corres[verts][cams][5]) << "," <<
					vert_col.x() << "," << vert_col.y() << "," << vert_col.z() << std::endl;
			}
		}


		output_correspondence.close();
	}

	void sibr::VarianceMeshGenerator::_computeDifferentialArea()
	{
		_diffArea.resize(_numCams);
		SIBR_LOG << "[ColorHarmonize] Calculate the differential area of the mesh vertex on each pixel" << std::endl;

#pragma omp parallel for
		for (int cam_id = _start_cam; cam_id < _start_cam + _numCams; cam_id++) {
			std::vector<float> g;
			g.resize(_numVertex);
			sibr::InputCamera cam = *_scene->cameras()->inputCameras()[cam_id];
			if (cam.isActive()) {
				sibr::Vector3f cam_center = cam.position();

				for (int vert_id = 0; vert_id < _numVertex; vert_id++) {
					sibr::Vector3f vert_pos = _originalMesh->vertices()[vert_id];
					sibr::Vector3f ray_dir = vert_pos - cam_center;
					sibr::Vector3f norm_ray_dir = ray_dir.normalized();

					sibr::Vector3f optical_axis = cam.dir();
					sibr::Vector3f vert_normal;
					if (_originalMesh->hasNormals()) {
						vert_normal = _originalMesh->normals()[vert_id];
						//g[vert_id] = (-norm_ray_dir.dot(vert_normal))*(norm_ray_dir.dot(optical_axis)) / ray_dir.squaredNorm();
						g[vert_id] = std::max((-norm_ray_dir.dot(vert_normal)) / ray_dir.squaredNorm(), 0.0f);
					}
					else {
						g[vert_id] = 1.0f;
					}
				}
				_diffArea[cam_id] = g;
			}
		}
	}

	float VarianceMeshGenerator::weightedMean(std::vector<std::pair<float, float>> radianceSet)
	{
		if (radianceSet.size() == 0) {
			return 0.0f;
		}

		float cumRadiance = std::accumulate(radianceSet.begin(), radianceSet.end(), 0.0f,
			[](float previous, auto& element) { return previous + element.first * element.second; });

		float cumWeight = std::accumulate(radianceSet.begin(), radianceSet.end(), 0.0f,
			[](float previous, auto& element) { return previous + element.first; });


		return cumRadiance / cumWeight;
	}

	// Calculate weighted median given an ordered map of weights (float) as the keys and data (float) as the values.
	// Inspired from https://stackoverflow.com/questions/9794558/weighted-median-computation
	float VarianceMeshGenerator::weightedMedian(std::vector<std::pair<float, float>> radianceSet)
	{
		if (radianceSet.size() == 0) {
			return 0.0f;
		}

		const size_t numElements = radianceSet.size();
		//SIBR_LOG << "[ColorHarmonize] Printing elements: weights, radiance " << std::endl;
		/*for (auto& it : radianceSet) {
			std::cout << it.first << " , " << it.second << std::endl;
		}*/

		float cumWeight = std::accumulate(radianceSet.begin(), radianceSet.end(), 0.0f,
			[](float previous, auto& element) { return previous + element.first; });

		sort(radianceSet.begin(), radianceSet.end());
		//std::cout << "Cumulative weight: " << cumWeight << std::endl;

		// sum is the total weight of all `x[i] > x[k]`
		float sum = cumWeight - radianceSet.begin()->first;
		float prevElement;
		for (auto& it : radianceSet) {
			//std::cout << "Sum: " << sum << std::endl;
			if (sum > (cumWeight / 2.0f)) {
				//std::cout << "Radiance weight: " << 1.0f*it.first << std::endl;
				sum -= it.first * 1.0f;
			}
			else {
				//std::cout << "Median Radiance: " << 1.0f*it.second << std::endl;
				return it.second * 1.0f;
			}
			prevElement = it.second * 1.0f;
		}
		//std::cout << "Median Radiance: " << prevElement << std::endl;
		return prevElement;
	}

	float VarianceMeshGenerator::weightedVariance(std::vector<std::pair<float, float>> radianceSet)
	{
		if (radianceSet.size() == 0) {
			return 0.0f;
		}

		const size_t numElements = radianceSet.size();
		//SIBR_LOG << "[ColorHarmonize] Printing elements: weights, radiance " << std::endl;
		/*for (auto& it : radianceSet) {
			std::cout << it.first << " , " << it.second << std::endl;
		}*/

		std::vector<float> weights, data;

		for (auto& it : radianceSet) {
			//std::cout << it.first << " , " << it.second << std::endl;
			weights.push_back(it.first);
			data.push_back(it.second);
		}

		double sumWeights = std::accumulate(weights.begin(), weights.end(), 0.0);
		double sumData = std::accumulate(radianceSet.begin(), radianceSet.end(), 0.0f,
			[](float previous, auto& element) { return previous + (element.first * element.second); });
		double weightedMean = sumData / sumWeights;

		std::vector<double> diff(data.size());
		std::transform(data.begin(), data.end(), diff.begin(),
			std::bind2nd(std::minus<double>(), weightedMean));

		std::transform(diff.begin(), diff.end(), diff.begin(), computeSquare);

		int nonZeroSWeightsCount = 0;
		for (int i = 0; i < diff.size(); i++) {
			diff[i] = weights[i] * diff[i];
			if (weights[i] != 0) {
				nonZeroSWeightsCount++;
			}
		}

		double sq_sum = std::accumulate(diff.begin(), diff.end(), 0.0);
		//std::cout << sq_sum << std::endl;
		//const float weightedVariance = std::sqrt(sq_sum / (((nonZeroSWeightsCount - 1)/ nonZeroSWeightsCount) * numElements));
		const float weightedVariance = std::sqrt(sq_sum / sumWeights);
		//std::cout << variance << std::endl;

		return weightedVariance;
	}

	float VarianceMeshGenerator::simpleVariance(std::vector<std::pair<float, float>> radianceSet)
	{
		if (radianceSet.size() == 0) {
			return 0.0f;
		}
		const size_t numElements = radianceSet.size();

		std::vector<float> v;
		for (auto& it : radianceSet) {
			//std::cout << it.first << " , " << it.second << std::endl;
			v.push_back(it.second);
		}

		double sum = std::accumulate(v.begin(), v.end(), 0.0);
		double mean = sum / v.size();

		std::vector<double> diff(v.size());
		std::transform(v.begin(), v.end(), diff.begin(),
			std::bind2nd(std::minus<double>(), mean));
		double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);

		const float variance = std::sqrt(sq_sum / numElements);
		//std::cout << variance << std::endl;

		return variance;
	
	}

	std::vector<std::vector<float>>			VarianceMeshGenerator::diffArea()
	{
		_computeDifferentialArea();
		return _diffArea;
	}
}
