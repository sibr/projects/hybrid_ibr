// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#pragma once

# include "Config.hpp"

# include "projects/hybrid_ibr/renderer/patch_cloud.h"
# include <core/renderer/RenderMaskHolder.hpp>
# include <core/scene/BasicIBRScene.hpp>

# include <core/graphics/GPUQuery.hpp>
# include <core/graphics/Texture.hpp>
# include <core/graphics/Shader.hpp>
# include <core/graphics/Mesh.hpp>
# include <core/system/SimpleTimer.hpp>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>

#ifndef NOMINMAX
#define NOMINMAX
#endif
# include <Windows.h>
# include <WinBase.h>


namespace sibr { 
	
	/**
	 * \class HybridRenderer
	 * \brief Perform per-pixel Hybrid Rendering of Textured Mesh, ULR etc.. No selection is done on the CPU side.
	 * Relies on texture arrays and uniform buffer objects to support a high number of cameras. 
	 */
	class SIBR_EXP_HYBRID_EXPORT HybridRenderer
	{
		SIBR_CLASS_PTR(HybridRenderer);
	
		// Vertex shader attribute locations
		static const int					VERTEX_LOCATION = 0;
		static const int					TEXCOORD_LOCATION = 1;
		static const int					NUM_LAYERS = 12;
		static const int					VOXEL_MEMORY_SIZE = 12;

	public:

		/**
		 * Constructor.
		 * \param cameras The input cameras to use.
		 * \param w The width of the internal rendertargets.
		 * \param h The height of the internal rendertargets.
		 * \param fShader An optional name of the fragment shader to use (default to hybrid_composite).
		 * \param vShader An optional name of the vertex shader to use (default to hybrid_composite).
		 * \param facecull Should the mesh be renderer with backface culling.
		 */
		HybridRenderer(const std::vector<sibr::InputCamera::Ptr>& cameras,
			const uint w, const uint h, 
			const std::string & fShader = "hybrid/hybrid_composite", 
			const std::string & vShader = "hybrid/hybrid_composite", 
			const bool facecull = true
		);

		/**
		 * Constructor.
		 * \param cameras The input cameras to use.
		 * \param w The width of the internal rendertargets.
		 * \param h The height of the internal rendertargets.
		 * \param fShader An optional name of the fragment shader to use (default to hybrid_composite).
		 * \param vShader An optional name of the vertex shader to use (default to hybrid_composite).
		 * \param facecull Should the mesh be renderer with backface culling.
		 */
		HybridRenderer(const std::vector<sibr::InputCamera::Ptr>& cameras,
			const std::vector<PatchCloud::Component::Ptr>& components,
			const sibr::VoxelGridFRIBR& grid, const sibr::VoxelGridFRIBR& fineGrid,
			const uint w, const uint h,
			const std::string& fShader = "hybrid/hybrid_composite",
			const std::string& vShader = "hybrid/hybrid_composite",
			const bool facecull = true
		);


		/**
		 * Change the shaders used by the renderer.
		 * \param fShader The name of the fragment shader to use.
		 * \param vShader The name of the vertex shader to use.
		 */
		virtual void setupShaders(
			const std::string & fShader = "hybrid/hybrid_composite",
			const std::string & vShader = "hybrid/hybrid_composite"
		);

		/**
		 * Performs Hybrid rendering to a given destination rendertarget.
		 * \param mesh The mesh to use as geometric proxy.
		 * \param eye The novel viewpoint.
		 * \param dst The destination rendertarget.
		 * \param textureID The handle to the texture to use
		 * \param inputRGBs A texture array containing the input RGB images.
		 * \param inputDepths A texture array containing the input depth maps.
		 * \param passthroughDepth If true, depth from the position map will be output to the depth buffer for ulterior passes.
		 */
		virtual void process(
			const sibr::Mesh& mesh,
			const sibr::Camera& eye,
			IRenderTarget& dst,
			const uint textureID, const uint specTextureID,
			const sibr::Texture2DArrayRGB::Ptr& inputRGBs,
			const sibr::Texture2DArrayLum32F::Ptr& inputDepths,
			bool passthroughDepth = false
		);


		virtual void process(const sibr::Mesh& mesh, const sibr::Camera& eye,
			IRenderTarget& dst,
			IRenderTarget& tex1,
			sibr::Texture2DRGBA::Ptr tex2);

		/** 
		 *  Update which cameras should be used for rendering, based on the indices passed.
		 *  \param camIds The indices to enable.
		 **/
		void updateCameras(const std::vector<uint> & camIds);

		/// Set the photometric variance mask threshold.
		float& varMaskThresh() { return _threshold.get(); }

		/// Set the geometric variance mask threshold.
		float& geoMaskThresh() { return _threshold_geo.get(); }

		/// Set the deviation threshold.
		float& devThresh() { return _threshold_dev; }

		/// Set the slice error variance threshold.
		float& sliceVarThresh() { return _threshold_var; }

		/// Set the filter mask threshold.
		float& filterMaskThresh() { return _filterThreshold.get(); }

		/// Set the alpha for blending.
		float& alphaThresh() { return _alpha.get(); }

		/// Set the epsilon occlusion threshold.
		float & epsilonOcclusion() { return _epsilonOcclusion.get(); }

		/// Flip the RGB images before using them.
		bool & renderMode() { return _renderMode.get(); }

		/// Enable or diable occlusion testing.
		bool& occTest() { return _occTest.get(); }

		/// Enable or diable variance mask.
		bool& toggleMask() { return _toggleMask.get(); }

		/// Enable or diable bilateral filtering.
		bool& bilateralFilter() { return _useBFilter; }

		/// Show debug weights.
		bool & showWeights() { return _showWeights.get(); }

		/// Set winner takes all weights strategy
		bool & winnerTakesAll() { return _winnerTakesAll.get(); }

		/// Apply gamma correction to the output.
		bool & gammaCorrection() { return _gammaCorrection.get(); }

		/// Apply backface culling to the mesh.
		bool & backfaceCull() { return _backFaceCulling; }

		/// Get the visible Imagebins
		std::vector<PatchCloud::ImageBin>& getBins() { return _bins; }
		
		/// Get num layers
		int getLayers() { return NUM_LAYERS; }

		/// Set voxel to debug
		void setVoxel(size_t voxel) { _selectedVoxel = voxel; _selectedVoxels.push_back(_selectedVoxel); }

		void resetVoxel(size_t voxel) { _selectedVoxel = -1; _selectedVoxels.clear(); }

		/// Set sub voxel to debug
		void setSubVoxel(size_t voxel) { _selectedFineVoxel = voxel; }

		void debugPrint() { _debugPrint = true; }

		/// Get visible voxels array
		std::vector<bool>& getVoxels() { return _fat_grid; }

		/// Clear visible voxels before rendering
		void clearVisibleVoxels() { _fat_grid.clear(); }

		void setDisplayMode(PatchCloud::DisplayMode mode) { _displayMode = mode; }

		void setDisplayLayer(int i) { _displayLayer = i; }

		void setDebugPath(const std::string path) { _debugPath = path; }

		int& displayDepth() { return _displayDepth; }

		void setSlice(int slc) { _sliceIdx = slc; }

		void setSliceErrMapSize(int sz) { _sliceSize = sz; resetSliceImage(); }

		int& displaySlice() { return _sliceIdx; }

		int& displayLayer() { return _displayLayer.get(); }

		int& numIter() { return _numIter.get(); }

		sibr::ImageRGB32F::Ptr& getSliceErrMap() { return _sliceErrImg; }

		int& sliceDisp() { return _sliceView.get(); }

		int& maskKernelSize() { return _maskKernelSize.get(); }

		int& kernelSize() { return _kernelSize.get(); }

		float& weightsExp() { return _weightExps.get(); }

		int& blendIdx() { return _blendIndex.get(); }

		int& depthIdx() { return _depthIndex.get(); }
		
		float& compositeMargin() { return _compositingMargin.get(); }
		
		float& tolerance() { return _tolerance.get(); }

		float& depthThreshold() { return _ibrDepthAlpha.get(); }

		bool& gpuSort() { return _gpuSorting.get(); }

		bool& varSigma() { return _variableSigma.get(); }

		bool& varMeasure() { return _useGeometric; }

		bool& rejectSlice() { return _sliceRejection; }

		bool& neighborhood() { return _neighborhood.get(); }

		bool& useIOMulti() { return _useIOMultiPass.get(); }

		bool& updateVoxelMemory() { return _update_voxel_memory; }
		
		bool& profile() { return _profiling; }

		bool& showSlices() { return _showSlices; }


		//sibr::BilateralFilterRenderer::Ptr& biFilter() { return _bFilter; }

		/** Resize the internal rendertargets.
		 *\param w the new width
		 *\param h the new height
		 **/
		void resize(const unsigned int w, const unsigned int h);

		/// Should the final RT be cleared or not.
		bool & clearDst() { return _clearDst; }

		/// \return The ID of the first pass position map texture.
		uint depthHandle() const { return _depthRT->texture(); }

		void update_voxel_memory(bool b);
		void clear_voxel_memory();

		void cluster_k(int k);

		void cluster_subk(int k);

		void cluster_threshold(int k);
		
		virtual void renderAll(
			const sibr::Camera& eye,
			const sibr::Mesh& mesh,
			const uint textureID, const uint specTextureID,
			const sibr::Texture2DArrayRGB::Ptr& inputRGBs,
			bool passthroughDepth = false
		);

		/**
		 * Render the world positions of the proxy points in an intermediate rendertarget.
		 * \param mesh the proxy mesh.
		 * \param eye The novel viewpoint.
		 */
		void renderProxyDepth(const sibr::Mesh & mesh, const sibr::Camera& eye);

		/**
		* Perform Compositing.
		* \param eye The novel viewpoint.
		* \param dst The destination rendertarget.
		* \param inputRGBs The texture array containing the input RGB images.
		* \param inputDepths A texture array containing the input depth maps.
		* \param passthroughDepth If true, depth from the position map will be output to the depth buffer for ulterior passes.
		*/
		void renderBlending(
			const sibr::Camera& eye,
			IRenderTarget& dst,
			const sibr::Texture2DArrayRGB::Ptr& inputRGBs,
			const sibr::Texture2DArrayLum32F::Ptr& inputDepths,
			bool passthroughDepth
		);

		/*void visibleCameraSelection(std::vector<uint>& cam_ids, const sibr::Mesh& mesh,
			const sibr::Camera& eye, const uint textureID);*/


		void fetchVisibleVoxels(const sibr::Mesh& mesh,
			const sibr::Camera& eye,
			const uint textureID, const uint specTextureID);


		void renderSlicePerVoxel(const sibr::Camera& eye,
			const float w, const float h, 
			const int sliceIdx,
			const Eigen::Vector3i& selectedVoxel,
			IRenderTarget& sliceProjections,
			const sibr::Texture2DArrayRGB::Ptr& inputRGBs,
			bool passthroughDepth = false
		);

		float refineSlices(const sibr::Camera& eye,
			const float w, const float h,
			const int sliceIdx, const sibr::Texture2DRGB::Ptr& outView,
			const Eigen::Vector3i& selectedVoxel,
			IRenderTarget& dst, IRenderTarget& mipMap,
			const sibr::Texture2DArrayRGB::Ptr& inputRGBs,
			bool passthroughDepth = false
		);

		void renderSlice(
			const sibr::Mesh& mesh,
			const sibr::Camera& eye,
			RenderTargetRGBA32F::Ptr& dst,
			const uint textureID, const uint specTextureID,
			const sibr::Texture2DArrayRGB::Ptr& inputRGBs,
			const int sliceIdx,
			bool passthroughDepth = false
		);

		void renderSliceError(
			IRenderTarget& dst
		);

		void startProfiling();

		void stopProfiling();

		struct DrawElementsIndirectCommand {
			uint32_t count;
			uint32_t instanceCount;
			uint32_t firstIndex;
			uint32_t baseVertex;
			uint32_t baseInstance;
		};
			
	protected:

		/** Camera infos data structure shared between the CPU and GPU.
			We have to be careful about alignment if we want to send those struct directly into the UBO. */
		struct CameraUBOInfos {	 
			Matrix4f vp; ///< Matrix viewproj.
			Vector3f pos; ///< Camera position.
			int selected = 0; ///< Is the camera selected (0/1).
			Vector3f dir; ///< Camera direction.
			float dummy = 0.0f; ///< Padding to a multiple of 16 bytes for alignment on the GPU.
		};

		struct CostIndex
		{
			float  cost;
			size_t index;
			CostIndex(float c, size_t i) : cost(c), index(i) {}
			bool operator<(const CostIndex& o)  const { return cost < o.cost; }
			bool operator>=(const CostIndex& o) const { return cost >= o.cost; }
		};

		struct DeviationIndex
		{
			float  dev;
			size_t index;
			DeviationIndex(float c, size_t i) : dev(c), index(i) {}
			bool operator<(const DeviationIndex& o)  const { return dev < o.dev; }
			bool operator>=(const DeviationIndex& o) const { return dev >= o.dev; }
		};

		void resetSliceImage();
		Eigen::Vector2f vec2devs(Eigen::Vector3f& inputVec, Eigen::Vector3f& normalVec, Eigen::Vector3f& upVec);
		Eigen::Vector2f vec2devs(Eigen::Vector3f& inputVec);
		void createCamData(const std::vector<sibr::InputCamera::Ptr>& cameras);
		void createComponentsData(const std::vector<PatchCloud::Component::Ptr>& components);
		void createGridOccupancyData();
		void createGPUGrid();

		void rankVoxelsAndPopulateCommands(const sibr::Camera& eye);
		//void rankVoxelsAndPopulateCommands(const size_t max_bins, const sibr::Camera& eye);

		void populateCommandPerSlice(const size_t max_bins, const sibr::Camera& eye, const int sliceIdx);


		
		inline size_t next_bit(uint64_t bf, size_t bit) { return ctz(bf & ~((1UL << bit) - 1)); }
		// https://stackoverflow.com/questions/355967/how-to-use-msvc-intrinsics-to-get-the-equivalent-of-this-gcc-code/20468180#20468180
		// https://msdn.microsoft.com/en-us/library/wfd9z0bb.aspx
		size_t __inline ctz(uint64_t x)
		{
			unsigned long index;
			return _BitScanForward64(&index, x) ? index : 64;
		}
		// https://msdn.microsoft.com/en-us/library/windows/desktop/ms683597%28v=vs.85%29.aspx
		size_t doAtomicOp(size_t* progress, int val) {
			return _InterlockedExchangeAdd(progress, val);
		}

		sibr::Timer		_timer0;	
		sibr::Timer		_timer1;
		sibr::Timer		_timer2;
		sibr::Timer		_timer4;
		sibr::Timer		_timer5;
		sibr::Timer		_timer3;
		sibr::Timer		_timer6;
		sibr::Timer		_timer7;
		sibr::Timer		_timer8;
		sibr::Timer		_timerTemp;

		/// Shader names.
		std::string fragString, vertexString;

		sibr::GLShader _hybridCompShader;
		sibr::GLShader _depthShader;
		sibr::GLShader _ulrShader;
		sibr::GLShader _bgCompo;
		sibr::GLShader _computeMaskShader;
		sibr::GLShader _pvMeshShader;
		sibr::GLShader _clearBinShader;
		sibr::GLShader _depthBinShader;
		sibr::GLShader _patchDepthShader;
		sibr::GLShader _consensusFilterShader;
		sibr::GLShader _gpuSortShader;
		sibr::GLShader _spatialFilterShader;
		sibr::GLShader _peelingShader;
		sibr::GLShader _blendShader;
		sibr::GLShader _averageShader;
		sibr::GLShader _guidedFilter;
		sibr::GLShader _textureShader;

		GLuint _rankProgram;

		RenderTargetRGBA32F::Ptr			_depthRT;
		RenderTargetRGBA32F::Ptr			_sliceRT;
		RenderTargetRGBA32F::Ptr			_patchDepthRT;
		RenderTargetRGBA32F::Ptr			_voxelRT;
		RenderTargetRGBA32F::Ptr			_filterRT;
		RenderTargetRGBA32F::Ptr			_ulrRT;
		RenderTargetRGBA32F::Ptr			_maskRT;
		RenderTargetRGBA32F::Ptr			_layerDepthRT;
		RenderTargetRGBA32F::Ptr			_spatialFilterRT;
		RenderTargetRGBA32F::Ptr			_compositeRT;
		RenderTargetRGBA32F::Ptr			_layersRT[NUM_LAYERS];

		GLuniform<Matrix4f>					_nCamProj[2];
		std::vector<GLuniform<Vector3f>>	_nCamPos;
		std::vector<GLuniform<Matrix4f>>	_nWorldToCam;
		std::vector<GLuniform<Matrix4f>>	_nCam2Clip;

		GLuniform<bool>
			_occTest = true,
			_renderMode = true,
			_flipRGBs = false,
			_toggleMask = false,
			_showWeights = false,
			_winnerTakesAll = false,
			_gammaCorrection = false;

		size_t _maxNumCams = 0;
		size_t _all_triangles = 0;

		GLuniform<int> _sliceView = 0;
		GLuniform<int> _camsCount = 0;
		GLuniform<float> _threshold = 0.83f;
		GLuniform<float> _threshold_geo = 0.03f;
		GLuniform<float> _alpha = 7.0f;

		GLuniform<float> _filterThreshold = 0.0f;
		GLuniform<int>	_kernelSize = 9;
		GLuniform<float> _weightExps = 7.0f;
		GLuniform<int>	_maskKernelSize = 9;
		GLuniform<bool> _variableSigma = false;
		GLuniform<bool> _gpuSorting = true;

		GLuniform<float>					_epsilonOcclusion = 0.01f;
		bool								_backFaceCulling = true;
		bool								_sliceRejection = false;
		bool								_clearDst = true;
		int									_displayDepth = 0;
		float								_threshold_var = 0.05f;
		float								_threshold_dev = 30.0f;

		GLuint								_uboIndex;
		std::vector<CameraUBOInfos>			_cameraInfos;
		sibr::Texture2DArrayRGBA32F::Ptr	_patchArray1;
		sibr::Texture2DArrayRGBA32F::Ptr	_patchArray2;

		// Patch cloud properties
		GLuniform<int>						_displayMode = static_cast<int>(sibr::PatchCloud::Colors);
		GLuniform<int>						_displayLayer = 0;
		GLuniform<int>						_numIter = 0;
		GLuniform<int>						_displayLayer1 = 0;
		int									_sliceIdx = 0;
		bool								_showSlices = false;
		sibr::ImageRGB32F::Ptr				_sliceErrImg;
		int									_sliceSize = 9;


		GLuniform<bool>						_useIOMultiPass = true;
		GLuniform<float>					_ibrSigma = 0.25f;
		GLuniform<float>					_ibrResAlpha = 0.1f;
		GLuniform<float>					_ibrDepthAlpha = 0.835f;
		GLuniform<int>						_blendIndex = 0;
		GLuniform<int>						_depthIndex = 0;
		GLuniform<int>						_shaderIdx;

		int									_cluster_k = 12;
		int									_cluster_sub_k = 12;
		int									_cluster_threshold = 4;

		bool								_showWireframe = false;
		bool								_useBFilter = false;
		bool								_useGeometric = false;

		size_t								_selectedVoxel = -1;
		std::vector<size_t>					_selectedVoxels;
		size_t								_selectedFineVoxel = -1;
		std::string							_debugPath;
		bool								_debugPrint = false;

		GLuint                         		_camera_pos_texture;
		GLuint                         		_patchFrameBuffer;
		GLuint                         		_patchDepthArray;
		GLuint                         		_command_to_layer_buffer;
		GLuint                         		_command_to_layer_texture;
		GLuint                         		_draw_calls;
		GLuint                        		_vao;
		GLuint                        		_index_buffer;
		GLuint                        		_vertex_buffer;
		GLuint                        		_texcoord_buffer;
		GLuint                         		_gpu_grid[4];
		
		std::vector<PatchCloud::Component::Ptr>	_components;
		sibr::VoxelGridFRIBR				_grid;
		sibr::VoxelGridFRIBR				_fineGrid;
		int									_refinement_Scale;
		
		GLuniform<Vector2i>					_resolution;
		GLuniform<Vector3f>					_grid_min;
		GLuniform<Vector3f>					_grid_max;
		GLuniform<Vector3f>					_grid_min_comp;
		GLuniform<Vector3f>					_grid_max_comp;
		GLuniform<float>					_tolerance = 0.05f;
		GLuniform<float>					_thresh = 1.0f;
		GLuniform<bool>						_neighborhood = true;
		GLuniform<float>					_compositingMargin = 0.03f;
		GLuniform<float>					_compoMargin = 0.03f;

		std::vector<int>					_command_to_layer;
		size_t								_command_index = 0;
		size_t								_total_triangles = 0;
		size_t								_voxel_index = 0;

		std::vector<unsigned int>			_fineVoxelComponentIndices;
		std::vector<int>					_fineVoxelComponents;

		GLuint								_fineVoxelComponentIndicesBuffer;
		GLuint								_fineVoxelComponentsBuffer;
		GLuint								_fineVoxelComponentIndicesTextureBuffer;
		GLuint								_fineVoxelComponentsTextureBuffer;
		GLuint								_selectedCamerasBuffer;
		GLuint								_selectedCamerasTextureBuffer;
		GLuint								_componentsPositionBuffer;
		GLuint								_componentsPositionTextureBuffer;
	
		GLuint								_fineGridSizeUniform;
		GLuint								_fineGridMinUniform;
		GLuint								_fineGridMaxUniform;
		GLuint								_cameraPositionUniform;


		bool								_update_voxel_memory = true;
		std::vector<bool>					_voxel_memory[VOXEL_MEMORY_SIZE];

		std::vector<bool>							_fat_grid;
		std::vector<PatchCloud::ImageBin>			_bins;
		std::vector<size_t>							_fineBinIdx;
		size_t										_max_bins;
		size_t										_numVisibleBins;
		size_t										_total_bins;
		std::vector<DrawElementsIndirectCommand>	_commands;
		std::vector<sibr::InputCamera::Ptr>			_cams;
		
		std::vector<unsigned int>					_visibleVoxels;
		std::vector<int>							_bestCameras;

		GLuint										_rankBuffer;
		GLuint										_rankTextureBuffer;
		GLuint										_visibleVoxelsBuffer;
		GLuint										_visibleVoxelsTextureBuffer;

		int											_numFramesProfiling = 100;
		std::string									_profileStr = "";
		std::vector<float>							_fetchVoxel1, _fetchVoxel2, _rankVoxels, 
													_pvMeshRaster, _clusterBlend, _spFilter, 
													_ulrCost, _compositing;
		bool										_profiling = false;
		int											_maxPVMCameras;

	};


	inline void sibr::HybridRenderer::update_voxel_memory(bool b)
	{
		_update_voxel_memory = b;
	}

	inline void HybridRenderer::clear_voxel_memory()
	{
		for (int i = 0; i < VOXEL_MEMORY_SIZE; ++i)
			_voxel_memory[i].clear();
	}

	inline void HybridRenderer::cluster_k(int k)
	{
		_cluster_k = std::max(_cluster_threshold, k);
		std::cout << "Number of complete tiles to draw: " << _cluster_threshold << std::endl;
		std::cout << "Updating max number of tiles to draw to: " << _cluster_k << std::endl;
	}

	inline void HybridRenderer::cluster_subk(int k)
	{
		_cluster_sub_k = std::max(_cluster_threshold, k);
	}

	inline void HybridRenderer::cluster_threshold(int k)
	{
		_cluster_threshold = k;
		_cluster_k = std::max(12, _cluster_threshold);
		std::cout << "Updating number of complete tiles to draw to: " << _cluster_threshold << std::endl;
		std::cout << "Or max number of tiles to draw to: " << _cluster_k << std::endl;
	}



} /*namespace sibr*/ 

