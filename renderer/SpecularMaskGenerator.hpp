// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#pragma once

#include "Config.hpp"

#include <core/imgproc/MRFSolver.h>
#include "core/graphics/Texture.hpp"
#include "core/graphics/Window.hpp"
#include <Eigen/Core>


namespace sibr {

	class SIBR_EXP_HYBRID_EXPORT SpecularMaskGenerator
	{
		SIBR_DISALLOW_COPY(SpecularMaskGenerator);

	public:
		typedef std::shared_ptr<SpecularMaskGenerator>		Ptr;
		//DeepBlendingScene::DeepBlendingScene();
		SpecularMaskGenerator::SpecularMaskGenerator(sibr::ImageRGB32F& image, sibr::ImageRGB32F& varMap, sibr::ImageRGB32F& diffMap, sibr::ImageRGB32F& gradX, sibr::ImageRGB32F& gradY);
		
		~SpecularMaskGenerator() {};

		double unaryCost(int p, int l);

		double binaryCost(int p1, int p2, int l1, int l2);

		void solve();

		const int pixelToArray(const int x, const int y) const;

		const sibr::Vector2i arrayToPixel(const int i) const;

		sibr::ImageL8 getResult();

		sibr::ImageRGB32F getOverlayResult();
		
	private:

		typedef std::function<double(int)>				FuncItoF;
		typedef std::function<double(int, int)>			Func2ItoF;
		typedef std::function<double(int, int, int, int)>	Func4ItoF;

		sibr::ImageRGB32F _image, _varMap, _diffMap, _gradX, _gradY;
		std::vector<std::vector<int>> _neighboursMap;
		std::shared_ptr<sibr::MRFSolver> _mrfSolver;
		uint _w, _h, _wExtMargin, _hExtMargin, _wIntMargin, _hIntMargin;
		
	};

		
}