# Copyright (C) 2021, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

SIBR_BIN=D:/codes/sibr_core/install/bin
DATASET_DIR=D:/datasets/hybrid_ibr
DATASET_NAMES=(Synthetic_Attic Library Salon Playroom Ponche Dr_Johnson Creepy_Attic Train Hugo)
echo "Start All Preprocess at:"
echo $(date +"%x %r %Z")
for((i=0; i < ${#DATASET_NAMES[@]}; i++))
do
	echo "====Start Preprocess at:===="
	echo $(date +"%x %r %Z")
	cd $SIBR_BIN/../scripts/
	echo $pwd
	
	DATASET_NAME=${DATASET_NAMES[$i]}
	echo $DATASET_NAME
	DATASET=$DATASET_DIR/$DATASET_NAME
	[ ! -d "${DATASET}/colmap/stereo" ] && echo "Directory ${DATASET}/colmap/stereo DOES NOT exists. Please run fullColmapPreprocess.py script." && continue
	
	if [ "${DATASET_NAME}" = "Train" ] 
	then
		echo "grid bound"
		[ -d "${DATASET}/deep_blending" ] && python ibr_hybrid_preprocess.py -r -i ${DATASET} -w 1920 -t 0 -b
		[ ! -d "${DATASET}/deep_blending" ] && python ibr_hybrid_preprocess.py -r -i ${DATASET} -w 1920 -b
	else
		echo "unbound"
		[ -d "${DATASET}/deep_blending" ] && python ibr_hybrid_preprocess.py -r -i ${DATASET} -w 1920 -t 0
		[ ! -d "${DATASET}/deep_blending" ] && python ibr_hybrid_preprocess.py -r -i ${DATASET} -w 1920
	fi
	
	echo "====End Preprocess at:===="
echo $(date +"%x %r %Z")
done;
echo "End All Preprocess at:"
echo $(date +"%x %r %Z")
