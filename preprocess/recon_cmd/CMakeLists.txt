# Copyright (C) 2021, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

# project name
project(sibr_recon_cmd)

file(GLOB SOURCES "*.cpp" "*.h" "*.hpp")
add_definitions(-DINRIA_WIN)

# Define build output for project
add_executable(${PROJECT_NAME} ${SOURCES})

target_link_libraries(${PROJECT_NAME}
    ${Boost_LIBRARIES}
    OpenMP::OpenMP_CXX
    sibr_system
    sibr_graphics
    sibr_view
    sibr_assets
    sibr_renderer
    sibr_fribr_framework
    sibr_hybrid
    MixKitNext
)


add_definitions(-DTF_INTEROP)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "projects/hybrid/preprocess")
if (WIN32)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd4251")
endif()



## High level macro to install in an homogen way all our ibr targets
include(install_runtime)
ibr_install_target(${PROJECT_NAME}
    INSTALL_PDB                         ## mean install also MSVC IDE *.pdb file (DEST according to target type)
    STANDALONE  ${INSTALL_STANDALONE}   ## mean call install_runtime with bundle dependencies resolution
    COMPONENT   ${PROJECT_NAME}_install ## will create custom target to install only this project
)
