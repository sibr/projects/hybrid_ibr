// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#pragma once

#include <projects/hybrid_ibr/renderer/HybridScene.hpp>

#include <core/renderer/TexturedMeshRenderer.hpp>
#include <core/renderer/ColoredMeshRenderer.hpp>
#include <core/graphics/Window.hpp>
#include <core/system/Utils.hpp>
#include <core/system/SimpleTimer.hpp>
#include <core/system/CommandLineArgs.hpp>

#include <opencv2/features2d/features2d.hpp>
#include <opencv2/flann/flann.hpp>
#include <boost/filesystem.hpp>
#include <fstream>

#define PROGRAM_NAME "sibr_patchCloud_generator"

using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path <reference scene path> " "\n";


#define DEBUG 0

std::vector<fribr::Mesh::Ptr> load_precomputed_meshes(const std::string& per_view_mesh_dir,
	const std::vector<sibr::InputCamera::Ptr>& cameras)
{
	std::vector<fribr::Mesh::Ptr> meshes;
	for (int i = 0; i < static_cast<int>(cameras.size()); ++i)
	{
		const sibr::InputCamera::Ptr	cam = cameras[i];
		if (cam->isActive()) {
			const std::string      mesh_file = sibr::removeExtension(cam->name()) + ".ply";
			const std::string      mesh_path = per_view_mesh_dir + "/" + mesh_file;

			std::vector<fribr::Mesh::Ptr> camera_meshes = load_meshes(mesh_path,
				fribr::CoordinateSystem::RH_Y_UP, fribr::Mesh::CPUOnly);
			if (camera_meshes.empty())
			{
				std::cerr << "Could not load per-view mesh at: " << mesh_path << std::endl;
				exit(1);
			}

			//// [SP] If a specific per-view mesh contain more than 1 mesh, 
			//// use the mesh with largest number of mesh vertices
			int best_index = 0;
			size_t best_size = 0;
			if (camera_meshes.size() > 1)
			{
				std::cout << "WARNING: per-view ply-file contains " << camera_meshes.size()
					<< " meshes! (" << mesh_path << ")" << std::endl;
				for (int j = 0; j < static_cast<int>(camera_meshes.size()); ++j)
				{
					if (camera_meshes[j]->get_vertices().size() > best_size)
					{
						best_index = j;
						best_size = camera_meshes[j]->get_vertices().size();
					}
				}
			}
			meshes.emplace_back(camera_meshes[best_index]);
		}
		else {
			meshes.emplace_back(fribr::Mesh::Ptr());
		}
	}

	return meshes;
}

struct PatchCloudGenArgs : 
	virtual BasicIBRAppArgs {
	Arg<int> grid_size = { "grid_size", 32 };
	Arg<int> r_scale = { "scale", 4 };
	Arg<bool> grid_bounds = { "grid_bounds" };
	Arg<std::string> outfile = { "output", "output_hybrid.pcloud" };
};

int main(int ac, char** av)
{
	CommandLineArgs::parseMainArgs(ac, av);
	PatchCloudGenArgs mainArgs;
	// rendering size
	uint rendering_width = mainArgs.rendering_size.get()[0];
	uint rendering_height = mainArgs.rendering_size.get()[1];

	sibr::Window window(50, 50, PROGRAM_NAME, mainArgs);
	
	BasicIBRScene::Ptr scene(new BasicIBRScene(mainArgs, true));
	uint scene_width = scene->cameras()->inputCameras()[0]->w();
	uint scene_height = scene->cameras()->inputCameras()[0]->h();
	float scene_aspect_ratio = scene_width * 1.0f / scene_height;
	float rendering_aspect_ratio = rendering_width * 1.0f / rendering_height;

	if ((rendering_width > 0)) {
		if (abs(scene_aspect_ratio - rendering_aspect_ratio) > 0.001f) {
			if (scene_width > scene_height) {
				rendering_height = rendering_width / scene_aspect_ratio;
			}
			else {
				rendering_width = rendering_height * scene_aspect_ratio;
			}
		}
	}

	// check rendering size
	rendering_width = (rendering_width <= 0) ? scene->cameras()->inputCameras()[0]->w() : rendering_width;
	rendering_height = (rendering_height <= 0) ? scene->cameras()->inputCameras()[0]->h() : rendering_height;

	const uint flags = SIBR_GPU_LINEAR_SAMPLING | SIBR_FLIP_TEXTURE;

	std::vector<sibr::InputCamera::Ptr> cams, resized_cams;
	cams = scene->cameras()->inputCameras();
	const int numCams = cams.size();
	//resized_cams.resize(numCams);
	for (int i = 0; i < numCams; ++i) {
		sibr::InputCamera cam = cams[i]->resizedW(rendering_width);
		cam.setActive(cams[i]->isActive());
		resized_cams.push_back(std::make_shared<sibr::InputCamera>(cam));
	}

	

	const std::string per_view_mesh_dir = std::string(mainArgs.dataset_path.get() + "/deep_blending/pvmeshes");
	std::vector<fribr::Mesh::Ptr> meshes;
	if (directoryExists(per_view_mesh_dir))
	{
		std::cout << "Loading precomputed meshes" << std::endl;
		meshes = load_precomputed_meshes(per_view_mesh_dir, cams);
	}
	else {
		SIBR_ERR << "No precomputed per view mesh found at" << per_view_mesh_dir << std::endl;
	}

	int  grid_size = mainArgs.grid_size;
	int refinement_Scale = mainArgs.r_scale;
	std::cout << "Setting grid size to (" << grid_size << "x" << grid_size << "x" << grid_size << ")" << std::endl;
	std::cout << "Setting refinement scale to " << refinement_Scale << std::endl;
	std::cout << "Creating refined patch cloud..." << std::endl;
	sibr::PatchCloud cloud(Eigen::Vector3i(grid_size, grid_size, grid_size));
	if (mainArgs.grid_bounds)
	{
		Eigen::Array3f camera_min(1e21f, 1e21f, 1e21f);
		Eigen::Array3f camera_max(-1e21f, -1e21f, -1e21f);
		for (int i = 0; i < numCams; ++i)
		{
			camera_min = camera_min.min(cams[i]->position().array());
			camera_max = camera_max.max(cams[i]->position().array());
		}

		static const float SAFETY_MARGIN = 5.0f;
		static const float EXPANSION_FACTOR = (SAFETY_MARGIN - 1.0f) / 2.0f;
		Eigen::Array3f camera_diagonal = camera_max - camera_min;
		camera_min -= EXPANSION_FACTOR * camera_diagonal;
		camera_max += EXPANSION_FACTOR * camera_diagonal;

		cloud.set_scene(scene->proxies()->proxyPtr(), camera_min, camera_max);
	}
	else
	{
		cloud.set_scene(scene->proxies()->proxyPtr());
	}
	cloud.set_refinementScale(refinement_Scale);
	

	cloud.add_cameras(resized_cams, meshes, sibr::PatchCloud::DeallocateWhenDone);

	

	const std::string cloud_path = std::string(mainArgs.dataset_path.get() + "/hybrid/" + mainArgs.outfile.get());
	std::cout << "\nSaving result in " << cloud_path << std::endl;
	cloud.save(cloud_path);

	
	return EXIT_SUCCESS;
}
