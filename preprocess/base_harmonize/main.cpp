// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#include <fstream>
#include <boost/filesystem.hpp>
#include <core/system/Utils.hpp>
#include <core/graphics/Window.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/flann/flann.hpp>
#include <core/renderer/DepthRenderer.hpp>
#include <core/renderer/TexturedMeshRenderer.hpp>
#include <core/renderer/ColoredMeshRenderer.hpp>
#include <core/system/CommandLineArgs.hpp>
#include <core/scene/BasicIBRScene.hpp>
#include <core/system/SimpleTimer.hpp>

#define PROGRAM_NAME "base_harmonize"

using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path <reference scene path> -path2 <scene to align path> -outPath <mesh output path>" "\n"
;

template <typename T> std::vector<size_t> sort_indexes(const std::vector<T> &v) {

	// initialize original index locations
	std::vector<size_t> idx(v.size());
	iota(idx.begin(), idx.end(), 0);

	// sort indexes based on comparing values in v
	sort(idx.begin(), idx.end(),
		[&v](size_t i1, size_t i2) {return v[i1] < v[i2]; });

	return idx;
}

void computeRT(std::vector<sibr::Vector3f> A, std::vector<sibr::Vector3f>B, Matrix3f S, Matrix3f & R, sibr::Vector3f & T) {

	//finding R,T such that B = RA + T;
	const int numPoint = B.size();
	//Scaling the points to align
	std::vector<sibr::Vector3f> AScaled;
	for (int i = 0; i < numPoint; i++) {
		AScaled.push_back(S*A[i]);
	}

	//Computing the centroids
	sibr::Vector3f centroidB(0, 0, 0);
	sibr::Vector3f centroidA(0, 0, 0);
	for (int i = 0; i < numPoint; i++) {
		centroidB += B[i];
		centroidA += AScaled[i];
	}
	centroidB /= numPoint;
	centroidA /= numPoint;


	//Now we estimate the rotation :
	Matrix3f H;
	H << 0, 0, 0,
		0, 0, 0,
		0, 0, 0;

	for (int i = 0; i < numPoint; i++) {
		H += (AScaled[i] - centroidA) * (B[i] - centroidB).transpose();
	}

	Eigen::JacobiSVD<Eigen::MatrixXf> svd(H, Eigen::ComputeThinU | Eigen::ComputeThinV);

	R = svd.matrixV()*svd.matrixU().transpose();
	if (R.determinant() < 0) {
		//R.col(2) *= -1;
		//std::cout << "Warning : determinant of rotation matrix is negative, multiplying last column by -1" << std::endl;
	}

	//Translation estimation :
	T = -R * centroidA + centroidB;

}

float computeS(std::vector<sibr::Vector3f> A, std::vector<sibr::Vector3f>B, float & minScale, float & maxScale) {
	//findin S such that A*S has the same scale as B
	sibr::Vector3f meanPosA(0, 0, 0);
	sibr::Vector3f meanPosB(0, 0, 0);

	for (int i = 0; i < A.size(); i++) {
		meanPosA += A[i];
		meanPosB += B[i];
	}

	meanPosA /= A.size();
	meanPosB /= A.size();

	float scale = 0;
	for (int i = 0; i < A.size(); i++) {

		float scale_i = (B[i] - meanPosB).norm() / (A[i] - meanPosA).norm();
		scale += scale_i;

		if (scale_i > maxScale)
			maxScale = scale_i;
		if (scale_i < minScale)
			minScale = scale_i;
	}

	scale /= A.size();

	return scale;
}

// Convienience function to find the Median of the given vector.
// Note that it isn't a strict median( just a quick approximation. )
float findMAD(const Eigen::VectorXf& vec) {
	Eigen::VectorXf vec_ = vec;
	// Sort the data in increasing order.
	std::sort(vec_.data(), vec_.data() + vec_.size());
	// Return the 'middle' element.
	return vec_[((vec_.size() + 1) / 2)];
}

// Weight function.( Takes a list of standardized adjusted residuals and returns the square-root-weights for each one )
// Currently, the Bisquares estimator is used.
// Note that this function should return the square root of the actual weight value since both X and Y are multiplied by this vector.
Eigen::VectorXf weight(Eigen::VectorXf v) {
	Eigen::VectorXf vout = v;

	for (int i = 0; i < v.size(); i++) {
		float r = v[i];
		vout[i] = ((abs(r) < 1) ? (1 - (r * r)) : 0);
	}

	return vout;
}


float weightedMean(std::vector<std::pair<float, float>> radianceSet) {

	if (radianceSet.size() == 0) {
		return 0.0f;
	}

	float cumRadiance = std::accumulate(radianceSet.begin(), radianceSet.end(), 0.0f,
		[](float previous, auto& element) { return previous + element.first * element.second; });

	float cumWeight = std::accumulate(radianceSet.begin(), radianceSet.end(), 0.0f,
		[](float previous, auto& element) { return previous + element.first; });


	return cumRadiance / cumWeight;
	
}

// Calculate weighted median given an ordered map of weights (float) as the keys and data (float) as the values.
// Inspired from https://stackoverflow.com/questions/9794558/weighted-median-computation
float weightedMedian(std::vector<std::pair<float, float>> radianceSet) {
	
	if (radianceSet.size() == 0) {
		return 0.0f;
	}

	const size_t numElements = radianceSet.size();
	//SIBR_LOG << "[ColorHarmonize] Printing elements: weights, radiance " << std::endl;
	/*for (auto& it : radianceSet) {
		std::cout << it.first << " , " << it.second << std::endl;
	}*/

	float cumWeight = std::accumulate(radianceSet.begin(), radianceSet.end(), 0.0f,
		[](float previous, auto& element) { return previous + element.first; });

	sort(radianceSet.begin(), radianceSet.end());
	//std::cout << "Cumulative weight: " << cumWeight << std::endl;

	// sum is the total weight of all `x[i] > x[k]`
	float sum = cumWeight - radianceSet.begin()->first;
	float prevElement;
	for (auto& it : radianceSet) {
		//std::cout << "Sum: " << sum << std::endl;
		if (sum > (cumWeight / 2.0f)) {
			//std::cout << "Radiance weight: " << 1.0f*it.first << std::endl;
			sum -= it.first*1.0f;
		}
		else {
			//std::cout << "Median Radiance: " << 1.0f*it.second << std::endl;
			return it.second*1.0f;
		}
		prevElement = it.second*1.0f;
	}
	//std::cout << "Median Radiance: " << prevElement << std::endl;
	return prevElement;
}

sibr::Vector3f confidence(sibr::ColorRGBA pix) {
	float r = pix.x();
	float g = pix.y();
	float b = pix.z();

	if (r > 0.5f) { r = r / 0.5f; }
	else { r = (1.0f - r) / 0.5f; }
	if (g > 0.5f) { g = g / 0.5f; }
	else { g = (1.0f - g) / 0.5f; }
	if (b > 0.5f) { b = b / 0.5f; }
	else { b = (1.0f - b) / 0.5f; }

	return Vector3f(r, g, b);
}

#define MAX_ITERS 100
#define DEBUG 0

// Procedure for IRLS( Iterative Reweighted Least Squares ).
void irls(Eigen::MatrixXf mX, Eigen::VectorXf vY, float & mCoeffs, float tune) {

	Eigen::MatrixXf mX_ = mX;
	Eigen::VectorXf vY_ = vY;
	// Find the least squares coefficients.
	Eigen::VectorXf vC = mX_.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(vY);
	//Log(EInfo, "Finished solving for LS solution.");

	// Form the leverage value matrix as H = X . ( X_T . X ) . X_T
	// Form the leverage factor matrix as 1/sqrt(1 - diag(H))
	Eigen::VectorXf mH = (mX_ * (((mX_.transpose() * mX_).inverse()) * mX_.transpose())).diagonal();
	Eigen::MatrixXf mH_ = (Eigen::VectorXf::Constant(mH.rows(), 1, 1) - mH).cwiseSqrt().cwiseInverse().asDiagonal();

	std::cout << vC << std::endl;
	for (int i = 0; ; i++) {
		std::cout << "IRLS: Iteration " << i << ":";

		// Find residuals:
		Eigen::VectorXf resid = vY - mX * vC;

		float mad = findMAD(resid.cwiseAbs());

		// Calcualte Standardized Adjusted Residuals.
		Eigen::VectorXf r = (mH_ * resid * 0.6745) / (mad * tune);

		// Find the root weight of residuals.
		Eigen::VectorXf wt = weight(r);

		// Multiply X and Y with the root of the weights.
		mX_ = wt.asDiagonal() * mX;
		vY_ = wt.asDiagonal() * vY;

		std::cout << "MAD= " << mad << ", ";

		// Regress the weighted X and Y to find weighted least squares optimisation.
		Eigen::VectorXf vC_ = mX_.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(vY_);

		// Find mean deviation in coefficients.
		float meanDiff = (vC - vC_).cwiseAbs().mean();
		std::cout << "MD=" << meanDiff << ",";
		// Terminate if the deviation is too small or number of iterations has been exceeded.
		if (meanDiff < 0.01f || i > MAX_ITERS) {
			mCoeffs = vC_.x();
			std::cout << "\n";
			break;
		}

		vC = vC_;
		std::cout << "\n";
	}
	std::cout << vC << std::endl;
}

struct linearHarmonizeAppArgs :
	virtual BasicIBRAppArgs {
	Arg<std::string> meshPath = { "mesh", "" };
	Arg<std::string> fullMeshPath = { "fullMesh", "" };
	Arg<std::string> texturePath = { "texture", "" };
	Arg<std::string> outputPath = { "output", "" };
};

int main(int ac, char** av)
{
	CommandLineArgs::parseMainArgs(ac, av);
	linearHarmonizeAppArgs mainArgs;

	sibr::Window window(100, 100, "Window");

	bool synthetic = false;
	std::string inPath;
	inPath = mainArgs.dataset_path.get();

	BasicIBRScene::Ptr scene(new BasicIBRScene(mainArgs, true, true));

	std::vector<sibr::InputCamera::Ptr> cams;
	cams = scene->cameras()->inputCameras();


	sibr::Mesh mesh, meshClone;
	if (mainArgs.meshPath.get() != "") {
		mesh.load(mainArgs.meshPath);
	}
	else {
		mesh.load(scene->data()->meshPath());
	}
	meshClone = *mesh.clone();



	//std::vector<std::vector<int>> visibility;
	std::map<int, std::map<int, sibr::ImageRGB32F::Pixel>> visibility;
	sibr::ColoredMeshRenderer CMR;
	std::string outPath;

	if (mainArgs.outputPath.get() != "") {
		outPath = mainArgs.outputPath.get() + "/";
	}
	else {
		outPath = mainArgs.dataset_path.get() + "/lin_harmonized_images_base/";
	}

	if (!directoryExists(outPath)) {
		makeDirectory(outPath);
	}


	sibr::ImageRGB32F imgsLinProxy;
	sibr::ImageRGB32F imgsLin2Harmonize;

	//Create the two scenes
	std::string texImgPath, meshPath;
	sibr::ImageRGB textureImg;
	if (mainArgs.texturePath.get() != "" && mainArgs.meshPath.get() != "") {
		texImgPath = mainArgs.texturePath;
		meshPath = mainArgs.meshPath;
	}
	else {
		texImgPath = removeExtension(scene->data()->meshPath()) + "_u1_v1.png";
		meshPath = removeExtension(scene->data()->meshPath()) + ".obj";
	}

	mesh.load(meshPath);
	if (sibr::fileExists(texImgPath)) {
		textureImg.load(texImgPath);
		scene->inputMeshTextures().reset(new sibr::Texture2DRGB(textureImg, SIBR_GPU_LINEAR_SAMPLING));
	}
	else {
		SIBR_ERR << "Mesh texture file " << texImgPath << " does not exist" << std::endl;
	}
	sibr::Texture2DRGB tex(textureImg);

	window.makeContextCurrent();
	sibr::TexturedMeshRenderer TMR;

	sibr::ImageRGB32F tempImage;
	int numActive = 0;
	for (int c = 0; c < cams.size(); c++) {
		sibr::InputCamera cam = *cams[c];
		sibr::ImageRGB img = scene->images()->image(c).clone();

		if (cam.isActive()) {

			sibr::ImageRGB32F linIm;
#if DEBUG
			//tempImage.fromOpenCV(img);
			//lin2sRGB(tempImage);
			img.save(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_image." + getExtension(getFileName(cam.name())));
#endif
			img.toOpenCV().convertTo(linIm.toOpenCVnonConst(), CV_32FC3, (1.0f / 255.0f));
			sRGB2Lin(linIm);

#if DEBUG
			//tempImage.fromOpenCV(linIm);
			//lin2sRGB(tempImage);
			linIm.save(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_linearized_image." + getExtension(getFileName(cam.name())));
#endif

			imgsLin2Harmonize = linIm.clone();

#if DEBUG
			//tempImage.fromOpenCV(linIm);
			//lin2sRGB(tempImage);
			imgsLin2Harmonize.save(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_linearized_image_clone." + getExtension(getFileName(cam.name())));
#endif

			glViewport((GLint)(0), (GLint)(0), (GLsizei)(img.w()), (GLsizei)(img.h()));
			sibr::RenderTargetRGB RT(img.w(), img.h());
			RT.clear();
			TMR.process(mesh, cam, tex.handle(), RT);
			window.swapBuffer();
			RT.readBack(img);

#if DEBUG
			//tempImage.fromOpenCV(linIm);
			//lin2sRGB(tempImage);
			img.save(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_proxy_image." + getExtension(getFileName(cam.name())));
#endif
			img.toOpenCV().convertTo(linIm.toOpenCVnonConst(), CV_32FC3, (1.0f / 255.0f));
			sRGB2Lin(linIm);
			imgsLinProxy = linIm.clone();

#if DEBUG
			//tempImage.fromOpenCV(linIm);
			//lin2sRGB(tempImage);
			imgsLinProxy.save(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_linearized_proxy_image." + getExtension(getFileName(cam.name())));
#endif

			//Format the data
			std::vector<float> XData0;
			std::vector<float> XData1;
			std::vector<float> XData2;
			std::vector<float> YData0;
			std::vector<float> YData1;
			std::vector<float> YData2;

			int kS = 10;
			int numX = 0;
			float weightFull = 0.1;
			float weightGround = 10.0;

			sibr::Vector3f proxyVal(0, 0, 0);
			sibr::Vector3f toHarmonizeVal(0, 0, 0);

#define MOVING_AVERAGE_HARMONIZATION 1
#if MOVING_AVERAGE_HARMONIZATION

			sibr::ImageRGB32F debugImgBlur, debugImgProxyBlur, debugBlurImgSat, debugBlurImgProxySat;

			static const int BLUR_RADIUS = 101;
			cv::Mat3f blurredLinProxy = imgsLinProxy.toOpenCV().clone();
			cv::Mat3f blurredImgsLin2Harmonize = imgsLin2Harmonize.toOpenCV().clone();
			cv::Mat3f rgbFactorImg = blurredLinProxy.clone();

#define MASK_OUT_SATURATED_REGIONS 1
#if MASK_OUT_SATURATED_REGIONS
			cv::Mat3f blurredLinProxySat = blurredLinProxy.clone();
			cv::Mat3f blurredImgs2Lin2HarmonizeSat = blurredImgsLin2Harmonize.clone();
			for (int j = 0; j < img.h(); j += 1) {
				for (int i = 0; i < img.w(); i += 1) {
					sibr::Vector3f imgColor = img(i, j).cast<float>();
					// Y = 0.299*R+0.587*G+0.114*B
					float luminance = (0.299 * imgColor.x() + 0.587 * imgColor.y() + 0.114 * imgColor.z());
					if (luminance <= 250.0f && luminance >= 5.0f && imgsLinProxy(i, j).norm() > 0.0f) {
						blurredLinProxySat(j, i) = cv::Vec3f(0.0f, 0.0f, 0.0f);
						blurredImgs2Lin2HarmonizeSat(j, i) = cv::Vec3f(0.0f, 0.0f, 0.0f);
					}
				}
			}

			cv::blur(blurredLinProxy, blurredLinProxy, cv::Size(BLUR_RADIUS, BLUR_RADIUS));
			cv::blur(blurredImgsLin2Harmonize, blurredImgsLin2Harmonize, cv::Size(BLUR_RADIUS, BLUR_RADIUS));
			cv::blur(blurredLinProxySat, blurredLinProxySat, cv::Size(BLUR_RADIUS, BLUR_RADIUS));
			cv::blur(blurredImgs2Lin2HarmonizeSat, blurredImgs2Lin2HarmonizeSat, cv::Size(BLUR_RADIUS, BLUR_RADIUS));

			blurredLinProxy -= blurredLinProxySat;
			blurredImgsLin2Harmonize -= blurredImgs2Lin2HarmonizeSat;

#if DEBUG
			sibr::ImageRGB32F debugBlurSat;
			debugBlurSat.fromOpenCV(blurredLinProxySat);
			lin2sRGB(debugBlurSat);
			debugBlurSat.save(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_saturated_mask." + getExtension(getFileName(cam.name())));
			
#endif

#else
			cv::blur(blurredLinProxy, blurredLinProxy, cv::Size(BLUR_RADIUS, BLUR_RADIUS));
			cv::blur(blurredImgsLin2Harmonize, blurredImgsLin2Harmonize, cv::Size(BLUR_RADIUS, BLUR_RADIUS));
#endif

#if DEBUG
			sibr::ImageRGB32F blurredProxy, blurredImg;
			blurredProxy.fromOpenCV(blurredLinProxy);
			lin2sRGB(blurredProxy);
			blurredProxy.save(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_blurred_proxy." + getExtension(getFileName(cam.name())));
			blurredImg.fromOpenCV(blurredImgsLin2Harmonize);
			lin2sRGB(blurredImg);
			blurredImg.save(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_blurred_img." + getExtension(getFileName(cam.name())));
#endif

			double min, max;
			cv::minMaxLoc(blurredImgsLin2Harmonize, &min, &max);
#if DEBUG
			std::cout << "Min value: " << min << " Max value: " << max << std::endl;
#endif

			//	#pragma omp parallel for
			for (int j = 0; j < img.h(); j += 1) {
				for (int i = 0; i < img.w(); i += 1) {
					if (imgsLinProxy(i, j).norm() <= 0.0f) {
						continue;
					}
				
					float r = std::max(blurredLinProxy(j, i)[0], 0.01f) / std::max(blurredImgsLin2Harmonize(j, i)[0], 0.01f);
					float g = std::max(blurredLinProxy(j, i)[1], 0.01f) / std::max(blurredImgsLin2Harmonize(j, i)[1], 0.01f);
					float b = std::max(blurredLinProxy(j, i)[2], 0.01f) / std::max(blurredImgsLin2Harmonize(j, i)[2], 0.01f);

					if (r == g == b == 1.0f) {
						continue;
					}

					Eigen::Matrix3f mFinal;


					mFinal << r, 0, 0,
						0, g, 0,
						0, 0, b;


					imgsLin2Harmonize(i, j) = mFinal * imgsLin2Harmonize(i, j);
					rgbFactorImg(j, i) = cv::Vec3f(r, g, b);
				}
			}
#else
			//#pragma omp parallel for
			for (int j = 0; j < img.h() - kS; j += kS) {
				for (int i = 0; i < img.w() - kS; i += kS) {

					float numVal = 0;

					for (int jj = 0; jj < kS; jj++) {
						for (int ii = 0; ii < kS; ii++) {


							sibr::Vector3f imgColor = img(i + ii, j + jj).cast<float>();
							float luminance = (imgColor.x() + imgColor.y() + imgColor.z()) / 3.0f; // Not really "luminance", there should be a bias for green.

							if (luminance > 250.0f || luminance < 5.0f) {
								continue;
							}


							if (imgsLinProxy(i + ii, j + ii).norm() > 0) {
								proxyVal += weightGround * imgsLinProxy(i, j);
								toHarmonizeVal += weightGround * imgsLin2Harmonize(i, j);
								numVal += weightGround;
							}

							/*
							if (fullMesh && imgsLinProxyFull(i + ii, j + ii).norm() > 0) {
								proxyVal += weightFull * imgsLinProxyFull(i, j);
								toHarmonizeVal += weightFull * imgsLin2Harmonize(i, j);
								numVal += weightFull;
							}*/
						}
					}
				}
			}

			float r = proxyVal.x() / std::max(toHarmonizeVal.x(), 0.001f);
			float g = proxyVal.y() / std::max(toHarmonizeVal.y(), 0.001f);
			float b = proxyVal.z() / std::max(toHarmonizeVal.z(), 0.001f);

			Eigen::Matrix3f mFinal;

			mFinal << r, 0, 0,
				0, g, 0,
				0, 0, b;

#pragma omp parallel for
			for (int j = 0; j < img.h(); j += 1) {
				for (int i = 0; i < img.w(); i += 1) {
					imgsLin2Harmonize(i, j) = mFinal * imgsLin2Harmonize(i, j);
				}
			}
#endif
			lin2sRGB(imgsLin2Harmonize);
#if DEBUG
			sibr::ImageRGB32F debugRGBFactor;
			debugRGBFactor.fromOpenCV(rgbFactorImg);
			lin2sRGB(debugBlurSat);
			debugRGBFactor.save(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_rgb_factor." + getExtension(getFileName(cam.name())));
#endif

			imgsLin2Harmonize.save(outPath + getFileName(cam.name()));
		}
	}

	return EXIT_SUCCESS;
}
