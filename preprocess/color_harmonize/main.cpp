// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#pragma once

#include <projects/hybrid_ibr/renderer/VarianceMeshGenerator.hpp>
#include <projects/hybrid_ibr/renderer/SpecularMaskGenerator.hpp>

#include <core/imgproc/PoissonReconstruction.hpp>
#include <core/renderer/DepthRenderer.hpp>
#include <core/renderer/TexturedMeshRenderer.hpp>
#include <core/renderer/ColoredMeshRenderer.hpp>
#include <core/graphics/Window.hpp>
#include <core/system/Utils.hpp>
#include <core/system/SimpleTimer.hpp>
#include <core/system/CommandLineArgs.hpp>

#include <opencv2/features2d/features2d.hpp>
#include <opencv2/flann/flann.hpp>
#include <boost/filesystem.hpp>
#include <fstream>

#define PROGRAM_NAME "sibr_color_harmonize"

using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path <reference scene path> -path2 <scene to align path> -outPath <mesh output path>" "\n";


#define MAX_ITERS 100
#define DEBUG 0


int main(int ac, char** av)
{
	CommandLineArgs::parseMainArgs(ac, av);
	HybridAppArgs mainArgs;

	sibr::Window window(100, 100, "Window");

	bool synthetic = false;
	std::string inPath;
	inPath = mainArgs.dataset_path.get();
	int cams_specified = mainArgs.cams_spec;
	int verts_specified = mainArgs.vert_spec;

	int mode = mainArgs.mode;

	// Specify scene initlaization options
	sibr::HybridSceneOpts harmonizeOpts;
	
	harmonizeOpts.mesh = false;
	harmonizeOpts.renderTargets = false;
	harmonizeOpts.patchClouds = false;
	HybridScene::Ptr scene(new HybridScene(mainArgs, harmonizeOpts));

	std::vector<sibr::InputCamera::Ptr> cams;
	cams = scene->cameras()->inputCameras();

	int cams_considered = (cams_specified > 0 && cams_specified <= cams.size()) ? cams_specified : cams.size();
	int start_cam = 0;

	sibr::Mesh::Ptr mesh, meshColored, meshClone;
	mesh.reset(new sibr::Mesh());
	meshColored.reset(new sibr::Mesh());
	meshClone.reset(new sibr::Mesh());
	if (mainArgs.meshPath.get() != "") {
		mesh->load(mainArgs.meshPath);
	}
	else {
		mesh->load(scene->data()->meshPath());
	}
	meshClone = mesh->clone();
	meshColored = mesh->clone();

	const int numCams = cams_considered;

	sibr::ColoredMeshRenderer CMR;
	sibr::TexturedMeshRenderer TMR;
	std::string outPath;

	if (!mainArgs.harmonizedPath.get().empty()) {
		outPath = mainArgs.dataset_path.get() + "/" + mainArgs.harmonizedPath.get() + "/";
	}
	else {
		outPath = mainArgs.dataset_path.get() + "/hybrid/harmonized_images/";
	}

	std::string varMeshPath = outPath + "/mesh_perVertexVariance.ply";

	if (!directoryExists(outPath)) {
		makeDirectory(outPath);
	}

	if (!scene->varianceMesh()) {

		VarianceMeshGenerator::Ptr meshGenerator(new VarianceMeshGenerator(scene, mesh));

		// [Mode 0]: Compute the correspondece set X_ij specifying the visibility of vertex i in camera j 
		// and write the corresponding RGB pixel color to the file correspondence.txt
		meshGenerator->computeVisibility();

		// Reproject the vertex on each image and find the RGB color corresponding to the vertex for the optimization
		if (mode == 0) {
			SIBR_LOG << "[ColorHarmonize] Reproject the vertex on each image and find the RGB color corresponding to the vertex for the optimization" << std::endl;
			meshGenerator->printCorrespondence(outPath);
			return EXIT_SUCCESS;
		}

		// Compute per-vertex variance mesh

		sibr::Mesh::Colors new_mesh_color;
		sibr::Mesh::Colors per_vertex_variance;

		meshGenerator->computePerVertexVariance(new_mesh_color, per_vertex_variance);

		SIBR_LOG << "[ColorHarmonize] Set the harmonized vertex colors" << std::endl;
		meshColored->colors(new_mesh_color);
		meshColored->normals(mesh->normals());
		meshColored->saveToASCIIPLY(outPath + "/mesh_meanColored.ply", true);
		SIBR_LOG << "[ColorHarmonize] Set the per-vertex variance" << std::endl;
		meshClone->colors(per_vertex_variance);
		meshClone->normals(mesh->normals());
		meshClone->saveToASCIIPLY(outPath + "/mesh_perVertexVariance.ply", true);
		if (mode == 1) {
			SIBR_LOG << "[ColorHarmonize] Saved the mean colored mesh and the per vertex variance mesh!" << std::endl;
			return EXIT_SUCCESS;
		}
	}
	else {
		meshClone = scene->varianceMesh();
	}

	sibr::Timer profile;


	sibr::ImageRGB32F imgsLinProxy;
	sibr::ImageRGB32F imgsLin2Harmonize;
	sibr::ImageRGB32F perVertexVarianceMap;

	static const float maskThresh = 0.1f;

	// Harmonize images against textured mesh
	window.makeContextCurrent();
	profile.tic();
	for (int cam_id = start_cam; cam_id < start_cam + cams_considered; cam_id++) {
		std::cout << "\rHarmonizing Image " << cam_id + 1 << " of " << start_cam + cams_considered << "..." << std::flush;
		sibr::InputCamera cam = *cams[cam_id];

		if (fileExists(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_image_harmonized." + getExtension(getFileName(cam.name()))) || !cam.isActive()) { continue; }

		sibr::ImageRGB img = scene->images()->image(cam_id).clone();
		sibr::ImageRGB texImg;
		sibr::ImageRGB32F linIm;

		// Linearize input image
		img.toOpenCV().convertTo(linIm.toOpenCVnonConst(), CV_32FC3, (1.0f / 255.0f));
		sRGB2Lin(linIm);
		imgsLin2Harmonize = linIm.clone();

#if DEBUG
		img.save(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_original_image." + getExtension(getFileName(cam.name())), false);
		linIm.saveHDR(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_linearized_image.exr", false);
		imgsLin2Harmonize.saveHDR(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_linear_image_2harmonize.exr", false);
#endif

		// Render the per-vertex variance and textured mesh images
		glViewport((GLint)(0), (GLint)(0), (GLsizei)(img.w()), (GLsizei)(img.h()));
		sibr::RenderTargetRGB32F RT(img.w(), img.h());

		RT.clear();
		CMR.process(*meshClone, *cams[cam_id], RT);
		window.swapBuffer();
		RT.readBack(perVertexVarianceMap);

		RT.clear();
		TMR.process(*mesh, *cams[cam_id], scene->inputMeshTextures()->handle(), RT);
		window.swapBuffer();
		RT.readBack(texImg);

		texImg.toOpenCV().convertTo(linIm.toOpenCVnonConst(), CV_32FC3, (1.0f / 255.0f));
		sRGB2Lin(linIm);
		imgsLinProxy = linIm.clone();

		sibr::ImageRGB32F debugImgBlur, debugImgProxyBlur, debugBlurImgSat, debugBlurImgProxySat;

		cv::Mat3f blurredLinProxy;
		cv::Mat3f blurredImgsLin2Harmonize;
		cv::Mat binaryLinProxySat;
		cv::Mat binarymask;
		cv::Mat1f specMask;

		const int img_res = (img.w() >= img.h()) ? img.w() : img.h();
		static const int BLUR_RADIUS = 0.03 * img_res;

#if DEBUG
		imgsLinProxy.saveHDR(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_linear_proxy_image.exr", false);
		perVertexVarianceMap.save(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_specular_mask." + getExtension(getFileName(cam.name())), false);
#endif

		cv::cvtColor(perVertexVarianceMap.toOpenCVnonConst(), binaryLinProxySat, cv::COLOR_RGB2GRAY);
		cv::threshold(binaryLinProxySat, specMask, maskThresh, 1.0, cv::THRESH_BINARY_INV);

		sibr::ImageRGB32F debugMap;
		debugMap.fromOpenCV(binaryLinProxySat);
		debugMap.saveHDR(outPath + "/map/" + removeExtension(getFileName(cam.name())) + "_map_variance.exr", false);

		// Mask out the saturated regions; luminance in [5,250]
		for (int j = 0; j < img.h(); j += 1) {
			for (int i = 0; i < img.w(); i += 1) {
				sibr::Vector3f imgColor = img(i, j).cast<float>();
				float luminance = (0.299 * imgColor.x() + 0.587 * imgColor.y() + 0.114 * imgColor.z());
				if (luminance > 250.0f || luminance < 5.0f || imgsLinProxy(i, j).norm() <= 0.0f) {
					specMask(j, i) = 0.0f;
				}
			}
		}

		sibr::ImageRGB32F debugBlurSat;
		
		specMask.convertTo(binarymask, CV_8U);

		imgsLinProxy.toOpenCV().copyTo(blurredLinProxy, binarymask);
		imgsLin2Harmonize.toOpenCV().copyTo(blurredImgsLin2Harmonize, binarymask);

		cv::blur(blurredLinProxy, blurredLinProxy, cv::Size(BLUR_RADIUS, BLUR_RADIUS));
		cv::blur(blurredImgsLin2Harmonize, blurredImgsLin2Harmonize, cv::Size(BLUR_RADIUS, BLUR_RADIUS));

#if DEBUG
		sibr::ImageRGB32F blurredProxy, blurredImg;

		blurredProxy.fromOpenCV(blurredLinProxy);
		blurredImg.fromOpenCV(blurredImgsLin2Harmonize);
		blurredProxy.saveHDR(outPath + "/mask/" + removeExtension(getFileName(cam.name())) + "_image_masked_proxy.exr", false);
		blurredImg.saveHDR(outPath + "/mask/" + removeExtension(getFileName(cam.name())) + "_image_masked_img.exr", false);
#endif

		cv::Mat3f rgbFactorImg = blurredLinProxy.clone();
		cv::Mat1f noChangeMask = cv::Mat1f::ones(blurredLinProxy.size());

#pragma omp parallel for
		for (int j = 0; j < img.h(); j += 1) {
			for (int i = 0; i < img.w(); i += 1) {
				//if (imgsLinProxy(i, j).norm() <= 0.0f) {
				if (specMask(j, i) == 0.0f) {
					noChangeMask(j, i) = 0.0f;
					continue;
				}

				float r = std::max(blurredLinProxy(j, i)[0], 0.000001f) / std::max(blurredImgsLin2Harmonize(j, i)[0], 0.000001f);
				float g = std::max(blurredLinProxy(j, i)[1], 0.000001f) / std::max(blurredImgsLin2Harmonize(j, i)[1], 0.000001f);
				float b = std::max(blurredLinProxy(j, i)[2], 0.000001f) / std::max(blurredImgsLin2Harmonize(j, i)[2], 0.000001f);

				Eigen::Matrix3f mFinal;


				mFinal << r, 0, 0,
					0, g, 0,
					0, 0, b;

				if (r == g == b == 1.0f) {
					noChangeMask(j, i) = 0.0f;
					continue;
				}

				imgsLin2Harmonize(i, j) = mFinal * imgsLin2Harmonize(i, j);
				rgbFactorImg(j, i) = cv::Vec3f(r, g, b);
			}
		}


#if DEBUG
		sibr::ImageRGB32F debugRGBFactor;
		debugRGBFactor.fromOpenCV(rgbFactorImg);
		debugRGBFactor.save(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_rgb_factor." + getExtension(getFileName(cam.name())), false);
#endif

		sibr::ImageRGB32F debugNoChangeMask;
		debugNoChangeMask.fromOpenCV(noChangeMask);
		debugNoChangeMask.save(outPath + "/map/" + removeExtension(getFileName(cam.name())) + "_no_change.png", false);


		imgsLin2Harmonize.saveHDR(outPath + "/map/" + removeExtension(getFileName(cam.name())) + "_image_harmonized.exr", false);
		sibr::lin2sRGB(imgsLin2Harmonize);
		imgsLin2Harmonize.save(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_image_harmonized." + getExtension(getFileName(cam.name())), false);


	}
	//profile.toc();
	//std::cout << std::endl;
	SIBR_LOG << "[Time taken] for harmonizing dataset: " << profile.deltaTimeFromLastTic() << " ms." << std::endl;
	//profile.display();

	profile.tic();
	// Perform gradient based poisson reconstruction to reintroduce the highlights in selected regions
	for (int cam_id = start_cam; cam_id < start_cam + cams_considered; cam_id++) {
		std::cout << "\rRe-injecting specularity: " << cam_id + 1 << " of " << start_cam + cams_considered << "..." << std::flush;
		sibr::InputCamera cam = *cams[cam_id];

		if (!fileExists(outPath + "/debug/" + removeExtension(getFileName(cam.name())) + "_image_harmonized." + getExtension(getFileName(cam.name()))) || !cam.isActive()) { continue; }
		if (fileExists(outPath + "/" + cam.name()) || !cam.isActive()) { continue; }

		sibr::ImageRGB img = scene->images()->image(cam_id).clone();

		//Compute the gradients 
		sibr::ImageRGB32F linIm;
		img.toOpenCV().convertTo(linIm.toOpenCVnonConst(), CV_32FC3, (1.0f / 255.0f));
		sRGB2Lin(linIm);

		cv::Mat3f imageGrad = linIm.toOpenCV().clone();
		cv::Mat3f gradX = cv::Mat3f::zeros(imageGrad.size());
		cv::Mat3f gradY = cv::Mat3f::zeros(imageGrad.size());

		int w = img.w();
		int h = img.h();

#pragma omp parallel for
		for (int i = 0; i < imageGrad.rows; ++i) {
#pragma omp parallel for
			for (int j = 0; j < imageGrad.cols; ++j) {

				int ip = std::min(i + 1, imageGrad.rows - 1);
				int jp = std::min(j + 1, imageGrad.cols - 1);

				// Get the 3x3 neighborhood.
				cv::Vec3f c = imageGrad.at<cv::Vec3f>(i, j);
				cv::Vec3f d = imageGrad.at<cv::Vec3f>(ip, j);
				cv::Vec3f r = imageGrad.at<cv::Vec3f>(i, jp);
				cv::Vec3f dX = d - c;
				cv::Vec3f dY = r - c;
				gradX.at<cv::Vec3f>(i, j) = dX;
				gradY.at<cv::Vec3f>(i, j) = dY;
			}
		}

		sibr::ImageRGB32F gradientX, gradientY, guideImg;

		/// Compute the mask

		// Read the variance maps
		sibr::ImageRGB32F varianceMap, harmonizedDifferenceMap;
		varianceMap.load(outPath + "/map/" + removeExtension(getFileName(cam.name())) + "_map_variance.exr", false);
		cv::Mat3f varMap = varianceMap.toOpenCV().clone();
		
		// Compute the difference image between original and harmonized image in linear space
		imgsLin2Harmonize.load(outPath + "/map/" + removeExtension(getFileName(cam.name())) + "_image_harmonized.exr", false);
		cv::Mat3f harmonizedImg = imgsLin2Harmonize.toOpenCV().clone();
		cv::Mat3f originalImg = linIm.toOpenCV().clone();
		cv::Mat3f diffImg;

		harmonizedImg.convertTo(harmonizedImg, CV_32F);
		originalImg.convertTo(originalImg, CV_32F);
		diffImg = abs(originalImg - harmonizedImg);

		sibr::ImageRGB32F diffMap;
		cv::Mat map;

		// To use difference map based mask
		cv::cvtColor(diffImg, map, cv::COLOR_BGR2GRAY);
		diffMap.fromOpenCV(map);
#if DEBUG
		diffMap.saveHDR(outPath + "/map/" + removeExtension(getFileName(cam.name())) + "_map_diff.exr", false);
#endif

		sibr::ImageRGB32F bMask;
		cv::Mat maskFinal, maskManual;
		cv::Mat tempMask;
		sibr::ImageL8 noChange;
		noChange.load(outPath + "/map/" + removeExtension(getFileName(cam.name())) + "_no_change.png", false);

		cv::Mat noChangeMask;
		noChange.toOpenCV().convertTo(noChangeMask, CV_8UC1);

		// Normalize variance and difference map to bound between [0.0, 1.0]
		cv::normalize(map, map, 0.0, 1.0, cv::NORM_MINMAX);
		harmonizedDifferenceMap.fromOpenCV(map);
		cv::normalize(varMap, varMap, 0.0, 1.0, cv::NORM_MINMAX);
		varianceMap.fromOpenCV(varMap);

#if DEBUG
		harmonizedDifferenceMap.saveHDR(outPath + "/mask/" + removeExtension(getFileName(cam.name())) + "_map_diff.exr", false);
		varianceMap.saveHDR(outPath + "/mask/" + removeExtension(getFileName(cam.name())) + "_map_var.exr", false);
#endif
		cv::Mat3f gradXNorm, gradYNorm;

		//cv::normalize(gradX, gradXNorm, 0.0, 1.0, cv::NORM_MINMAX);
		gradientX.fromOpenCV(gradX);
		//cv::normalize(gradY, gradYNorm, 0.0, 1.0, cv::NORM_MINMAX);
		gradientY.fromOpenCV(gradY);


#if DEBUG
		gradientX.saveHDR(outPath + "/mask/" + removeExtension(getFileName(cam.name())) + "_gradX.exr", false);
		gradientY.saveHDR(outPath + "/mask/" + removeExtension(getFileName(cam.name())) + "_gradY.exr", false);
#endif

		sibr::SpecularMaskGenerator mrfMask(linIm, varianceMap, harmonizedDifferenceMap, gradientX, gradientY);
		mrfMask.solve();
		
		sibr::ImageL8 specMask = mrfMask.getResult();

		specMask.save(outPath + "/mask/" + removeExtension(getFileName(cam.name())) + "_mask_MRF.png", false);
		specMask.toOpenCV().convertTo(tempMask, CV_8UC1);
		cv::bitwise_and(noChangeMask, tempMask, maskFinal);

		maskFinal.convertTo(maskFinal, CV_32FC3, 1.0 / 255.0, 0);
		bMask.fromOpenCV(maskFinal);
		bMask.save(outPath + "/mask/" + removeExtension(getFileName(cam.name())) + "_mask_final.png", false);


		cv::Mat3f guideF = imgsLin2Harmonize.toOpenCV().clone();

#if DEBUG
		bMask.fromOpenCV(guideF);
		bMask.saveHDR(outPath + "/mask/" + removeExtension(getFileName(cam.name())) + "_image_harmonized_guide.exr", false);
#endif

		// Create Poisson Solver and Solve
		sibr::PoissonReconstruction poisson(gradX, gradY, maskFinal, guideF);
		poisson.solve();
		const cv::Mat3f resultF = poisson.result();
		
		ImageRGB32F::Ptr filled(new ImageRGB32F());
		filled->fromOpenCV(resultF);
		sibr::lin2sRGB(*filled);
		filled->save(outPath + getFileName(cam.name()), false);

	}
	SIBR_LOG << "[Time taken] for specularity re-injection: " << profile.deltaTimeFromLastTic() << " ms." << std::endl;

	return EXIT_SUCCESS;
}
