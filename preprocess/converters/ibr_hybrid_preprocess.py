# Copyright (C) 2021, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
#
# This software is free for non-commercial, research and evaluation use
# under the terms of the LICENSE.md file.
#
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#!/usr/bin/env python
# -*- coding: utf-8 -*-
# --------------------------------------------
""" @package deep_blending_preprocess
This script creates Hybrid IBR data from a COLMAP dataset which can be fed to a Hybrid IBR application

Parameters: -h help,
            -i <path to input directory>,
            -r use release w/ debug symbols executables
            -w to specify max-width in which the dataset should be preprocessed [optional]

Usage: python ibr_hybrid_preprocess.py -r -i <path_to_sibr>\sibr\install\bin\datasets\Library

"""

import subprocess
import shutil
import os
import re
import sys
import getopt
from utils.commands import getProcess
from utils.paths import getBinariesPath
import time
from os import walk

# --------------------------------------------

# ===============================================================================

import struct
import imghdr


def get_image_size(fname):
    '''Determine the image type of fhandle and return its size.
    from draco'''
    with open(fname, 'rb') as fhandle:
        head = fhandle.read(24)
        if len(head) != 24:
            return
        if imghdr.what(fname) == 'png':
            check = struct.unpack('>i', head[4:8])[0]
            if check != 0x0d0a1a0a:
                return
            width, height = struct.unpack('>ii', head[16:24])
        elif imghdr.what(fname) == 'gif':
            width, height = struct.unpack('<HH', head[6:10])
        elif imghdr.what(fname) == 'jpeg':
            try:
                fhandle.seek(0)  # Read 0xff next
                size = 2
                ftype = 0
                while not 0xc0 <= ftype <= 0xcf:
                    fhandle.seek(size, 1)
                    byte = fhandle.read(1)
                    while ord(byte) == 0xff:
                        byte = fhandle.read(1)
                    ftype = ord(byte)
                    size = struct.unpack('>H', fhandle.read(2))[0] - 2
                # We are at a SOFn block
                fhandle.seek(1, 1)  # Skip `precision' byte.
                height, width = struct.unpack('>HH', fhandle.read(4))
            except Exception:  # IGNORE:W0703
                return
        else:
            return
        return width, height


def checkOutput(output, force_continue):
    if (output != 0):
        if (not force_continue):
            sys.exit()
        else:
            return False
    else:
        return True


# ===============================================================================

# --------------------------------------------
# 0. Paths, commands and options

def main(argv):
    opts, args = getopt.getopt(argv, "hi:rd:w:g:s:t:b", ["idir=", "bin="])
    executables_suffix = ""
    executables_folder = getBinariesPath()
    path_data = ""
    m_width = 0
    grid_size = 32
    r_scale = 4
    target = 1
    create_bounds = False
    for opt, arg in opts:
        if opt == '-h':
            print(
                "-i path_to_rc_data_dir [-w max_width -g grid_size -s refinement_scale -b use_grid_bounds -r (use release w/ debug symbols executables)]")
            sys.exit()
        elif opt == '-i':
            path_data = arg
            print(['Setting path_data to ', path_data])
        elif opt == '-r':
            executables_suffix = "_rwdi"
            print(["Using rwdi executables."])
        elif opt == '-w':
            m_width = arg
            print(["Max width set to", m_width])
        elif opt == '-g':
            grid_size = arg
            print(["Grid size set to", grid_size])
        elif opt == '-s':
            r_scale = arg
            print(["Refinement scale set to", r_scale])
        elif opt == '-t':
            target = arg
            print(["Target mode set to", target])
        elif opt == '-b':
            create_bounds = True
            print(["Create bounds from cameras: ", create_bounds])
        elif opt in ('-bin', '--bin'):
            executables_folder = os.path.abspath(arg)

    return (path_data, executables_suffix, executables_folder, m_width, grid_size, r_scale, target, create_bounds)


# m_width = 0
grid_size = 32
r_scale = 4
# create_bounds = False
target = 1
path_data, executables_suffix, executables_folder, m_width, grid_size, r_scale, target, create_bounds = main(
    sys.argv[1:])

if (path_data == ""):
    path_data = os.path.abspath(os.path.join(os.path.dirname(__file__), "../datasets"))

path_data = os.path.abspath(path_data + "/") + "/"

print(['Raw_data folder: ', path_data])
print(['Executables folder: ', executables_folder])

force_continue = False

color_harmonize = getProcess("color_harmonize" + executables_suffix, executables_folder)
spec_texturer = getProcess("textureMeshMask" + executables_suffix, executables_folder)
recon_cmd = getProcess("sibr_recon_cmd" + executables_suffix, executables_folder)
depthmap_mesher = getProcess("sibr_depthmap_mesher" + executables_suffix, executables_folder)
patchCloud_generate = getProcess("patchCloud_generator" + executables_suffix, executables_folder)


# 0.1. Run color harmonize to get harmonized images

#print(color_harmonize)

print([color_harmonize, "--path", path_data, "--noPC", executables_folder])
tic = time.perf_counter()
program_exit = subprocess.call([color_harmonize, "--path", path_data, "--noPC"], cwd=executables_folder)
toc = time.perf_counter()
print(f"Harmonized in {toc - tic:0.4f} seconds")
print("Color Harmonization exited with ", program_exit)
sys.stdout.flush()
if( not checkOutput(program_exit, force_continue) ):
   sys.exit(-1)
# --------------------------------------------
# 0.2. Run specMaskTexturer to get specular texture

print(spec_texturer)

print([spec_texturer, "--path", path_data, "--output", path_data + "\\hybrid\\harmonized_images\\specTexture_u1_v1.png", "--image",
       path_data + "\\hybrid\\harmonized_images\\mask", executables_folder])
tic = time.perf_counter()
program_exit = subprocess.call(
    [spec_texturer, "--path", path_data, "--output", path_data + "\\hybrid\\harmonized_images\\specTexture_u1_v1.png", "--image",
     path_data + "\\hybrid\\harmonized_images\\mask"], cwd=executables_folder)
toc = time.perf_counter()
print(f"Specular Texture generated in {toc - tic:0.4f} seconds")
print("Specular Texturer exited with ", program_exit)
sys.stdout.flush()
if (not checkOutput(program_exit, force_continue)):
    sys.exit(-1)

# --------------------------------------------


if target != '0':

    # 1. Run recon_cmd to output an NVM scene representation
    print(recon_cmd)
    tic = time.perf_counter()
    if int(m_width) > 0:
        print([recon_cmd, path_data, "max_width=", m_width, executables_folder])
        program_exit = subprocess.call([recon_cmd, path_data, "max_width=" + m_width, "grid_size=" + str(grid_size)],
                                       cwd=executables_folder)
    else:
        print([recon_cmd, path_data, executables_folder])
        program_exit = subprocess.call([recon_cmd, path_data, "grid_size=" + str(grid_size)], cwd=executables_folder)

    toc = time.perf_counter()
    print(f"Depth map refined in {toc - tic:0.4f} seconds")
    print("Recon cmd exited with ", program_exit)
    sys.stdout.flush()
    if (not checkOutput(program_exit, force_continue)):
        sys.exit(-1)

    # --------------------------------------------
    # 2. Run depthmapmesher to mesh and get per-view meshes

    print(depthmap_mesher)

    print([depthmap_mesher, "--path", path_data, executables_folder])
    tic = time.perf_counter()
    program_exit = subprocess.call([depthmap_mesher, "--path", path_data], cwd=executables_folder)
    toc = time.perf_counter()
    print(f"PV Meshes generated in {toc - tic:0.4f} seconds")
    print("Depthmapmesher exited with ", program_exit)
    sys.stdout.flush()
    if (not checkOutput(program_exit, force_continue)):
        sys.exit(-1)


# --------------------------------------------
# 4. Run patchCloud_generator to construct the hybrid patch clouds and store them in a output_hybrid.pcloud file

print(patchCloud_generate)
tic = time.perf_counter()
if int(m_width) > 0:
    print([patchCloud_generate, "--path", path_data, "--rendering-size", m_width, "--grid_size", str(grid_size),
           "--scale", str(r_scale), executables_folder])
    if (create_bounds):
        program_exit = subprocess.call([patchCloud_generate, "--path", path_data, "--rendering-size", m_width, "--grid_size", str(grid_size),
         "--scale", str(r_scale), "--grid_bounds"], cwd=executables_folder)
    else:
        program_exit = subprocess.call([patchCloud_generate, "--path", path_data, "--rendering-size", m_width, "--grid_size", str(grid_size),
         "--scale", str(r_scale)], cwd=executables_folder)
else:
    print([recon_cmd, "--path", path_data, "--grid_size", str(grid_size), "--scale", str(r_scale), executables_folder])
    if (create_bounds):
        program_exit = subprocess.call([patchCloud_generate, "--path", path_data, "--grid_size", str(grid_size), "--scale", str(r_scale), "--grid_bounds"],
            cwd=executables_folder)
    else:
        program_exit = subprocess.call([patchCloud_generate, "--path", path_data, "--grid_size", str(grid_size), "--scale", str(r_scale)],
        cwd=executables_folder)
toc = time.perf_counter()
print(f"Hybrid PClouds generated in {toc - tic:0.4f} seconds")
print("PatchCloud Generator exited with ", program_exit)
sys.stdout.flush()
if (not checkOutput(program_exit, force_continue)):
    sys.exit(-1)

# --------------------------------------------
