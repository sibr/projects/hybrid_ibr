// Copyright (C) 2021, Inria
// GRAPHDECO research group, https://team.inria.fr/graphdeco
// All rights reserved.
// 
// This software is free for non-commercial, research and evaluation use 
// under the terms of the LICENSE.md file.
// 
// For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

#include <fstream>

#include "projects/hybrid_ibr/renderer/HybridView.hpp"
#include "projects/hybrid_ibr/renderer/PerViewMeshDebugView.hpp"
#include <core/graphics/Window.hpp>
#include <core/view/MultiViewManager.hpp>
#include <core/system/String.hpp>
#include <core/scene/BasicIBRScene.hpp>
#include <core/raycaster/Raycaster.hpp>
#include <core/view/SceneDebugView.hpp>
#include <Windows.h>

#define PROGRAM_NAME "pvm_render_app"
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path <dataset-path>"    	                                "\n"
;


int main(int ac, char** av) {

	// Parse Commad-line Args
	CommandLineArgs::parseMainArgs(ac, av);
	BasicIBRAppArgs basicArgs;
	HybridAppArgs myArgs;

	const bool doVSync = !myArgs.vsync;
	// rendering size
	uint rendering_width = myArgs.rendering_size.get()[0];
	uint rendering_height = myArgs.rendering_size.get()[1];
	// window size
	uint win_width = myArgs.win_width;
	uint win_height = myArgs.win_height;

	// Window setup
	sibr::Window		window(PROGRAM_NAME, sibr::Vector2i(50, 50), myArgs);


	// Specify scene initlaization options
	BasicIBRScene::SceneOptions sceneOpts;
	std::string harmonizedLinImagesPath = myArgs.dataset_path.get() + "/" + myArgs.harmonizedPath.get() + "/";

	if (!myArgs.meshPath.get().empty()) {
		sceneOpts.mesh = false;
	}
	sceneOpts.renderTargets = false;
	if (directoryExists(harmonizedLinImagesPath) && !myArgs.compareMode && !myArgs.noPC && !myArgs.test_mode)
	{
		sceneOpts.images = false;

	}
	sibr::Timer timer;

	// Always load patch clouds for rendering
	myArgs.noPC = false;

	// Setup a Basic IBR scene: create cameras, images, load the proxy, and create the texture arrays.
	SIBR_LOG << "[PVMApp] : Creating Basic IBR Scene" << std::endl;
	BasicIBRScene::Ptr		scene(new BasicIBRScene(basicArgs, sceneOpts));
	
	sibr::Mesh::Ptr customMesh;
	if (!sceneOpts.mesh) {
		
		customMesh.reset(new Mesh());
		customMesh->load(myArgs.meshPath);
		scene->proxies()->replaceProxyPtr(customMesh);
	}


	const uint flags = SIBR_GPU_LINEAR_SAMPLING | SIBR_FLIP_TEXTURE; 

	// Fix rendering aspect ratio if user provided rendering size
	uint scene_width = scene->cameras()->inputCameras()[0]->w();
	uint scene_height = scene->cameras()->inputCameras()[0]->h();
	float scene_aspect_ratio = scene_width * 1.0f / scene_height;
	float rendering_aspect_ratio = rendering_width * 1.0f / rendering_height;

	if ((rendering_width > 0)) {
		if (abs(scene_aspect_ratio - rendering_aspect_ratio) > 0.001f) {
			if (scene_width > scene_height) {
				rendering_height = rendering_width / scene_aspect_ratio;
			}
			else {
				rendering_width = rendering_height * scene_aspect_ratio;
			}
		}
	}

	// check rendering size
	rendering_width = (rendering_width <= 0) ? scene->cameras()->inputCameras()[0]->w() : rendering_width;
	rendering_height = (rendering_height <= 0) ? scene->cameras()->inputCameras()[0]->h() : rendering_height;
	Vector2u usedResolution(rendering_width, rendering_height);
	SIBR_LOG << "[PVMApp] : Display resolution: " << usedResolution << std::endl;

	const unsigned int sceneResWidth = usedResolution.x();
	const unsigned int sceneResHeight = usedResolution.y();
	
	if (myArgs.compareMode || myArgs.noPC)
	{
		scene->renderTargets()->initRGBandDepthTextureArrays(scene->cameras(), scene->images(), scene->proxies(), flags, myArgs.texture_width);
	}

	// Finish creating a Basic IBR Scene

	// Create a Hybrid scene from basic IBR Scene
	SIBR_LOG << "[PVMApp] : Creating Hybrid IBR Scene" << std::endl;
	HybridScene::Ptr		hybridScene(new HybridScene(scene, myArgs));
	HybridView::Ptr			hybridView;

	sibr::ImageRGB img;
	std::vector<sibr::ImageRGB::Ptr> harmonizedLinImages;
	
	// Load harmonized images and assign it to hybrid scene if present
	if (directoryExists(harmonizedLinImagesPath) && !myArgs.noPC) {
		SIBR_LOG << "[PVMApp] : Loading harmonized images from: " << harmonizedLinImagesPath << std::endl;
		for (int i = 0; i < hybridScene->data()->numCameras(); i++) {
			if (hybridScene->cameras()->inputCameras()[i]->isActive()) {
				std::string imageName = harmonizedLinImagesPath + hybridScene->data()->imgInfos()[i].filename;
				img.load(imageName, false);
				harmonizedLinImages.push_back(img.clonePtr());
			}
			else {
				harmonizedLinImages.push_back(new ImageRGB(scene->cameras()->inputCameras()[i]->w(), scene->cameras()->inputCameras()[i]->h()));
			}
		}

		hybridScene->images()->loadFromExisting(harmonizedLinImages);
	}
	else {
		SIBR_LOG << "[PVMApp] : Couldn't load harmonized images from: " << harmonizedLinImagesPath << std::endl;
	}

	hybridScene->renderTargets()->initRGBandDepthTextureArrays(hybridScene->cameras(), hybridScene->images(), hybridScene->proxies(), flags, myArgs.texture_width);


	hybridView.reset(new HybridView(hybridScene, sceneResWidth, sceneResHeight, "PVM View Settings"));
	
	// Raycaster.
	std::shared_ptr<sibr::Raycaster> raycaster = std::make_shared<sibr::Raycaster>();
	raycaster->init();
	raycaster->addMesh(hybridScene->proxies()->proxy());

	// Camera handler for main view.
	sibr::InteractiveCameraHandler::Ptr generalCamera(new InteractiveCameraHandler());
	generalCamera->setup(hybridScene->cameras()->inputCameras(), Viewport(0, 0, (float)usedResolution.x(), (float)usedResolution.y()), raycaster);

	// Add views to mvm.
	MultiViewManager        multiViewManager(window, false);

	// Hybrid view: Shows Hybrid view; uses Harmonized images if compare view is chosen
	multiViewManager.addIBRSubView("PVM Render view", hybridView, usedResolution, ImGuiWindowFlags_ResizeFromAnySide);
	multiViewManager.addCameraForView("PVM Render view", generalCamera);

	// Top view
	const std::shared_ptr<sibr::SceneDebugView> topView(new sibr::SceneDebugView(hybridScene, generalCamera, myArgs));
	multiViewManager.addSubView("Top view", topView, usedResolution);


	// If topView is true, don't show debug View. Also if No Patch cloud, don't create debug view
	std::shared_ptr<sibr::MultiMeshManager>   debugView(nullptr);
	std::shared_ptr<sibr::PerViewMeshDebugView> pvMeshView(nullptr);
	if (!myArgs.topView) {
		multiViewManager.getIBRSubView("Top view")->active(false);
		if (!myArgs.noPC) {
			debugView = std::shared_ptr<sibr::MultiMeshManager>(new MultiMeshManager("Debug view"));
			debugView->addMeshAsLines("Gizmo", RenderUtility::createAxisGizmo()).setDepthTest(false).setColorMode(MeshData::ColorMode::VERTEX);
			debugView->setIntialView(myArgs.dataset_path);
			multiViewManager.addSubView("Mesh Debug view", debugView, usedResolution / 2);
			hybridView->registerDebugObjects(debugView, generalCamera);
			multiViewManager.getIBRSubView("Mesh Debug view")->active(false);

			if (myArgs.meshDebugView) {
				pvMeshView.reset(new PerViewMeshDebugView(hybridScene, sceneResWidth, sceneResHeight, "PVMesh Debug View Settings"));
				multiViewManager.addIBRSubView("PVMesh Debug View", pvMeshView, usedResolution, ImGuiWindowFlags_ResizeFromAnySide);
				multiViewManager.addCameraForView("PVMesh Debug View", generalCamera);
				pvMeshView->registerDebugObjects(debugView, generalCamera);
				multiViewManager.getIBRSubView("PVMesh Debug View")->active(false);
			}

		}
	}

	hybridView->setLayerMode(HybridView::RenderLayer::PVM);
	CHECK_GL_ERROR;
	if (myArgs.pathFile.get() != "") {
		
		generalCamera->getCameraRecorder().loadPath(myArgs.pathFile.get(), usedResolution.x(), usedResolution.y());
		generalCamera->getCameraRecorder().recordOfflinePath(myArgs.outPath, multiViewManager.getIBRSubView("PVM Render view"), "hybrid/pvm");
		if (!myArgs.noExit)
			exit(0);
	}

	// Main looooooop.
	while (window.isOpened()) {

		sibr::Input::poll();
		window.makeContextCurrent();
		if (sibr::Input::global().key().isPressed(sibr::Key::Escape)) {
			window.close();
		}

		multiViewManager.onUpdate(sibr::Input::global());
		multiViewManager.onRender(window);

		window.swapBuffer();
		CHECK_GL_ERROR;
	}
	
	return EXIT_SUCCESS;
}