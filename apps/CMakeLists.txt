# Copyright (C) 2021, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

project(SIBR_hybrid_apps)

add_subdirectory(hybrid_render_app)
add_subdirectory(pvm_render_app)
add_subdirectory(hybrid_compare_app)

