# Copyright (C) 2021, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr

# project name
project(hybrid_compare_app)

file(GLOB SOURCES "*.cpp" "*.h" "*.hpp")

# Define build output for project
add_executable(${PROJECT_NAME} ${SOURCES})

target_link_libraries(${PROJECT_NAME}
  ${Boost_LIBRARIES}
  OpenMP::OpenMP_CXX
  sibr_assets
  sibr_graphics
  sibr_system
  sibr_view
  sibr_renderer
  sibr_scene
  sibr_ulr
  sibr_hybrid
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "projects/hybrid/apps")

if (WIN32)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd4251")
endif()


## Hight level macro to install in an homogen way all our ibr targets
include(install_runtime)
ibr_install_target(${PROJECT_NAME}
    INSTALL_PDB                         ## mean install also MSVC IDE *.pdb file (DEST according to target type)
		#RSC_FILE_ADD "${PROJECT_NAME}_rsc"
		#DIRS 				${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
    STANDALONE  ${INSTALL_STANDALONE}   ## mean call install_runtime with bundle dependencies resolution
    COMPONENT   ${PROJECT_NAME}_install ## will create custom target to install only this project
)
